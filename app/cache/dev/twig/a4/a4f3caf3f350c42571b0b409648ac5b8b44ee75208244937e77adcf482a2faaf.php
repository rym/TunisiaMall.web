<?php

/* AcmeDemoBundle:Secured:helloadmin.html.twig */
class __TwigTemplate_5909cd3b6bcdb5b8ec1e3b0bcb9cdffcb59636a818da64cfec05744cb7871758 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AcmeDemoBundle:Secured:layout.html.twig", "AcmeDemoBundle:Secured:helloadmin.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AcmeDemoBundle:Secured:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aca403693e7b375a23ca5cf2c5507692c1e155eb7a8f31fc7c83a79e87420fd1 = $this->env->getExtension("native_profiler");
        $__internal_aca403693e7b375a23ca5cf2c5507692c1e155eb7a8f31fc7c83a79e87420fd1->enter($__internal_aca403693e7b375a23ca5cf2c5507692c1e155eb7a8f31fc7c83a79e87420fd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AcmeDemoBundle:Secured:helloadmin.html.twig"));

        // line 9
        $context["code"] = $this->env->getExtension('demo')->getCode($this);
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aca403693e7b375a23ca5cf2c5507692c1e155eb7a8f31fc7c83a79e87420fd1->leave($__internal_aca403693e7b375a23ca5cf2c5507692c1e155eb7a8f31fc7c83a79e87420fd1_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_8e86ffa4010a898ab241a6a9b2e86691c9475eabf0d1e810679181459ca0b818 = $this->env->getExtension("native_profiler");
        $__internal_8e86ffa4010a898ab241a6a9b2e86691c9475eabf0d1e810679181459ca0b818->enter($__internal_8e86ffa4010a898ab241a6a9b2e86691c9475eabf0d1e810679181459ca0b818_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, ("Hello " . (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name"))), "html", null, true);
        
        $__internal_8e86ffa4010a898ab241a6a9b2e86691c9475eabf0d1e810679181459ca0b818->leave($__internal_8e86ffa4010a898ab241a6a9b2e86691c9475eabf0d1e810679181459ca0b818_prof);

    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        $__internal_827df4d4a73648ffb40eb490855639d343107a0e5db514daa3cbe315a87fc0f7 = $this->env->getExtension("native_profiler");
        $__internal_827df4d4a73648ffb40eb490855639d343107a0e5db514daa3cbe315a87fc0f7->enter($__internal_827df4d4a73648ffb40eb490855639d343107a0e5db514daa3cbe315a87fc0f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <h1 class=\"title\">Hello ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo " secured for Admins only!</h1>
";
        
        $__internal_827df4d4a73648ffb40eb490855639d343107a0e5db514daa3cbe315a87fc0f7->leave($__internal_827df4d4a73648ffb40eb490855639d343107a0e5db514daa3cbe315a87fc0f7_prof);

    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle:Secured:helloadmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 6,  50 => 5,  38 => 3,  31 => 1,  29 => 9,  11 => 1,);
    }
}
/* {% extends "AcmeDemoBundle:Secured:layout.html.twig" %}*/
/* */
/* {% block title "Hello " ~ name %}*/
/* */
/* {% block content %}*/
/*     <h1 class="title">Hello {{ name }} secured for Admins only!</h1>*/
/* {% endblock %}*/
/* */
/* {% set code = code(_self) %}*/
/* */
