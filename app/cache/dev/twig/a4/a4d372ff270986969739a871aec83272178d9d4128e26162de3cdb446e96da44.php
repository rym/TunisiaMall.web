<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_b9d9683a6a5f535dcfb8c0454e93bc4e9efc8ad06b84316b2c863d6c9c40a7c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_151737e609e923a064822ef34900c3875df1fc41fdbb6614d46e6d32ea02ded7 = $this->env->getExtension("native_profiler");
        $__internal_151737e609e923a064822ef34900c3875df1fc41fdbb6614d46e6d32ea02ded7->enter($__internal_151737e609e923a064822ef34900c3875df1fc41fdbb6614d46e6d32ea02ded7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_151737e609e923a064822ef34900c3875df1fc41fdbb6614d46e6d32ea02ded7->leave($__internal_151737e609e923a064822ef34900c3875df1fc41fdbb6614d46e6d32ea02ded7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}*/
/* */
