<?php

/* MyAppUserBundle:Group:show_content.html.twig */
class __TwigTemplate_ad2aad9193554ad772dc13486d70d9425187e2c53fa8d258e9e3de02e5aa56c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_745cdc3e51120af0309c2842e5770443ae8f3538d8840f4c1b3a50f084b01137 = $this->env->getExtension("native_profiler");
        $__internal_745cdc3e51120af0309c2842e5770443ae8f3538d8840f4c1b3a50f084b01137->enter($__internal_745cdc3e51120af0309c2842e5770443ae8f3538d8840f4c1b3a50f084b01137_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["group"]) ? $context["group"] : $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_745cdc3e51120af0309c2842e5770443ae8f3538d8840f4c1b3a50f084b01137->leave($__internal_745cdc3e51120af0309c2842e5770443ae8f3538d8840f4c1b3a50f084b01137_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 4,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* <div class="fos_user_group_show">*/
/*     <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>*/
/* </div>*/
/* */
