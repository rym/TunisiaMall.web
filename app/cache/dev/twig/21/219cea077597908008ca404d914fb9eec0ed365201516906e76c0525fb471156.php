<?php

/* AcmeDemoBundle:Demo:hello.html.twig */
class __TwigTemplate_999a7996e14e849faed3448f0060be6a3daa773f7776daf925d27789fc887a7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AcmeDemoBundle::layout.html.twig", "AcmeDemoBundle:Demo:hello.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AcmeDemoBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_87796d6950d98e63798ffabb191bb92aeffd328d6ed6b62c6b3827cc9dbcee3a = $this->env->getExtension("native_profiler");
        $__internal_87796d6950d98e63798ffabb191bb92aeffd328d6ed6b62c6b3827cc9dbcee3a->enter($__internal_87796d6950d98e63798ffabb191bb92aeffd328d6ed6b62c6b3827cc9dbcee3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AcmeDemoBundle:Demo:hello.html.twig"));

        // line 9
        $context["code"] = $this->env->getExtension('demo')->getCode($this);
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_87796d6950d98e63798ffabb191bb92aeffd328d6ed6b62c6b3827cc9dbcee3a->leave($__internal_87796d6950d98e63798ffabb191bb92aeffd328d6ed6b62c6b3827cc9dbcee3a_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_088159c97a042ca39e45b84cfa2587b1ac62ddc5281b5e14cc479f927e6380d1 = $this->env->getExtension("native_profiler");
        $__internal_088159c97a042ca39e45b84cfa2587b1ac62ddc5281b5e14cc479f927e6380d1->enter($__internal_088159c97a042ca39e45b84cfa2587b1ac62ddc5281b5e14cc479f927e6380d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, ("Hello " . (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name"))), "html", null, true);
        
        $__internal_088159c97a042ca39e45b84cfa2587b1ac62ddc5281b5e14cc479f927e6380d1->leave($__internal_088159c97a042ca39e45b84cfa2587b1ac62ddc5281b5e14cc479f927e6380d1_prof);

    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        $__internal_7f4df6130e3439b1436d6d1a52d22165e00078f0bb0d1734ae9c211ae08761fd = $this->env->getExtension("native_profiler");
        $__internal_7f4df6130e3439b1436d6d1a52d22165e00078f0bb0d1734ae9c211ae08761fd->enter($__internal_7f4df6130e3439b1436d6d1a52d22165e00078f0bb0d1734ae9c211ae08761fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <h1>Hello ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "!</h1>
";
        
        $__internal_7f4df6130e3439b1436d6d1a52d22165e00078f0bb0d1734ae9c211ae08761fd->leave($__internal_7f4df6130e3439b1436d6d1a52d22165e00078f0bb0d1734ae9c211ae08761fd_prof);

    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle:Demo:hello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 6,  50 => 5,  38 => 3,  31 => 1,  29 => 9,  11 => 1,);
    }
}
/* {% extends "AcmeDemoBundle::layout.html.twig" %}*/
/* */
/* {% block title "Hello " ~ name %}*/
/* */
/* {% block content %}*/
/*     <h1>Hello {{ name }}!</h1>*/
/* {% endblock %}*/
/* */
/* {% set code = code(_self) %}*/
/* */
