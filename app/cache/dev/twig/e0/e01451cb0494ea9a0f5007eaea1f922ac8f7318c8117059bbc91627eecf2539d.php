<?php

/* AcmeDemoBundle:Secured:layout.html.twig */
class __TwigTemplate_e11197ed03c70d01579eae2294b018f37d479fd53768f1ccf62bca10dce852b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AcmeDemoBundle::layout.html.twig", "AcmeDemoBundle:Secured:layout.html.twig", 1);
        $this->blocks = array(
            'content_header_more' => array($this, 'block_content_header_more'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AcmeDemoBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_99e01877392cf08e2ccf09c3557bfd222f79c3b2a7c0870f933bac65110c389a = $this->env->getExtension("native_profiler");
        $__internal_99e01877392cf08e2ccf09c3557bfd222f79c3b2a7c0870f933bac65110c389a->enter($__internal_99e01877392cf08e2ccf09c3557bfd222f79c3b2a7c0870f933bac65110c389a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AcmeDemoBundle:Secured:layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_99e01877392cf08e2ccf09c3557bfd222f79c3b2a7c0870f933bac65110c389a->leave($__internal_99e01877392cf08e2ccf09c3557bfd222f79c3b2a7c0870f933bac65110c389a_prof);

    }

    // line 3
    public function block_content_header_more($context, array $blocks = array())
    {
        $__internal_d764ee30c4be718f241baf3ee21b5f86d9d45198f2026c42ba12d191fb7d7039 = $this->env->getExtension("native_profiler");
        $__internal_d764ee30c4be718f241baf3ee21b5f86d9d45198f2026c42ba12d191fb7d7039->enter($__internal_d764ee30c4be718f241baf3ee21b5f86d9d45198f2026c42ba12d191fb7d7039_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header_more"));

        // line 4
        echo "    ";
        $this->displayParentBlock("content_header_more", $context, $blocks);
        echo "
    <li>logged in as <strong>";
        // line 5
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array())) ? ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())) : ("Anonymous")), "html", null, true);
        echo "</strong> - <a href=\"";
        echo $this->env->getExtension('routing')->getPath("_demo_logout");
        echo "\">Logout</a></li>
";
        
        $__internal_d764ee30c4be718f241baf3ee21b5f86d9d45198f2026c42ba12d191fb7d7039->leave($__internal_d764ee30c4be718f241baf3ee21b5f86d9d45198f2026c42ba12d191fb7d7039_prof);

    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle:Secured:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 5,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "AcmeDemoBundle::layout.html.twig" %}*/
/* */
/* {% block content_header_more %}*/
/*     {{ parent() }}*/
/*     <li>logged in as <strong>{{ app.user ? app.user.username : 'Anonymous' }}</strong> - <a href="{{ path('_demo_logout') }}">Logout</a></li>*/
/* {% endblock %}*/
/* */
