<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_3bf310ca7fe751fc469908db0ebf4f3099dc36ba9ee630d7c36e2450bd409615 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_db4ed3d2b08c3cbe63a4fe339adea5d7a01b6c90069cb7f441d7bd17a6a74a3f = $this->env->getExtension("native_profiler");
        $__internal_db4ed3d2b08c3cbe63a4fe339adea5d7a01b6c90069cb7f441d7bd17a6a74a3f->enter($__internal_db4ed3d2b08c3cbe63a4fe339adea5d7a01b6c90069cb7f441d7bd17a6a74a3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("TwigBundle:Exception:exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_db4ed3d2b08c3cbe63a4fe339adea5d7a01b6c90069cb7f441d7bd17a6a74a3f->leave($__internal_db4ed3d2b08c3cbe63a4fe339adea5d7a01b6c90069cb7f441d7bd17a6a74a3f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include 'TwigBundle:Exception:exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
