<?php

/* MyAppAdminBundle:Admin:statistique.html.twig */
class __TwigTemplate_29106aa15e17a9c4e2145816df0792573172ee1545d3212a4b68bc8f85caa268 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MyAppAdminBundle::layout.html.twig", "MyAppAdminBundle:Admin:statistique.html.twig", 1);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MyAppAdminBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f23498b672b15167892003e946e1f1f28d19260e26bcaabf4d691d8cd6d48bc6 = $this->env->getExtension("native_profiler");
        $__internal_f23498b672b15167892003e946e1f1f28d19260e26bcaabf4d691d8cd6d48bc6->enter($__internal_f23498b672b15167892003e946e1f1f28d19260e26bcaabf4d691d8cd6d48bc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppAdminBundle:Admin:statistique.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f23498b672b15167892003e946e1f1f28d19260e26bcaabf4d691d8cd6d48bc6->leave($__internal_f23498b672b15167892003e946e1f1f28d19260e26bcaabf4d691d8cd6d48bc6_prof);

    }

    // line 2
    public function block_container($context, array $blocks = array())
    {
        $__internal_e2e026064505e9a2ed830354eab87d550712929ea69f9714e24e74a91078aa19 = $this->env->getExtension("native_profiler");
        $__internal_e2e026064505e9a2ed830354eab87d550712929ea69f9714e24e74a91078aa19->enter($__internal_e2e026064505e9a2ed830354eab87d550712929ea69f9714e24e74a91078aa19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 3
        echo "    statistique
";
        
        $__internal_e2e026064505e9a2ed830354eab87d550712929ea69f9714e24e74a91078aa19->leave($__internal_e2e026064505e9a2ed830354eab87d550712929ea69f9714e24e74a91078aa19_prof);

    }

    public function getTemplateName()
    {
        return "MyAppAdminBundle:Admin:statistique.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "MyAppAdminBundle::layout.html.twig" %}*/
/* {% block container %}*/
/*     statistique*/
/* {% endblock container %}*/
