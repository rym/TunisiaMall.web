<?php

/* MyAppUserBundle:Resetting:passwordAlreadyRequested.html.twig */
class __TwigTemplate_831a7d7c2f528c37cd9bfb8a3b912af66d9b52adc5c3ebd4a7df6df777f4b18a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "MyAppUserBundle:Resetting:passwordAlreadyRequested.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_310eb765663c60ead414681b58398bd8f83c4827c684957bb6b3c9f3bd8b9599 = $this->env->getExtension("native_profiler");
        $__internal_310eb765663c60ead414681b58398bd8f83c4827c684957bb6b3c9f3bd8b9599->enter($__internal_310eb765663c60ead414681b58398bd8f83c4827c684957bb6b3c9f3bd8b9599_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Resetting:passwordAlreadyRequested.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_310eb765663c60ead414681b58398bd8f83c4827c684957bb6b3c9f3bd8b9599->leave($__internal_310eb765663c60ead414681b58398bd8f83c4827c684957bb6b3c9f3bd8b9599_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_7872975df2fd1fd8f7f57ffa4b872f6b77e289ff93e6b60dda70c4011835f033 = $this->env->getExtension("native_profiler");
        $__internal_7872975df2fd1fd8f7f57ffa4b872f6b77e289ff93e6b60dda70c4011835f033->enter($__internal_7872975df2fd1fd8f7f57ffa4b872f6b77e289ff93e6b60dda70c4011835f033_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.password_already_requested", array(), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_7872975df2fd1fd8f7f57ffa4b872f6b77e289ff93e6b60dda70c4011835f033->leave($__internal_7872975df2fd1fd8f7f57ffa4b872f6b77e289ff93e6b60dda70c4011835f033_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Resetting:passwordAlreadyRequested.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* <p>{{ 'resetting.password_already_requested'|trans }}</p>*/
/* {% endblock fos_user_content %}*/
/* */
