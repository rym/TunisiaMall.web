<?php

/* MyAppUserBundle:Registration:email.txt.twig */
class __TwigTemplate_ad9f6986ac112e44ba04644601449f20c01b7484f0c73f7bb4cdc0494cfebd35 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5f680c65f16e421b28f0fa0e8941ad63671f2b18702edceb6d280746b679bbab = $this->env->getExtension("native_profiler");
        $__internal_5f680c65f16e421b28f0fa0e8941ad63671f2b18702edceb6d280746b679bbab->enter($__internal_5f680c65f16e421b28f0fa0e8941ad63671f2b18702edceb6d280746b679bbab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_5f680c65f16e421b28f0fa0e8941ad63671f2b18702edceb6d280746b679bbab->leave($__internal_5f680c65f16e421b28f0fa0e8941ad63671f2b18702edceb6d280746b679bbab_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_34ccc29133aea6456171b795833bb64c1361ecf2a5535b36724bb4b98de92de1 = $this->env->getExtension("native_profiler");
        $__internal_34ccc29133aea6456171b795833bb64c1361ecf2a5535b36724bb4b98de92de1->enter($__internal_34ccc29133aea6456171b795833bb64c1361ecf2a5535b36724bb4b98de92de1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_34ccc29133aea6456171b795833bb64c1361ecf2a5535b36724bb4b98de92de1->leave($__internal_34ccc29133aea6456171b795833bb64c1361ecf2a5535b36724bb4b98de92de1_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_284c9f9ddead296af0464836fa797c4c7a6a84eb96e54fa3b10b2f9a97eeaa4a = $this->env->getExtension("native_profiler");
        $__internal_284c9f9ddead296af0464836fa797c4c7a6a84eb96e54fa3b10b2f9a97eeaa4a->enter($__internal_284c9f9ddead296af0464836fa797c4c7a6a84eb96e54fa3b10b2f9a97eeaa4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_284c9f9ddead296af0464836fa797c4c7a6a84eb96e54fa3b10b2f9a97eeaa4a->leave($__internal_284c9f9ddead296af0464836fa797c4c7a6a84eb96e54fa3b10b2f9a97eeaa4a_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_a56a3c5e2c2deffe4a70a961736e2e6622bb033bc2426b8b52e2a5ed3c4251e0 = $this->env->getExtension("native_profiler");
        $__internal_a56a3c5e2c2deffe4a70a961736e2e6622bb033bc2426b8b52e2a5ed3c4251e0->enter($__internal_a56a3c5e2c2deffe4a70a961736e2e6622bb033bc2426b8b52e2a5ed3c4251e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_a56a3c5e2c2deffe4a70a961736e2e6622bb033bc2426b8b52e2a5ed3c4251e0->leave($__internal_a56a3c5e2c2deffe4a70a961736e2e6622bb033bc2426b8b52e2a5ed3c4251e0_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
