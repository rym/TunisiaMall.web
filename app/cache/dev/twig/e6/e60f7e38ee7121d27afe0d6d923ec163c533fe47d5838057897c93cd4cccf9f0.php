<?php

/* FOSUserBundle:Registration:register_content.html.twig */
class __TwigTemplate_e1108befe41269320390e7e3f203ee35e7a52f94686377b294e802aad9dee313 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7ed4eec9bf075b920c172bcff1614f1d873f4c5dd8ea852bbca3a183d8ad0972 = $this->env->getExtension("native_profiler");
        $__internal_7ed4eec9bf075b920c172bcff1614f1d873f4c5dd8ea852bbca3a183d8ad0972->enter($__internal_7ed4eec9bf075b920c172bcff1614f1d873f4c5dd8ea852bbca3a183d8ad0972_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register_content.html.twig"));

        // line 2
        echo "  <div class=\"container text-center\">

 

    
    <h1><center>Inscription</center></h1>
    <form action=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
        echo "\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " method=\"POST\" class=\"fos_user_registration_register\" style=\"vertical-align: middle; alignment-adjust: middle; padding-left: 40%; padding-right: 25%;>
        <div class=\"form-group\">
            <div class=\"input-group\">
                <span class=\"input-group-addon glyphicon glyphicon-user\"></span>
                ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Saisir un Username")));
        echo "                
            </div>
            <div class=\"input-group\">
                <span class=\"input-group-addon glyphicon glyphicon-user\"></span>
                ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Saisir votre Nom")));
        echo "                
            </div>
            <div class=\"input-group\">
                <span class=\"input-group-addon glyphicon glyphicon-user\"></span>
                ";
        // line 20
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Saisir votre Prenom")));
        echo "                
            </div>
            <div class=\"input-group\">
                <span class=\"input-group-addon glyphicon glyphicon-envelope\"></span>
                ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Saisir votre @ Email")));
        echo "                
            </div>
            <div class=\"input-group\">
                <span class=\"input-group-addon glyphicon glyphicon-eye-close\"></span>
                ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Saisir votre Mot de Passe")));
        echo "                
            </div>
            <div class=\"input-group\">
                <span class=\"input-group-addon glyphicon glyphicon-eye-close\"></span>
                ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Repeter votre Mot de Passe")));
        echo "                
            </div>
            <div class=\"input-group\">
                <span class=\"input-group-addon glyphicon glyphicon-flag\"></span>
                ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "Pays", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "                
            </div>
             <input type=\"submit\" class=\"btn btn-success\" value=\"Inscription\" />
            ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo " 
        </div>
        <div>
           
        </div>
    </form>
</div>";
        
        $__internal_7ed4eec9bf075b920c172bcff1614f1d873f4c5dd8ea852bbca3a183d8ad0972->leave($__internal_7ed4eec9bf075b920c172bcff1614f1d873f4c5dd8ea852bbca3a183d8ad0972_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 39,  81 => 36,  74 => 32,  67 => 28,  60 => 24,  53 => 20,  46 => 16,  39 => 12,  30 => 8,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/*   <div class="container text-center">*/
/* */
/*  */
/* */
/*     */
/*     <h1><center>Inscription</center></h1>*/
/*     <form action="{{ path('fos_user_registration_register') }}" {{ form_enctype(form) }} method="POST" class="fos_user_registration_register" style="vertical-align: middle; alignment-adjust: middle; padding-left: 40%; padding-right: 25%;>*/
/*         <div class="form-group">*/
/*             <div class="input-group">*/
/*                 <span class="input-group-addon glyphicon glyphicon-user"></span>*/
/*                 {{ form_widget(form.username, { 'attr': {'class': 'form-control', 'placeholder': 'Saisir un Username'} }) }}                */
/*             </div>*/
/*             <div class="input-group">*/
/*                 <span class="input-group-addon glyphicon glyphicon-user"></span>*/
/*                 {{ form_widget(form.nom, { 'attr': {'class': 'form-control', 'placeholder': 'Saisir votre Nom'} }) }}                */
/*             </div>*/
/*             <div class="input-group">*/
/*                 <span class="input-group-addon glyphicon glyphicon-user"></span>*/
/*                 {{ form_widget(form.prenom, { 'attr': {'class': 'form-control', 'placeholder': 'Saisir votre Prenom'} }) }}                */
/*             </div>*/
/*             <div class="input-group">*/
/*                 <span class="input-group-addon glyphicon glyphicon-envelope"></span>*/
/*                 {{ form_widget(form.email, { 'attr': {'class': 'form-control', 'placeholder': 'Saisir votre @ Email'} }) }}                */
/*             </div>*/
/*             <div class="input-group">*/
/*                 <span class="input-group-addon glyphicon glyphicon-eye-close"></span>*/
/*                 {{ form_widget(form.plainPassword.first, { 'attr': {'class': 'form-control', 'placeholder': 'Saisir votre Mot de Passe'} }) }}                */
/*             </div>*/
/*             <div class="input-group">*/
/*                 <span class="input-group-addon glyphicon glyphicon-eye-close"></span>*/
/*                 {{ form_widget(form.plainPassword.second, { 'attr': {'class': 'form-control', 'placeholder': 'Repeter votre Mot de Passe'} }) }}                */
/*             </div>*/
/*             <div class="input-group">*/
/*                 <span class="input-group-addon glyphicon glyphicon-flag"></span>*/
/*                 {{ form_widget(form.Pays, { 'attr': {'class': 'form-control'} }) }}                */
/*             </div>*/
/*              <input type="submit" class="btn btn-success" value="Inscription" />*/
/*             {{form_rest(form)}} */
/*         </div>*/
/*         <div>*/
/*            */
/*         </div>*/
/*     </form>*/
/* </div>*/
