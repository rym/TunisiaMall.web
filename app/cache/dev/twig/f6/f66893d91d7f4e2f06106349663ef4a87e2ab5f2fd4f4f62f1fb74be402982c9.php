<?php

/* MyAppUserBundle:Resetting:request.html.twig */
class __TwigTemplate_95ed6b39584dd6633a8381b5ee41908c2942c8cc114750017e5441b60da3303a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "MyAppUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bb63aa6a78123cbc5d912da22983c8479261aadb3ee2acec28530f4945307b86 = $this->env->getExtension("native_profiler");
        $__internal_bb63aa6a78123cbc5d912da22983c8479261aadb3ee2acec28530f4945307b86->enter($__internal_bb63aa6a78123cbc5d912da22983c8479261aadb3ee2acec28530f4945307b86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bb63aa6a78123cbc5d912da22983c8479261aadb3ee2acec28530f4945307b86->leave($__internal_bb63aa6a78123cbc5d912da22983c8479261aadb3ee2acec28530f4945307b86_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6f3d54acb8a35a2eabb1c54717721623c4bb8fca292460695d21cb6b8d426fd1 = $this->env->getExtension("native_profiler");
        $__internal_6f3d54acb8a35a2eabb1c54717721623c4bb8fca292460695d21cb6b8d426fd1->enter($__internal_6f3d54acb8a35a2eabb1c54717721623c4bb8fca292460695d21cb6b8d426fd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:request_content.html.twig", "MyAppUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_6f3d54acb8a35a2eabb1c54717721623c4bb8fca292460695d21cb6b8d426fd1->leave($__internal_6f3d54acb8a35a2eabb1c54717721623c4bb8fca292460695d21cb6b8d426fd1_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:request_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
