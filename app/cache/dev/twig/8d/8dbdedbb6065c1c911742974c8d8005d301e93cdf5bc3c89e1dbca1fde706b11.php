<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_b0127423070943a2bf955c87167dce34832c5ba015f0dbf9e1fde219092dc208 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1667661de216f27d6e414b678566c580718cd08f31cececdf213a8483f416a6a = $this->env->getExtension("native_profiler");
        $__internal_1667661de216f27d6e414b678566c580718cd08f31cececdf213a8483f416a6a->enter($__internal_1667661de216f27d6e414b678566c580718cd08f31cececdf213a8483f416a6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("TwigBundle:Exception:exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_1667661de216f27d6e414b678566c580718cd08f31cececdf213a8483f416a6a->leave($__internal_1667661de216f27d6e414b678566c580718cd08f31cececdf213a8483f416a6a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include 'TwigBundle:Exception:exception.xml.twig' with { 'exception': exception } %}*/
/* */
