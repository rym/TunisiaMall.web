<?php

/* MyAppUserBundle:Profile:profileResponsable.html.twig */
class __TwigTemplate_d23062ee54c6c33174fb8d86406671a67afc029e946ce9077c7a1a48359a71b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'container' => array($this, 'block_container'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ff683ef9960571d759691622a6e0d37c3e15353fcc3fb6ce8e0491a7df31f1d1 = $this->env->getExtension("native_profiler");
        $__internal_ff683ef9960571d759691622a6e0d37c3e15353fcc3fb6ce8e0491a7df31f1d1->enter($__internal_ff683ef9960571d759691622a6e0d37c3e15353fcc3fb6ce8e0491a7df31f1d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Profile:profileResponsable.html.twig"));

        // line 2
        $this->displayBlock('container', $context, $blocks);
        
        $__internal_ff683ef9960571d759691622a6e0d37c3e15353fcc3fb6ce8e0491a7df31f1d1->leave($__internal_ff683ef9960571d759691622a6e0d37c3e15353fcc3fb6ce8e0491a7df31f1d1_prof);

    }

    public function block_container($context, array $blocks = array())
    {
        $__internal_4c15d8a02fc0c91c7dee07fd8fc096fefe2a02284cf4554e79706eaa85e4d764 = $this->env->getExtension("native_profiler");
        $__internal_4c15d8a02fc0c91c7dee07fd8fc096fefe2a02284cf4554e79706eaa85e4d764->enter($__internal_4c15d8a02fc0c91c7dee07fd8fc096fefe2a02284cf4554e79706eaa85e4d764_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 3
        echo "
    <html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"fr\" xmlns:fb=\"http://www.facebook.com/2008/fbml\">
        <head>
            <title>Tunisa Malll Badalhom lkol </title>
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
                <meta name=\"keywords\" content=\"Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
                      Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\" />
                <script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
                <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
                <!-- Custom Theme files -->
                <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/style.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
                <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 
                    <script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
                    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
                    <meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />
                    <meta charset=\"UTF-8\">

                        ";
        // line 21
        $this->displayBlock('title', $context, $blocks);
        // line 22
        echo "                        ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 36
        echo "                        <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/js/a8de5014480b4daa57965aee6265098c_1452767442.js"), "html", null, true);
        echo "\" charset=\"utf-8\"></script>
                        <script type=\"text/javascript\" src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/js/e644c8cbc8bd826f9f922db7d95f32b4_1446657238.js"), "html", null, true);
        echo "\" charset=\"utf-8\"></script>
                        </head>
                        <body>
                            <div id=\"container\" class=\"\" >
                                <header class=\"header-home\"><!--header site global-->
                                    <div class=\"int\">
                                        <div id=\"logo\">
                                            <a href=\"Creteil-Soleil.html\">
                                                <img src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/images/logo.png"), "html", null, true);
        echo "\"   style=\"\" alt=\"Créteil Soleil\" title=\"Créteil Soleil\" />
                                            </a>
                                        </div>

                                        <ul  class=\"rslides rslides1\"  id=\"opening\"><!--NEW BLOC horaires-->
                                            <li class=\"\" style=\"display: list-item; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;\" id=\"rslides1_s0\">
                                                <a href=\"Creteil-Soleil/Horaires-Acces.html\">

                                                </a>
                                            </li>
                                        </ul><!--FIN NEW BLOC-->
                                    </div>
                                    <span id=\"accessMobile\">Menu</span>
                                    <div id=\"navigation\"><!--englobe les 2 nav pour responsive-->
                                        <nav id=\"mainNav\"><!--navigation principale site-->
                                            <ul class=\"int\">
                                                <li>
                                                    <a href=\"#\">Acceuil</a>
                                                </li>
                                                <li>
                                                    <a href=\"#\">Boutiques</a>
                                                </li>
                                                <li>
                                                    <a href=\"#\">Catalogues</a>
                                                </li>
                                            </ul>
                                        </nav>
                                        <div id=\"topBarSite\"><!--2eme nav et reseaux sociaux-->
                                            <div class=\"int\">
                                                <ul id=\"secondaryNav\"><!--New BLOC-->

                                                    <li>
                                                        <a href=\"https://www.facebook.com/TunisiaMall/app/412175208980168/\" >Horaires de 9h à 22h & Accès</a>
                                                    </li>

                                                    <li>
                                                        <a href=\"https://www.facebook.com/TunisiaMall/app/1082508541773591/\" >Plan du centre</a>
                                                    </li>

                                                  

                                                </ul><!--New BLOC-->
                                                <ul class=\"blocSocialMedia\">
                                                    <li class=\"facebook\">
                                                        <a href=\"#\" rel=\"nofollow\" target=\"_blank\">Facebook</a>
                                                        <div class=\"contentbloc\">
                                                            <a  href=\"#\">Follow @Facebook</a>
                                                        </div>
                                                    </li>
                                                    <li class=\"twitter\">
                                                        <a href=\"#\" rel=\"nofollow\" target=\"_blank\">Twitter</a>
                                                        <div class=\"contentbloc\">
                                                            <a href=\"#\">Follow @twitter</a>
                                                        </div>
                                                    </li>
                                                    <li class=\"instagram\">
                                                        <a href=\"#\" target=\"_blank\" rel=\"nofollow\" >Instagram</a>
                                                        <div class=\"contentbloc\">
                                                            <a href=\"#\">Follow @instagram</a>
                                                    </li>
                                                </ul>        
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            </header>

                            ";
        // line 114
        echo "
                            <div class=\"login-bottom\">
                                <center><h2>Modifier profile</h2></center>

                                <form action=\"";
        // line 118
        echo $this->env->getExtension('routing')->getPath("my_app_profile");
        echo "\"";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " method=\"POST\">  
                                    ";
        // line 119
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token", array()), 'row');
        echo "
                                    <div class=\"login-mail\">
                                        ";
        // line 121
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget', array("attr" => array("placeholder" => "Nom")));
        echo "
                                        <i class=\"fa fa-user\"></i>
                                    </div>
                                    <div class=\"login-mail\">
                                        ";
        // line 125
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget', array("attr" => array("placeholder" => "Prenom")));
        echo "
                                        <i class=\"fa fa-user\"></i>
                                    </div>
                                    <div class=\"login-mail\">
                                        ";
        // line 129
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("placeholder" => "Nom dutilisateur")));
        echo "
                                        <i class=\"fa fa-user\"></i>
                                    </div>
                                    <div class=\"login-mail\">
                                        ";
        // line 133
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("placeholder" => "E-mail")));
        echo "
                                        <i class=\"fa fa-envelope\"></i>
                                    </div>
                                        
                                    <div class=\"login-do\">
                                        <label class=\"hvr-shutter-in-horizontal login-sub\">
                                            <a href=\"";
        // line 139
        echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
        echo "\" >Modifier mot de passe</a>
                                        </label>
                                    </div>
                                    <br>

                                    <div class=\"login-do\">
                                        <label class=\"hvr-shutter-in-horizontal login-sub\">
                                            <input type=\"submit\" id=\"_submit\" class=\"btn btn-success\" name=\"_submit\" value=\"Modifier\">
                                        </label>
                                    </div>
                                </form>
                            </div>
                            </div>

                            <style>
                                .login-bottom{
                                    height: 400px;
                                }
                            </style>
                            <!---->

                            <div class=\"copy-right\">
                                <p> &copy; 2016 Tunisia Mall. All Rights Reserved | <a href=\"#\" target=\"_blank\">Tunisia Mall</a> </p>       </div>  
                            <!--scrolling js-->
                            <script src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.nicescroll.js"), "html", null, true);
        echo "\"></script>
                            <script src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/scripts.js"), "html", null, true);
        echo "\"></script>
                            <!--//scrolling js-->
                            <div id=\"container\" class=\"\" >
                                <footer ><!--footer-->
                                    <div class=\"left\">
                                        <h2>Newsletter Tunisia Mall</h2>
                                        <p>Pour être informé sur:</p>
                                        <ul>
                                            <li>Les sorties de nouvelles collections</li>
                                            <li>Vos marques préférées</li>
                                        </ul>


                                        <form action=\"#\" id=\"newsletter_form\" method=\"post\">
                                            <fieldset>
                                                <input type=\"text\" name=\"email\" value=\"Votre email\" data-init-value=\"Votre email\" />
                                                <input type=\"hidden\" name=\"centreId\" value=\"399\"/>
                                                <input type=\"hidden\" name=\"centreName\" value=\"Créteil Soleil\"/>
                                                <input type=\"submit\" value=\"ok\" />
                                            </fieldset>
                                        </form>
                                    </div>

                                    <div class=\"right\">
                                        <h2>Application Mobile Tunisia Mall</h2>
                                        <p>Retrouvez votre centre Tunisia Mall sur votre smartphone et vos tablettes !</p>
                                        <a class=\"apple\" target=\"_blank\" href=\"#\"onClick=\"return xt_click(this, 'C', '75', 'Depuis_lien_footer:App_store', 'N');
                                                return false;\">Appstore</a>
                                        <a class=\"google\" target=\"_blank\" href=\"#\" onClick=\"return xt_click(this, 'C', '75', 'Depuis_lien_footer:Google_Play', 'N');
                                                return false;\"> Google play</a>
                                    </div>


                                    <nav>
                                        <ul>
                                            <li><a href=\"Creteil-Soleil/Nous-contacter.html\" title=\"Nous contacter\">Nous contacter</a></li>
                                            <li><a href=\"Creteil-Soleil/Mentions-Legales.html\" title=\"Mentions Légales\">Mentions Légales</a></li>
                                            <li><a href=\"Creteil-Soleil/Louer-un-emplacement-temporaire.html\" title=\"Louer un emplacement temporaire\">Visite guidee</a></li>
                                        </ul>
                                    </nav>


                                    <ul class=\"blocSocialMedia\">
                                        <li class=\"facebook\">
                                            <a href=\"#\" target=\"_blank\" rel=\"nofollow\">Facebook</a>
                                        </li>
                                        <li class=\"twitter\">
                                            <a href=\"#\" target=\"_blank\" rel=\"nofollow\">Twitter</a>
                                        </li>
                                        <li class=\"instagram\">
                                            <a href=\"#\" target=\"_blank\" rel=\"nofollow\" >Instagram</a>
                                        </li>
                                    </ul>

                                </footer><!--Fin footer-->        
                            </div>
                        </body>
                        </html>
                    ";
        
        $__internal_4c15d8a02fc0c91c7dee07fd8fc096fefe2a02284cf4554e79706eaa85e4d764->leave($__internal_4c15d8a02fc0c91c7dee07fd8fc096fefe2a02284cf4554e79706eaa85e4d764_prof);

    }

    // line 21
    public function block_title($context, array $blocks = array())
    {
        $__internal_5e8c7da29f0f0225fea5074f8a538f69fd34be647aceb220f5b09733c78fb3c8 = $this->env->getExtension("native_profiler");
        $__internal_5e8c7da29f0f0225fea5074f8a538f69fd34be647aceb220f5b09733c78fb3c8->enter($__internal_5e8c7da29f0f0225fea5074f8a538f69fd34be647aceb220f5b09733c78fb3c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "<title>Tunisia Mall</title>";
        
        $__internal_5e8c7da29f0f0225fea5074f8a538f69fd34be647aceb220f5b09733c78fb3c8->leave($__internal_5e8c7da29f0f0225fea5074f8a538f69fd34be647aceb220f5b09733c78fb3c8_prof);

    }

    // line 22
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_a4625c0b6cf2caa8cbf2911b0ca1c36da7c5475e847de026d91e8b6b3fb69708 = $this->env->getExtension("native_profiler");
        $__internal_a4625c0b6cf2caa8cbf2911b0ca1c36da7c5475e847de026d91e8b6b3fb69708->enter($__internal_a4625c0b6cf2caa8cbf2911b0ca1c36da7c5475e847de026d91e8b6b3fb69708_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 23
        echo "
                            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/6c6c9e8995c7ee151b5a1c238da4a176_1453471972_all.css"), "html", null, true);
        echo "\" />
                            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/mobile.css"), "html", null, true);
        echo "\" media=\"screen and (max-width:1100px)\" />
                            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/taggage.css"), "html", null, true);
        echo "\" />
                            <style type=\"text/css\">        
                                #mainNav >ul >li >a:hover,#accessCatalogue > li .active,#mainNav >ul >.active >a,.back .add-wishlist,.scroll-list
                                {background-color:#bb9854;}
                                .cat,#opening a,#sidebar-nav strong,.listingCatalogue li h3, .listingCatalogue li h2,.fidelityContent p a,#modal-inscription span,.globalInfos h3,.front             .club-offer strong,.back .club-offer h2, #modal-add-wishlist p,#modal-shopping-alert p,#slider h2 .first,.backlinks a
                                {color:#bb9854;}
                                li.soon {border-color:#bb9854 !important;}
                                li.soon .mask-bonplan, li.soon .accessOffer a.access-infos{background-color:#bb9854;}
                            </style>
                        ";
        
        $__internal_a4625c0b6cf2caa8cbf2911b0ca1c36da7c5475e847de026d91e8b6b3fb69708->leave($__internal_a4625c0b6cf2caa8cbf2911b0ca1c36da7c5475e847de026d91e8b6b3fb69708_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Profile:profileResponsable.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  337 => 26,  333 => 25,  329 => 24,  326 => 23,  320 => 22,  308 => 21,  242 => 164,  238 => 163,  211 => 139,  202 => 133,  195 => 129,  188 => 125,  181 => 121,  176 => 119,  170 => 118,  164 => 114,  93 => 45,  82 => 37,  77 => 36,  74 => 22,  72 => 21,  65 => 17,  61 => 16,  57 => 15,  53 => 14,  48 => 12,  37 => 3,  25 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% block container %}*/
/* */
/*     <html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xmlns:fb="http://www.facebook.com/2008/fbml">*/
/*         <head>*/
/*             <title>Tunisa Malll Badalhom lkol </title>*/
/*             <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*                 <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, */
/*                       Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />*/
/*                 <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>*/
/*                 <link href="{{asset('bundles/myappadmin/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />*/
/*                 <!-- Custom Theme files -->*/
/*                 <link href="{{asset('bundles/myappadmin/css/style.css')}}" rel='stylesheet' type='text/css' />*/
/*                 <link href="{{asset('bundles/myappadmin/css/font-awesome.css')}}" rel="stylesheet"> */
/*                     <script src="{{asset('bundles/myappadmin/js/jquery.min.js')}}"></script>*/
/*                     <script src="{{asset('bundles/myappadmin/js/bootstrap.min.js')}}"></script>*/
/*                     <meta http-equiv="content-type" content="text/html;charset=utf-8" />*/
/*                     <meta charset="UTF-8">*/
/* */
/*                         {% block title %}<title>Tunisia Mall</title>{% endblock %}*/
/*                         {% block stylesheets %}*/
/* */
/*                             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/6c6c9e8995c7ee151b5a1c238da4a176_1453471972_all.css') }}" />*/
/*                             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/mobile.css') }}" media="screen and (max-width:1100px)" />*/
/*                             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/taggage.css') }}" />*/
/*                             <style type="text/css">        */
/*                                 #mainNav >ul >li >a:hover,#accessCatalogue > li .active,#mainNav >ul >.active >a,.back .add-wishlist,.scroll-list*/
/*                                 {background-color:#bb9854;}*/
/*                                 .cat,#opening a,#sidebar-nav strong,.listingCatalogue li h3, .listingCatalogue li h2,.fidelityContent p a,#modal-inscription span,.globalInfos h3,.front             .club-offer strong,.back .club-offer h2, #modal-add-wishlist p,#modal-shopping-alert p,#slider h2 .first,.backlinks a*/
/*                                 {color:#bb9854;}*/
/*                                 li.soon {border-color:#bb9854 !important;}*/
/*                                 li.soon .mask-bonplan, li.soon .accessOffer a.access-infos{background-color:#bb9854;}*/
/*                             </style>*/
/*                         {% endblock %}*/
/*                         <script type="text/javascript" src="{{ asset('bundles/myappuser/js/a8de5014480b4daa57965aee6265098c_1452767442.js') }}" charset="utf-8"></script>*/
/*                         <script type="text/javascript" src="{{ asset('bundles/myappuser/js/e644c8cbc8bd826f9f922db7d95f32b4_1446657238.js') }}" charset="utf-8"></script>*/
/*                         </head>*/
/*                         <body>*/
/*                             <div id="container" class="" >*/
/*                                 <header class="header-home"><!--header site global-->*/
/*                                     <div class="int">*/
/*                                         <div id="logo">*/
/*                                             <a href="Creteil-Soleil.html">*/
/*                                                 <img src="{{ asset('bundles/myappuser/images/logo.png') }}"   style="" alt="Créteil Soleil" title="Créteil Soleil" />*/
/*                                             </a>*/
/*                                         </div>*/
/* */
/*                                         <ul  class="rslides rslides1"  id="opening"><!--NEW BLOC horaires-->*/
/*                                             <li class="" style="display: list-item; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;" id="rslides1_s0">*/
/*                                                 <a href="Creteil-Soleil/Horaires-Acces.html">*/
/* */
/*                                                 </a>*/
/*                                             </li>*/
/*                                         </ul><!--FIN NEW BLOC-->*/
/*                                     </div>*/
/*                                     <span id="accessMobile">Menu</span>*/
/*                                     <div id="navigation"><!--englobe les 2 nav pour responsive-->*/
/*                                         <nav id="mainNav"><!--navigation principale site-->*/
/*                                             <ul class="int">*/
/*                                                 <li>*/
/*                                                     <a href="#">Acceuil</a>*/
/*                                                 </li>*/
/*                                                 <li>*/
/*                                                     <a href="#">Boutiques</a>*/
/*                                                 </li>*/
/*                                                 <li>*/
/*                                                     <a href="#">Catalogues</a>*/
/*                                                 </li>*/
/*                                             </ul>*/
/*                                         </nav>*/
/*                                         <div id="topBarSite"><!--2eme nav et reseaux sociaux-->*/
/*                                             <div class="int">*/
/*                                                 <ul id="secondaryNav"><!--New BLOC-->*/
/* */
/*                                                     <li>*/
/*                                                         <a href="https://www.facebook.com/TunisiaMall/app/412175208980168/" >Horaires de 9h à 22h & Accès</a>*/
/*                                                     </li>*/
/* */
/*                                                     <li>*/
/*                                                         <a href="https://www.facebook.com/TunisiaMall/app/1082508541773591/" >Plan du centre</a>*/
/*                                                     </li>*/
/* */
/*                                                   */
/* */
/*                                                 </ul><!--New BLOC-->*/
/*                                                 <ul class="blocSocialMedia">*/
/*                                                     <li class="facebook">*/
/*                                                         <a href="#" rel="nofollow" target="_blank">Facebook</a>*/
/*                                                         <div class="contentbloc">*/
/*                                                             <a  href="#">Follow @Facebook</a>*/
/*                                                         </div>*/
/*                                                     </li>*/
/*                                                     <li class="twitter">*/
/*                                                         <a href="#" rel="nofollow" target="_blank">Twitter</a>*/
/*                                                         <div class="contentbloc">*/
/*                                                             <a href="#">Follow @twitter</a>*/
/*                                                         </div>*/
/*                                                     </li>*/
/*                                                     <li class="instagram">*/
/*                                                         <a href="#" target="_blank" rel="nofollow" >Instagram</a>*/
/*                                                         <div class="contentbloc">*/
/*                                                             <a href="#">Follow @instagram</a>*/
/*                                                     </li>*/
/*                                                 </ul>        */
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                             </div>*/
/* */
/*                             </header>*/
/* */
/*                             {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/*                             <div class="login-bottom">*/
/*                                 <center><h2>Modifier profile</h2></center>*/
/* */
/*                                 <form action="{{ path('my_app_profile') }}"{{ form_enctype(form) }} method="POST">  */
/*                                     {{ form_row(form._token) }}*/
/*                                     <div class="login-mail">*/
/*                                         {{ form_widget(form.nom,{ 'attr': {'placeholder': 'Nom'} }) }}*/
/*                                         <i class="fa fa-user"></i>*/
/*                                     </div>*/
/*                                     <div class="login-mail">*/
/*                                         {{ form_widget(form.prenom,{ 'attr': {'placeholder': 'Prenom'} }) }}*/
/*                                         <i class="fa fa-user"></i>*/
/*                                     </div>*/
/*                                     <div class="login-mail">*/
/*                                         {{ form_widget(form.username,{ 'attr': {'placeholder': 'Nom dutilisateur'} }) }}*/
/*                                         <i class="fa fa-user"></i>*/
/*                                     </div>*/
/*                                     <div class="login-mail">*/
/*                                         {{ form_widget(form.email,{ 'attr': {'placeholder': 'E-mail'} }) }}*/
/*                                         <i class="fa fa-envelope"></i>*/
/*                                     </div>*/
/*                                         */
/*                                     <div class="login-do">*/
/*                                         <label class="hvr-shutter-in-horizontal login-sub">*/
/*                                             <a href="{{path('fos_user_change_password')}}" >Modifier mot de passe</a>*/
/*                                         </label>*/
/*                                     </div>*/
/*                                     <br>*/
/* */
/*                                     <div class="login-do">*/
/*                                         <label class="hvr-shutter-in-horizontal login-sub">*/
/*                                             <input type="submit" id="_submit" class="btn btn-success" name="_submit" value="Modifier">*/
/*                                         </label>*/
/*                                     </div>*/
/*                                 </form>*/
/*                             </div>*/
/*                             </div>*/
/* */
/*                             <style>*/
/*                                 .login-bottom{*/
/*                                     height: 400px;*/
/*                                 }*/
/*                             </style>*/
/*                             <!---->*/
/* */
/*                             <div class="copy-right">*/
/*                                 <p> &copy; 2016 Tunisia Mall. All Rights Reserved | <a href="#" target="_blank">Tunisia Mall</a> </p>       </div>  */
/*                             <!--scrolling js-->*/
/*                             <script src="{{asset('bundles/myappadmin/js/jquery.nicescroll.js')}}"></script>*/
/*                             <script src="{{asset('bundles/myappadmin/js/scripts.js')}}"></script>*/
/*                             <!--//scrolling js-->*/
/*                             <div id="container" class="" >*/
/*                                 <footer ><!--footer-->*/
/*                                     <div class="left">*/
/*                                         <h2>Newsletter Tunisia Mall</h2>*/
/*                                         <p>Pour être informé sur:</p>*/
/*                                         <ul>*/
/*                                             <li>Les sorties de nouvelles collections</li>*/
/*                                             <li>Vos marques préférées</li>*/
/*                                         </ul>*/
/* */
/* */
/*                                         <form action="#" id="newsletter_form" method="post">*/
/*                                             <fieldset>*/
/*                                                 <input type="text" name="email" value="Votre email" data-init-value="Votre email" />*/
/*                                                 <input type="hidden" name="centreId" value="399"/>*/
/*                                                 <input type="hidden" name="centreName" value="Créteil Soleil"/>*/
/*                                                 <input type="submit" value="ok" />*/
/*                                             </fieldset>*/
/*                                         </form>*/
/*                                     </div>*/
/* */
/*                                     <div class="right">*/
/*                                         <h2>Application Mobile Tunisia Mall</h2>*/
/*                                         <p>Retrouvez votre centre Tunisia Mall sur votre smartphone et vos tablettes !</p>*/
/*                                         <a class="apple" target="_blank" href="#"onClick="return xt_click(this, 'C', '75', 'Depuis_lien_footer:App_store', 'N');*/
/*                                                 return false;">Appstore</a>*/
/*                                         <a class="google" target="_blank" href="#" onClick="return xt_click(this, 'C', '75', 'Depuis_lien_footer:Google_Play', 'N');*/
/*                                                 return false;"> Google play</a>*/
/*                                     </div>*/
/* */
/* */
/*                                     <nav>*/
/*                                         <ul>*/
/*                                             <li><a href="Creteil-Soleil/Nous-contacter.html" title="Nous contacter">Nous contacter</a></li>*/
/*                                             <li><a href="Creteil-Soleil/Mentions-Legales.html" title="Mentions Légales">Mentions Légales</a></li>*/
/*                                             <li><a href="Creteil-Soleil/Louer-un-emplacement-temporaire.html" title="Louer un emplacement temporaire">Visite guidee</a></li>*/
/*                                         </ul>*/
/*                                     </nav>*/
/* */
/* */
/*                                     <ul class="blocSocialMedia">*/
/*                                         <li class="facebook">*/
/*                                             <a href="#" target="_blank" rel="nofollow">Facebook</a>*/
/*                                         </li>*/
/*                                         <li class="twitter">*/
/*                                             <a href="#" target="_blank" rel="nofollow">Twitter</a>*/
/*                                         </li>*/
/*                                         <li class="instagram">*/
/*                                             <a href="#" target="_blank" rel="nofollow" >Instagram</a>*/
/*                                         </li>*/
/*                                     </ul>*/
/* */
/*                                 </footer><!--Fin footer-->        */
/*                             </div>*/
/*                         </body>*/
/*                         </html>*/
/*                     {% endblock container %}*/
