<?php

/* FOSUserBundle:ChangePassword:changePassword.html.twig */
class __TwigTemplate_8e37605e517b0b805c24d6f901031cf57b8a0a447c5058c8047e40f626c1c729 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'container' => array($this, 'block_container'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c34b1092159f02c3a58217c9d481646878b41edc37aaa70ee8714ee3c38f7462 = $this->env->getExtension("native_profiler");
        $__internal_c34b1092159f02c3a58217c9d481646878b41edc37aaa70ee8714ee3c38f7462->enter($__internal_c34b1092159f02c3a58217c9d481646878b41edc37aaa70ee8714ee3c38f7462_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:changePassword.html.twig"));

        // line 1
        echo "




";
        // line 7
        $this->displayBlock('container', $context, $blocks);
        
        $__internal_c34b1092159f02c3a58217c9d481646878b41edc37aaa70ee8714ee3c38f7462->leave($__internal_c34b1092159f02c3a58217c9d481646878b41edc37aaa70ee8714ee3c38f7462_prof);

    }

    public function block_container($context, array $blocks = array())
    {
        $__internal_c145492c1bfcc6ad696f12a2e1848ce414a2d0bac362653af25aeab3641940ac = $this->env->getExtension("native_profiler");
        $__internal_c145492c1bfcc6ad696f12a2e1848ce414a2d0bac362653af25aeab3641940ac->enter($__internal_c145492c1bfcc6ad696f12a2e1848ce414a2d0bac362653af25aeab3641940ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 8
        echo "
    <html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"fr\" xmlns:fb=\"http://www.facebook.com/2008/fbml\">
        <head>
            <title>Tunisa Malll Badalhom lkol </title>
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
                <meta name=\"keywords\" content=\"Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
                      Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\" />
                <script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
                <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
                <!-- Custom Theme files -->
                <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/style.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
                <link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 
                    <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
                    <script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
                    <meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />
                    <meta charset=\"UTF-8\">

                        ";
        // line 26
        $this->displayBlock('title', $context, $blocks);
        // line 27
        echo "                        ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 41
        echo "                        <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/js/a8de5014480b4daa57965aee6265098c_1452767442.js"), "html", null, true);
        echo "\" charset=\"utf-8\"></script>
                        <script type=\"text/javascript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/js/e644c8cbc8bd826f9f922db7d95f32b4_1446657238.js"), "html", null, true);
        echo "\" charset=\"utf-8\"></script>
                        </head>
                        <body>
                            <div id=\"container\" class=\"\" >
                                <header class=\"header-home\"><!--header site global-->
                                    <div class=\"int\">
                                        <div id=\"logo\">
                                            <a href=\"Creteil-Soleil.html\">
                                                <img src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/images/logo.png"), "html", null, true);
        echo "\"   style=\"\" alt=\"Créteil Soleil\" title=\"Créteil Soleil\" />
                                            </a>
                                        </div>

                                        <ul  class=\"rslides rslides1\"  id=\"opening\"><!--NEW BLOC horaires-->
                                            <li class=\"\" style=\"display: list-item; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;\" id=\"rslides1_s0\">
                                                <a href=\"Creteil-Soleil/Horaires-Acces.html\">

                                                </a>
                                            </li>
                                        </ul><!--FIN NEW BLOC-->
                                    </div>
                                    <span id=\"accessMobile\">Menu</span>
                                    <div id=\"navigation\"><!--englobe les 2 nav pour responsive-->
                                        <nav id=\"mainNav\"><!--navigation principale site-->
                                            <ul class=\"int\">
                                                <li>
                                                    <a href=\"#\">Acceuil</a>
                                                </li>
                                                <li>
                                                    <a href=\"#\">Boutiques</a>
                                                </li>
                                                <li>
                                                    <a href=\"#\">Catalogues</a>
                                                </li>
                                            </ul>
                                        </nav>
                                        <div id=\"topBarSite\"><!--2eme nav et reseaux sociaux-->
                                            <div class=\"int\">
                                                <ul id=\"secondaryNav\"><!--New BLOC-->

                                                    <li>
                                                        <a href=\"Creteil-Soleil/Horaires-Acces.html\" >Horaires & Accès</a>
                                                    </li>

                                                    <li>
                                                        <a href=\"Creteil-Soleil/Plan-du-centre.html\" >Plan du centre</a>
                                                    </li>

                                                    <li>
                                                        <a href=\"Creteil-Soleil/Services.html\" >News</a>
                                                    </li>


                                                </ul><!--New BLOC-->
                                                <ul class=\"blocSocialMedia\">
                                                    <li class=\"facebook\">
                                                        <a href=\"#\" rel=\"nofollow\" target=\"_blank\">Facebook</a>
                                                        <div class=\"contentbloc\">
                                                            <a  href=\"#\">Follow @Facebook</a>
                                                        </div>
                                                    </li>
                                                    <li class=\"twitter\">
                                                        <a href=\"#\" rel=\"nofollow\" target=\"_blank\">Twitter</a>
                                                        <div class=\"contentbloc\">
                                                            <a href=\"#\">Follow @twitter</a>
                                                        </div>
                                                    </li>
                                                    <li class=\"instagram\">
                                                        <a href=\"#\" target=\"_blank\" rel=\"nofollow\" >Instagram</a>
                                                        <div class=\"contentbloc\">
                                                            <a href=\"#\">Follow @instagram</a>
                                                    </li>
                                                </ul>        
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            </header>

                            ";
        // line 122
        echo "
                            <div class=\"login-bottom\">
                                <center><h2>Modifier Mot de passe</h2></center>
                                    ";
        // line 125
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 128
        echo "                            </div>
                            </div>

                            <style>
                                .login-bottom{
                                    height: 400px;
                                }
                            </style>
                            <!---->

                            <div class=\"copy-right\">
                                <p> &copy; 2016 Tunisia Mall. All Rights Reserved | <a href=\"#\" target=\"_blank\">Tunisia Mall</a> </p>       </div>  
                            <!--scrolling js-->
                            <script src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.nicescroll.js"), "html", null, true);
        echo "\"></script>
                            <script src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/scripts.js"), "html", null, true);
        echo "\"></script>
                            <!--//scrolling js-->
                            <div id=\"container\" class=\"\" >
                                <footer ><!--footer-->
                                    <div class=\"left\">
                                        <h2>Newsletter Tunisia Mall</h2>
                                        <p>Pour être informé sur:</p>
                                        <ul>
                                            <li>Les sorties de nouvelles collections</li>
                                            <li>Vos marques préférées</li>
                                        </ul>


                                        <form action=\"#\" id=\"newsletter_form\" method=\"post\">
                                            <fieldset>
                                                <input type=\"text\" name=\"email\" value=\"Votre email\" data-init-value=\"Votre email\" />
                                                <input type=\"hidden\" name=\"centreId\" value=\"399\"/>
                                                <input type=\"hidden\" name=\"centreName\" value=\"Créteil Soleil\"/>
                                                <input type=\"submit\" value=\"ok\" />
                                            </fieldset>
                                        </form>
                                    </div>

                                    <div class=\"right\">
                                        <h2>Application Mobile Tunisia Mall</h2>
                                        <p>Retrouvez votre centre Tunisia Mall sur votre smartphone et vos tablettes !</p>
                                        <a class=\"apple\" target=\"_blank\" href=\"#\"onClick=\"return xt_click(this, 'C', '75', 'Depuis_lien_footer:App_store', 'N');
                                                return false;\">Appstore</a>
                                        <a class=\"google\" target=\"_blank\" href=\"#\" onClick=\"return xt_click(this, 'C', '75', 'Depuis_lien_footer:Google_Play', 'N');
                                                return false;\"> Google play</a>
                                    </div>


                                    <nav>
                                        <ul>
                                            <li><a href=\"Creteil-Soleil/Nous-contacter.html\" title=\"Nous contacter\">Nous contacter</a></li>
                                            <li><a href=\"Creteil-Soleil/Mentions-Legales.html\" title=\"Mentions Légales\">Mentions Légales</a></li>
                                            <li><a href=\"Creteil-Soleil/Louer-un-emplacement-temporaire.html\" title=\"Louer un emplacement temporaire\">Visite guidee</a></li>
                                        </ul>
                                    </nav>


                                    <ul class=\"blocSocialMedia\">
                                        <li class=\"facebook\">
                                            <a href=\"#\" target=\"_blank\" rel=\"nofollow\">Facebook</a>
                                        </li>
                                        <li class=\"twitter\">
                                            <a href=\"#\" target=\"_blank\" rel=\"nofollow\">Twitter</a>
                                        </li>
                                        <li class=\"instagram\">
                                            <a href=\"#\" target=\"_blank\" rel=\"nofollow\" >Instagram</a>
                                        </li>
                                    </ul>

                                </footer><!--Fin footer-->        
                            </div>
                        </body>
                        </html>
                    ";
        
        $__internal_c145492c1bfcc6ad696f12a2e1848ce414a2d0bac362653af25aeab3641940ac->leave($__internal_c145492c1bfcc6ad696f12a2e1848ce414a2d0bac362653af25aeab3641940ac_prof);

    }

    // line 26
    public function block_title($context, array $blocks = array())
    {
        $__internal_31e31ada9e9505066a0ca5278481813eb56157496d05a63c7aa28a8e0114431a = $this->env->getExtension("native_profiler");
        $__internal_31e31ada9e9505066a0ca5278481813eb56157496d05a63c7aa28a8e0114431a->enter($__internal_31e31ada9e9505066a0ca5278481813eb56157496d05a63c7aa28a8e0114431a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "<title>Tunisia Mall</title>";
        
        $__internal_31e31ada9e9505066a0ca5278481813eb56157496d05a63c7aa28a8e0114431a->leave($__internal_31e31ada9e9505066a0ca5278481813eb56157496d05a63c7aa28a8e0114431a_prof);

    }

    // line 27
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_320574e16569a73046b31be4d678b7a8cf805019c89322365ec3518c10bc8602 = $this->env->getExtension("native_profiler");
        $__internal_320574e16569a73046b31be4d678b7a8cf805019c89322365ec3518c10bc8602->enter($__internal_320574e16569a73046b31be4d678b7a8cf805019c89322365ec3518c10bc8602_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 28
        echo "
                            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/6c6c9e8995c7ee151b5a1c238da4a176_1453471972_all.css"), "html", null, true);
        echo "\" />
                            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/mobile.css"), "html", null, true);
        echo "\" media=\"screen and (max-width:1100px)\" />
                            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/taggage.css"), "html", null, true);
        echo "\" />
                            <style type=\"text/css\">        
                                #mainNav >ul >li >a:hover,#accessCatalogue > li .active,#mainNav >ul >.active >a,.back .add-wishlist,.scroll-list
                                {background-color:#bb9854;}
                                .cat,#opening a,#sidebar-nav strong,.listingCatalogue li h3, .listingCatalogue li h2,.fidelityContent p a,#modal-inscription span,.globalInfos h3,.front             .club-offer strong,.back .club-offer h2, #modal-add-wishlist p,#modal-shopping-alert p,#slider h2 .first,.backlinks a
                                {color:#bb9854;}
                                li.soon {border-color:#bb9854 !important;}
                                li.soon .mask-bonplan, li.soon .accessOffer a.access-infos{background-color:#bb9854;}
                            </style>
                        ";
        
        $__internal_320574e16569a73046b31be4d678b7a8cf805019c89322365ec3518c10bc8602->leave($__internal_320574e16569a73046b31be4d678b7a8cf805019c89322365ec3518c10bc8602_prof);

    }

    // line 125
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b634b8d92009270c365bc21a5b1d21faebe58e402426640d204fa38fc8dcbbd5 = $this->env->getExtension("native_profiler");
        $__internal_b634b8d92009270c365bc21a5b1d21faebe58e402426640d204fa38fc8dcbbd5->enter($__internal_b634b8d92009270c365bc21a5b1d21faebe58e402426640d204fa38fc8dcbbd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 126
        echo "                                        ";
        $this->loadTemplate("FOSUserBundle:ChangePassword:changePassword_content.html.twig", "FOSUserBundle:ChangePassword:changePassword.html.twig", 126)->display($context);
        // line 127
        echo "                                ";
        
        $__internal_b634b8d92009270c365bc21a5b1d21faebe58e402426640d204fa38fc8dcbbd5->leave($__internal_b634b8d92009270c365bc21a5b1d21faebe58e402426640d204fa38fc8dcbbd5_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:changePassword.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  322 => 127,  319 => 126,  313 => 125,  296 => 31,  292 => 30,  288 => 29,  285 => 28,  279 => 27,  267 => 26,  201 => 142,  197 => 141,  182 => 128,  180 => 125,  175 => 122,  101 => 50,  90 => 42,  85 => 41,  82 => 27,  80 => 26,  73 => 22,  69 => 21,  65 => 20,  61 => 19,  56 => 17,  45 => 8,  33 => 7,  26 => 1,);
    }
}
/* */
/* */
/* */
/* */
/* */
/* {# empty Twig template #}*/
/* {% block container %}*/
/* */
/*     <html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xmlns:fb="http://www.facebook.com/2008/fbml">*/
/*         <head>*/
/*             <title>Tunisa Malll Badalhom lkol </title>*/
/*             <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*                 <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, */
/*                       Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />*/
/*                 <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>*/
/*                 <link href="{{asset('bundles/myappadmin/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />*/
/*                 <!-- Custom Theme files -->*/
/*                 <link href="{{asset('bundles/myappadmin/css/style.css')}}" rel='stylesheet' type='text/css' />*/
/*                 <link href="{{asset('bundles/myappadmin/css/font-awesome.css')}}" rel="stylesheet"> */
/*                     <script src="{{asset('bundles/myappadmin/js/jquery.min.js')}}"></script>*/
/*                     <script src="{{asset('bundles/myappadmin/js/bootstrap.min.js')}}"></script>*/
/*                     <meta http-equiv="content-type" content="text/html;charset=utf-8" />*/
/*                     <meta charset="UTF-8">*/
/* */
/*                         {% block title %}<title>Tunisia Mall</title>{% endblock %}*/
/*                         {% block stylesheets %}*/
/* */
/*                             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/6c6c9e8995c7ee151b5a1c238da4a176_1453471972_all.css') }}" />*/
/*                             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/mobile.css') }}" media="screen and (max-width:1100px)" />*/
/*                             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/taggage.css') }}" />*/
/*                             <style type="text/css">        */
/*                                 #mainNav >ul >li >a:hover,#accessCatalogue > li .active,#mainNav >ul >.active >a,.back .add-wishlist,.scroll-list*/
/*                                 {background-color:#bb9854;}*/
/*                                 .cat,#opening a,#sidebar-nav strong,.listingCatalogue li h3, .listingCatalogue li h2,.fidelityContent p a,#modal-inscription span,.globalInfos h3,.front             .club-offer strong,.back .club-offer h2, #modal-add-wishlist p,#modal-shopping-alert p,#slider h2 .first,.backlinks a*/
/*                                 {color:#bb9854;}*/
/*                                 li.soon {border-color:#bb9854 !important;}*/
/*                                 li.soon .mask-bonplan, li.soon .accessOffer a.access-infos{background-color:#bb9854;}*/
/*                             </style>*/
/*                         {% endblock %}*/
/*                         <script type="text/javascript" src="{{ asset('bundles/myappuser/js/a8de5014480b4daa57965aee6265098c_1452767442.js') }}" charset="utf-8"></script>*/
/*                         <script type="text/javascript" src="{{ asset('bundles/myappuser/js/e644c8cbc8bd826f9f922db7d95f32b4_1446657238.js') }}" charset="utf-8"></script>*/
/*                         </head>*/
/*                         <body>*/
/*                             <div id="container" class="" >*/
/*                                 <header class="header-home"><!--header site global-->*/
/*                                     <div class="int">*/
/*                                         <div id="logo">*/
/*                                             <a href="Creteil-Soleil.html">*/
/*                                                 <img src="{{ asset('bundles/myappuser/images/logo.png') }}"   style="" alt="Créteil Soleil" title="Créteil Soleil" />*/
/*                                             </a>*/
/*                                         </div>*/
/* */
/*                                         <ul  class="rslides rslides1"  id="opening"><!--NEW BLOC horaires-->*/
/*                                             <li class="" style="display: list-item; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;" id="rslides1_s0">*/
/*                                                 <a href="Creteil-Soleil/Horaires-Acces.html">*/
/* */
/*                                                 </a>*/
/*                                             </li>*/
/*                                         </ul><!--FIN NEW BLOC-->*/
/*                                     </div>*/
/*                                     <span id="accessMobile">Menu</span>*/
/*                                     <div id="navigation"><!--englobe les 2 nav pour responsive-->*/
/*                                         <nav id="mainNav"><!--navigation principale site-->*/
/*                                             <ul class="int">*/
/*                                                 <li>*/
/*                                                     <a href="#">Acceuil</a>*/
/*                                                 </li>*/
/*                                                 <li>*/
/*                                                     <a href="#">Boutiques</a>*/
/*                                                 </li>*/
/*                                                 <li>*/
/*                                                     <a href="#">Catalogues</a>*/
/*                                                 </li>*/
/*                                             </ul>*/
/*                                         </nav>*/
/*                                         <div id="topBarSite"><!--2eme nav et reseaux sociaux-->*/
/*                                             <div class="int">*/
/*                                                 <ul id="secondaryNav"><!--New BLOC-->*/
/* */
/*                                                     <li>*/
/*                                                         <a href="Creteil-Soleil/Horaires-Acces.html" >Horaires & Accès</a>*/
/*                                                     </li>*/
/* */
/*                                                     <li>*/
/*                                                         <a href="Creteil-Soleil/Plan-du-centre.html" >Plan du centre</a>*/
/*                                                     </li>*/
/* */
/*                                                     <li>*/
/*                                                         <a href="Creteil-Soleil/Services.html" >News</a>*/
/*                                                     </li>*/
/* */
/* */
/*                                                 </ul><!--New BLOC-->*/
/*                                                 <ul class="blocSocialMedia">*/
/*                                                     <li class="facebook">*/
/*                                                         <a href="#" rel="nofollow" target="_blank">Facebook</a>*/
/*                                                         <div class="contentbloc">*/
/*                                                             <a  href="#">Follow @Facebook</a>*/
/*                                                         </div>*/
/*                                                     </li>*/
/*                                                     <li class="twitter">*/
/*                                                         <a href="#" rel="nofollow" target="_blank">Twitter</a>*/
/*                                                         <div class="contentbloc">*/
/*                                                             <a href="#">Follow @twitter</a>*/
/*                                                         </div>*/
/*                                                     </li>*/
/*                                                     <li class="instagram">*/
/*                                                         <a href="#" target="_blank" rel="nofollow" >Instagram</a>*/
/*                                                         <div class="contentbloc">*/
/*                                                             <a href="#">Follow @instagram</a>*/
/*                                                     </li>*/
/*                                                 </ul>        */
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                             </div>*/
/* */
/*                             </header>*/
/* */
/*                             {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/*                             <div class="login-bottom">*/
/*                                 <center><h2>Modifier Mot de passe</h2></center>*/
/*                                     {% block fos_user_content %}*/
/*                                         {% include "FOSUserBundle:ChangePassword:changePassword_content.html.twig" %}*/
/*                                 {% endblock fos_user_content %}*/
/*                             </div>*/
/*                             </div>*/
/* */
/*                             <style>*/
/*                                 .login-bottom{*/
/*                                     height: 400px;*/
/*                                 }*/
/*                             </style>*/
/*                             <!---->*/
/* */
/*                             <div class="copy-right">*/
/*                                 <p> &copy; 2016 Tunisia Mall. All Rights Reserved | <a href="#" target="_blank">Tunisia Mall</a> </p>       </div>  */
/*                             <!--scrolling js-->*/
/*                             <script src="{{asset('bundles/myappadmin/js/jquery.nicescroll.js')}}"></script>*/
/*                             <script src="{{asset('bundles/myappadmin/js/scripts.js')}}"></script>*/
/*                             <!--//scrolling js-->*/
/*                             <div id="container" class="" >*/
/*                                 <footer ><!--footer-->*/
/*                                     <div class="left">*/
/*                                         <h2>Newsletter Tunisia Mall</h2>*/
/*                                         <p>Pour être informé sur:</p>*/
/*                                         <ul>*/
/*                                             <li>Les sorties de nouvelles collections</li>*/
/*                                             <li>Vos marques préférées</li>*/
/*                                         </ul>*/
/* */
/* */
/*                                         <form action="#" id="newsletter_form" method="post">*/
/*                                             <fieldset>*/
/*                                                 <input type="text" name="email" value="Votre email" data-init-value="Votre email" />*/
/*                                                 <input type="hidden" name="centreId" value="399"/>*/
/*                                                 <input type="hidden" name="centreName" value="Créteil Soleil"/>*/
/*                                                 <input type="submit" value="ok" />*/
/*                                             </fieldset>*/
/*                                         </form>*/
/*                                     </div>*/
/* */
/*                                     <div class="right">*/
/*                                         <h2>Application Mobile Tunisia Mall</h2>*/
/*                                         <p>Retrouvez votre centre Tunisia Mall sur votre smartphone et vos tablettes !</p>*/
/*                                         <a class="apple" target="_blank" href="#"onClick="return xt_click(this, 'C', '75', 'Depuis_lien_footer:App_store', 'N');*/
/*                                                 return false;">Appstore</a>*/
/*                                         <a class="google" target="_blank" href="#" onClick="return xt_click(this, 'C', '75', 'Depuis_lien_footer:Google_Play', 'N');*/
/*                                                 return false;"> Google play</a>*/
/*                                     </div>*/
/* */
/* */
/*                                     <nav>*/
/*                                         <ul>*/
/*                                             <li><a href="Creteil-Soleil/Nous-contacter.html" title="Nous contacter">Nous contacter</a></li>*/
/*                                             <li><a href="Creteil-Soleil/Mentions-Legales.html" title="Mentions Légales">Mentions Légales</a></li>*/
/*                                             <li><a href="Creteil-Soleil/Louer-un-emplacement-temporaire.html" title="Louer un emplacement temporaire">Visite guidee</a></li>*/
/*                                         </ul>*/
/*                                     </nav>*/
/* */
/* */
/*                                     <ul class="blocSocialMedia">*/
/*                                         <li class="facebook">*/
/*                                             <a href="#" target="_blank" rel="nofollow">Facebook</a>*/
/*                                         </li>*/
/*                                         <li class="twitter">*/
/*                                             <a href="#" target="_blank" rel="nofollow">Twitter</a>*/
/*                                         </li>*/
/*                                         <li class="instagram">*/
/*                                             <a href="#" target="_blank" rel="nofollow" >Instagram</a>*/
/*                                         </li>*/
/*                                     </ul>*/
/* */
/*                                 </footer><!--Fin footer-->        */
/*                             </div>*/
/*                         </body>*/
/*                         </html>*/
/*                     {% endblock container %}*/
