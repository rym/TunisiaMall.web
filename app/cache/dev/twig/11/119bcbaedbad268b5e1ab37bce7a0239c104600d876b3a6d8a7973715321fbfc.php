<?php

/* ::base.html.twig */
class __TwigTemplate_95392139a4ae51bc3eb92bff174e5c9c5f43e2896eaacc6f5ea6e39b9b8e3998 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5dbb9f62c6915acabb36f970e8aed0f34f8fd970586cdab218ba3f3232c2cef7 = $this->env->getExtension("native_profiler");
        $__internal_5dbb9f62c6915acabb36f970e8aed0f34f8fd970586cdab218ba3f3232c2cef7->enter($__internal_5dbb9f62c6915acabb36f970e8aed0f34f8fd970586cdab218ba3f3232c2cef7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_5dbb9f62c6915acabb36f970e8aed0f34f8fd970586cdab218ba3f3232c2cef7->leave($__internal_5dbb9f62c6915acabb36f970e8aed0f34f8fd970586cdab218ba3f3232c2cef7_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_3fdd79322a24580a2e4fd4e2bae0e462bde38e1143b56df8daaf685e0fcfb70e = $this->env->getExtension("native_profiler");
        $__internal_3fdd79322a24580a2e4fd4e2bae0e462bde38e1143b56df8daaf685e0fcfb70e->enter($__internal_3fdd79322a24580a2e4fd4e2bae0e462bde38e1143b56df8daaf685e0fcfb70e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_3fdd79322a24580a2e4fd4e2bae0e462bde38e1143b56df8daaf685e0fcfb70e->leave($__internal_3fdd79322a24580a2e4fd4e2bae0e462bde38e1143b56df8daaf685e0fcfb70e_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_e9ffdbd7d10f83f09fac389cd5a9eb6a69caf3a107f1daf18cf6b03458542b91 = $this->env->getExtension("native_profiler");
        $__internal_e9ffdbd7d10f83f09fac389cd5a9eb6a69caf3a107f1daf18cf6b03458542b91->enter($__internal_e9ffdbd7d10f83f09fac389cd5a9eb6a69caf3a107f1daf18cf6b03458542b91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_e9ffdbd7d10f83f09fac389cd5a9eb6a69caf3a107f1daf18cf6b03458542b91->leave($__internal_e9ffdbd7d10f83f09fac389cd5a9eb6a69caf3a107f1daf18cf6b03458542b91_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_7114da3b33d38ff18eef17fbb3d16bf70fd04478f2bf557313bbd6a2e349ec10 = $this->env->getExtension("native_profiler");
        $__internal_7114da3b33d38ff18eef17fbb3d16bf70fd04478f2bf557313bbd6a2e349ec10->enter($__internal_7114da3b33d38ff18eef17fbb3d16bf70fd04478f2bf557313bbd6a2e349ec10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_7114da3b33d38ff18eef17fbb3d16bf70fd04478f2bf557313bbd6a2e349ec10->leave($__internal_7114da3b33d38ff18eef17fbb3d16bf70fd04478f2bf557313bbd6a2e349ec10_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_1e30782ed5e282cefdb201c1ee5d2160d7dfca30942a1ba96296c27dfd73d9a4 = $this->env->getExtension("native_profiler");
        $__internal_1e30782ed5e282cefdb201c1ee5d2160d7dfca30942a1ba96296c27dfd73d9a4->enter($__internal_1e30782ed5e282cefdb201c1ee5d2160d7dfca30942a1ba96296c27dfd73d9a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_1e30782ed5e282cefdb201c1ee5d2160d7dfca30942a1ba96296c27dfd73d9a4->leave($__internal_1e30782ed5e282cefdb201c1ee5d2160d7dfca30942a1ba96296c27dfd73d9a4_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
