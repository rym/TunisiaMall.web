<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_133c2c4e3262f1a9eb4ddf5cf18ac84dd4907b2c1e1376466e3de2a0f95d2c8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_edc6aba5d4c6b84f5cb5e5ee76055a6997231534786f4e9c70ffba78e1b15e14 = $this->env->getExtension("native_profiler");
        $__internal_edc6aba5d4c6b84f5cb5e5ee76055a6997231534786f4e9c70ffba78e1b15e14->enter($__internal_edc6aba5d4c6b84f5cb5e5ee76055a6997231534786f4e9c70ffba78e1b15e14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_edc6aba5d4c6b84f5cb5e5ee76055a6997231534786f4e9c70ffba78e1b15e14->leave($__internal_edc6aba5d4c6b84f5cb5e5ee76055a6997231534786f4e9c70ffba78e1b15e14_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
