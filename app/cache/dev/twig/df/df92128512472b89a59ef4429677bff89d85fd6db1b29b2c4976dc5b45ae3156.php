<?php

/* MyAppUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_0de0f5fc2ed7b8df1828fefe5de180a045b43ae1bda200d23cef40cdb860c0b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4823c917230dc654544dd3baec2ec5421a5641b60cb5848f6cd5d14efd9331c8 = $this->env->getExtension("native_profiler");
        $__internal_4823c917230dc654544dd3baec2ec5421a5641b60cb5848f6cd5d14efd9331c8->enter($__internal_4823c917230dc654544dd3baec2ec5421a5641b60cb5848f6cd5d14efd9331c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_4823c917230dc654544dd3baec2ec5421a5641b60cb5848f6cd5d14efd9331c8->leave($__internal_4823c917230dc654544dd3baec2ec5421a5641b60cb5848f6cd5d14efd9331c8_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_8c9a1b8c8bf23b1553d0bd76c9cfa6c816649c439fbcd6d6f4acaae71e059e89 = $this->env->getExtension("native_profiler");
        $__internal_8c9a1b8c8bf23b1553d0bd76c9cfa6c816649c439fbcd6d6f4acaae71e059e89->enter($__internal_8c9a1b8c8bf23b1553d0bd76c9cfa6c816649c439fbcd6d6f4acaae71e059e89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        echo "
";
        
        $__internal_8c9a1b8c8bf23b1553d0bd76c9cfa6c816649c439fbcd6d6f4acaae71e059e89->leave($__internal_8c9a1b8c8bf23b1553d0bd76c9cfa6c816649c439fbcd6d6f4acaae71e059e89_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_c2267b116f668089830f38ab33d8c08c613207394b3f844a3e8557c899d80c62 = $this->env->getExtension("native_profiler");
        $__internal_c2267b116f668089830f38ab33d8c08c613207394b3f844a3e8557c899d80c62->enter($__internal_c2267b116f668089830f38ab33d8c08c613207394b3f844a3e8557c899d80c62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_c2267b116f668089830f38ab33d8c08c613207394b3f844a3e8557c899d80c62->leave($__internal_c2267b116f668089830f38ab33d8c08c613207394b3f844a3e8557c899d80c62_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_283c3daddf85ac9f007eda8ff7d087ab8c0dd7a78865ffbb410617d0e583de59 = $this->env->getExtension("native_profiler");
        $__internal_283c3daddf85ac9f007eda8ff7d087ab8c0dd7a78865ffbb410617d0e583de59->enter($__internal_283c3daddf85ac9f007eda8ff7d087ab8c0dd7a78865ffbb410617d0e583de59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_283c3daddf85ac9f007eda8ff7d087ab8c0dd7a78865ffbb410617d0e583de59->leave($__internal_283c3daddf85ac9f007eda8ff7d087ab8c0dd7a78865ffbb410617d0e583de59_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.subject'|trans({'%username%': user.username}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
