<?php

/* MyAppUserBundle:Group:edit.html.twig */
class __TwigTemplate_737061c6eee4a77ad5bea313cd398cb5acfb50588b40d53999ee3e724d46d9d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "MyAppUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d0e0589221adb911d172bbc116313495e1f217fcd6898e81cdee3688e628cd1f = $this->env->getExtension("native_profiler");
        $__internal_d0e0589221adb911d172bbc116313495e1f217fcd6898e81cdee3688e628cd1f->enter($__internal_d0e0589221adb911d172bbc116313495e1f217fcd6898e81cdee3688e628cd1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d0e0589221adb911d172bbc116313495e1f217fcd6898e81cdee3688e628cd1f->leave($__internal_d0e0589221adb911d172bbc116313495e1f217fcd6898e81cdee3688e628cd1f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ce38e85bef6c49a9d5c5748a6a58c1f10e1ff0bc81ec31f49113187b84560ac8 = $this->env->getExtension("native_profiler");
        $__internal_ce38e85bef6c49a9d5c5748a6a58c1f10e1ff0bc81ec31f49113187b84560ac8->enter($__internal_ce38e85bef6c49a9d5c5748a6a58c1f10e1ff0bc81ec31f49113187b84560ac8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:edit_content.html.twig", "MyAppUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_ce38e85bef6c49a9d5c5748a6a58c1f10e1ff0bc81ec31f49113187b84560ac8->leave($__internal_ce38e85bef6c49a9d5c5748a6a58c1f10e1ff0bc81ec31f49113187b84560ac8_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
