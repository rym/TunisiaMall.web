<?php

/* MyAppUserBundle:Registration:register-client_content.html.twig */
class __TwigTemplate_481367ba08ef5589c6b9040b7f5588f637ad6c1093eb4e1879229f7bd1b4638c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1cd7b31a621cba795b930e1a123c2f633fa211e09c55f0b64cded174bfb22a7c = $this->env->getExtension("native_profiler");
        $__internal_1cd7b31a621cba795b930e1a123c2f633fa211e09c55f0b64cded174bfb22a7c->enter($__internal_1cd7b31a621cba795b930e1a123c2f633fa211e09c55f0b64cded174bfb22a7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Registration:register-client_content.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('routing')->getPath("my_app_register_client"), "attr" => array("class" => "fos_user_registration_register")));
        echo "
    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 8
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        
        $__internal_1cd7b31a621cba795b930e1a123c2f633fa211e09c55f0b64cded174bfb22a7c->leave($__internal_1cd7b31a621cba795b930e1a123c2f633fa211e09c55f0b64cded174bfb22a7c_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Registration:register-client_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 8,  34 => 6,  29 => 4,  25 => 3,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {{ form_start(form, {'method': 'post', 'action': path('my_app_register_client'), 'attr': {'class': 'fos_user_registration_register'}}) }}*/
/*     {{ form_widget(form) }}*/
/*     <div>*/
/*         <input type="submit" value="{{ 'registration.submit'|trans }}" />*/
/*     </div>*/
/* {{ form_end(form) }}*/
