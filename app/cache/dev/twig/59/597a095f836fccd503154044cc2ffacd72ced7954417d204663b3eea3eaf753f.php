<?php

/* AcmeDemoBundle::layout.html.twig */
class __TwigTemplate_b3a23360076f1cd4b6d05cc5dbd23e5d92b40bfcf3b50347a149bd4f6840b470 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "AcmeDemoBundle::layout.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'content_header' => array($this, 'block_content_header'),
            'content_header_more' => array($this, 'block_content_header_more'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e32cb644f900139d304a4e595667a6a017eaa8de9340d32716ee792d554e4dc3 = $this->env->getExtension("native_profiler");
        $__internal_e32cb644f900139d304a4e595667a6a017eaa8de9340d32716ee792d554e4dc3->enter($__internal_e32cb644f900139d304a4e595667a6a017eaa8de9340d32716ee792d554e4dc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AcmeDemoBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e32cb644f900139d304a4e595667a6a017eaa8de9340d32716ee792d554e4dc3->leave($__internal_e32cb644f900139d304a4e595667a6a017eaa8de9340d32716ee792d554e4dc3_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_31cf735b6f9aec6f0b665dc66a6a2e8aa8ba946188cb98c33dea836297f38b94 = $this->env->getExtension("native_profiler");
        $__internal_31cf735b6f9aec6f0b665dc66a6a2e8aa8ba946188cb98c33dea836297f38b94->enter($__internal_31cf735b6f9aec6f0b665dc66a6a2e8aa8ba946188cb98c33dea836297f38b94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link rel=\"icon\" sizes=\"16x16\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/acmedemo/css/demo.css"), "html", null, true);
        echo "\" />
";
        
        $__internal_31cf735b6f9aec6f0b665dc66a6a2e8aa8ba946188cb98c33dea836297f38b94->leave($__internal_31cf735b6f9aec6f0b665dc66a6a2e8aa8ba946188cb98c33dea836297f38b94_prof);

    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        $__internal_5db59251e251606fdee8bf0a919633fcfca0e6ea1c5500a675f7b7501c1d8dce = $this->env->getExtension("native_profiler");
        $__internal_5db59251e251606fdee8bf0a919633fcfca0e6ea1c5500a675f7b7501c1d8dce->enter($__internal_5db59251e251606fdee8bf0a919633fcfca0e6ea1c5500a675f7b7501c1d8dce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Demo Bundle";
        
        $__internal_5db59251e251606fdee8bf0a919633fcfca0e6ea1c5500a675f7b7501c1d8dce->leave($__internal_5db59251e251606fdee8bf0a919633fcfca0e6ea1c5500a675f7b7501c1d8dce_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_7d390b695442af5418c0c1d8d242a774b953cd7323397cbe328067485ff5d18e = $this->env->getExtension("native_profiler");
        $__internal_7d390b695442af5418c0c1d8d242a774b953cd7323397cbe328067485ff5d18e->enter($__internal_7d390b695442af5418c0c1d8d242a774b953cd7323397cbe328067485ff5d18e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 11
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 12
            echo "        <div class=\"flash-message\">
            <em>Notice</em>: ";
            // line 13
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "
    ";
        // line 17
        $this->displayBlock('content_header', $context, $blocks);
        // line 26
        echo "
    <div class=\"block\">
        ";
        // line 28
        $this->displayBlock('content', $context, $blocks);
        // line 29
        echo "    </div>

    ";
        // line 31
        if (array_key_exists("code", $context)) {
            // line 32
            echo "        <h2>Code behind this page</h2>
        <div class=\"block\">
            <div class=\"symfony-content\">";
            // line 34
            echo (isset($context["code"]) ? $context["code"] : $this->getContext($context, "code"));
            echo "</div>
        </div>
    ";
        }
        
        $__internal_7d390b695442af5418c0c1d8d242a774b953cd7323397cbe328067485ff5d18e->leave($__internal_7d390b695442af5418c0c1d8d242a774b953cd7323397cbe328067485ff5d18e_prof);

    }

    // line 17
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_0dd54a3f6bc5dc9d0d86480b327836720291d86bbaa2b06874ef9e11f9c60db4 = $this->env->getExtension("native_profiler");
        $__internal_0dd54a3f6bc5dc9d0d86480b327836720291d86bbaa2b06874ef9e11f9c60db4->enter($__internal_0dd54a3f6bc5dc9d0d86480b327836720291d86bbaa2b06874ef9e11f9c60db4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        // line 18
        echo "        <ul id=\"menu\">
            ";
        // line 19
        $this->displayBlock('content_header_more', $context, $blocks);
        // line 22
        echo "        </ul>

        <div style=\"clear: both\"></div>
    ";
        
        $__internal_0dd54a3f6bc5dc9d0d86480b327836720291d86bbaa2b06874ef9e11f9c60db4->leave($__internal_0dd54a3f6bc5dc9d0d86480b327836720291d86bbaa2b06874ef9e11f9c60db4_prof);

    }

    // line 19
    public function block_content_header_more($context, array $blocks = array())
    {
        $__internal_417e70a7df3beef9930d7d22d8c7238414159670e628ebefc61e0cf0f8c7610e = $this->env->getExtension("native_profiler");
        $__internal_417e70a7df3beef9930d7d22d8c7238414159670e628ebefc61e0cf0f8c7610e->enter($__internal_417e70a7df3beef9930d7d22d8c7238414159670e628ebefc61e0cf0f8c7610e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header_more"));

        // line 20
        echo "                <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("_demo");
        echo "\">Demo Home</a></li>
            ";
        
        $__internal_417e70a7df3beef9930d7d22d8c7238414159670e628ebefc61e0cf0f8c7610e->leave($__internal_417e70a7df3beef9930d7d22d8c7238414159670e628ebefc61e0cf0f8c7610e_prof);

    }

    // line 28
    public function block_content($context, array $blocks = array())
    {
        $__internal_b2aff132eb1253dbe1a490b756fb45f29e8fbb31b249113126f674eb11d7c799 = $this->env->getExtension("native_profiler");
        $__internal_b2aff132eb1253dbe1a490b756fb45f29e8fbb31b249113126f674eb11d7c799->enter($__internal_b2aff132eb1253dbe1a490b756fb45f29e8fbb31b249113126f674eb11d7c799_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_b2aff132eb1253dbe1a490b756fb45f29e8fbb31b249113126f674eb11d7c799->leave($__internal_b2aff132eb1253dbe1a490b756fb45f29e8fbb31b249113126f674eb11d7c799_prof);

    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 28,  153 => 20,  147 => 19,  137 => 22,  135 => 19,  132 => 18,  126 => 17,  115 => 34,  111 => 32,  109 => 31,  105 => 29,  103 => 28,  99 => 26,  97 => 17,  94 => 16,  85 => 13,  82 => 12,  77 => 11,  71 => 10,  59 => 8,  50 => 5,  45 => 4,  39 => 3,  11 => 1,);
    }
}
/* {% extends "TwigBundle::layout.html.twig" %}*/
/* */
/* {% block head %}*/
/*     <link rel="icon" sizes="16x16" href="{{ asset('favicon.ico') }}" />*/
/*     <link rel="stylesheet" href="{{ asset('bundles/acmedemo/css/demo.css') }}" />*/
/* {% endblock %}*/
/* */
/* {% block title 'Demo Bundle' %}*/
/* */
/* {% block body %}*/
/*     {% for flashMessage in app.session.flashbag.get('notice') %}*/
/*         <div class="flash-message">*/
/*             <em>Notice</em>: {{ flashMessage }}*/
/*         </div>*/
/*     {% endfor %}*/
/* */
/*     {% block content_header %}*/
/*         <ul id="menu">*/
/*             {% block content_header_more %}*/
/*                 <li><a href="{{ path('_demo') }}">Demo Home</a></li>*/
/*             {% endblock %}*/
/*         </ul>*/
/* */
/*         <div style="clear: both"></div>*/
/*     {% endblock %}*/
/* */
/*     <div class="block">*/
/*         {% block content %}{% endblock %}*/
/*     </div>*/
/* */
/*     {% if code is defined %}*/
/*         <h2>Code behind this page</h2>*/
/*         <div class="block">*/
/*             <div class="symfony-content">{{ code|raw }}</div>*/
/*         </div>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
