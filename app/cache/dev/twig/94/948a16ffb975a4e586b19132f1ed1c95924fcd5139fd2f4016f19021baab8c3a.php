<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_b1390e5e9466154b15309cc6564b6e3457dba4a426fb9d37163e71e42bcfbb7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c248c4ee887b4e68b6c00e34f910568324572d93dd0c0371507b7ae85950551a = $this->env->getExtension("native_profiler");
        $__internal_c248c4ee887b4e68b6c00e34f910568324572d93dd0c0371507b7ae85950551a->enter($__internal_c248c4ee887b4e68b6c00e34f910568324572d93dd0c0371507b7ae85950551a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_c248c4ee887b4e68b6c00e34f910568324572d93dd0c0371507b7ae85950551a->leave($__internal_c248c4ee887b4e68b6c00e34f910568324572d93dd0c0371507b7ae85950551a_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_861561e529557aeae96b927d0611e0ae7e35a966eb2da86ba6f2fb8e88366fb8 = $this->env->getExtension("native_profiler");
        $__internal_861561e529557aeae96b927d0611e0ae7e35a966eb2da86ba6f2fb8e88366fb8->enter($__internal_861561e529557aeae96b927d0611e0ae7e35a966eb2da86ba6f2fb8e88366fb8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_861561e529557aeae96b927d0611e0ae7e35a966eb2da86ba6f2fb8e88366fb8->leave($__internal_861561e529557aeae96b927d0611e0ae7e35a966eb2da86ba6f2fb8e88366fb8_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
