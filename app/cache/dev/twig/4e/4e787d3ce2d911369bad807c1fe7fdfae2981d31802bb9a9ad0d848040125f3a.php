<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_efd135a782da93e7ddb8d5d7f6e6cc8a199547160d235c5bbeb7a48f7dfbab40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71e7340bfb8823f2fc43b5cbf2d27cc835859470c262282a9217fe660b8adce5 = $this->env->getExtension("native_profiler");
        $__internal_71e7340bfb8823f2fc43b5cbf2d27cc835859470c262282a9217fe660b8adce5->enter($__internal_71e7340bfb8823f2fc43b5cbf2d27cc835859470c262282a9217fe660b8adce5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_71e7340bfb8823f2fc43b5cbf2d27cc835859470c262282a9217fe660b8adce5->leave($__internal_71e7340bfb8823f2fc43b5cbf2d27cc835859470c262282a9217fe660b8adce5_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
