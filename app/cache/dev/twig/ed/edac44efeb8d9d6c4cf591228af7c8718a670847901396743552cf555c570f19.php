<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_9840d53e77ddef805ebd24226fea7c22972def9c4050f2b4b2f74c16bc5d3b07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4974fc38081270cadb7d24fce06399c11b9f67775a5864bff6d3e1d9e4ba940 = $this->env->getExtension("native_profiler");
        $__internal_b4974fc38081270cadb7d24fce06399c11b9f67775a5864bff6d3e1d9e4ba940->enter($__internal_b4974fc38081270cadb7d24fce06399c11b9f67775a5864bff6d3e1d9e4ba940_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b4974fc38081270cadb7d24fce06399c11b9f67775a5864bff6d3e1d9e4ba940->leave($__internal_b4974fc38081270cadb7d24fce06399c11b9f67775a5864bff6d3e1d9e4ba940_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_e6795b5c4df3dcd5bc083eb78ea56386955609beffe047cb72348cdf975ec7a3 = $this->env->getExtension("native_profiler");
        $__internal_e6795b5c4df3dcd5bc083eb78ea56386955609beffe047cb72348cdf975ec7a3->enter($__internal_e6795b5c4df3dcd5bc083eb78ea56386955609beffe047cb72348cdf975ec7a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_e6795b5c4df3dcd5bc083eb78ea56386955609beffe047cb72348cdf975ec7a3->leave($__internal_e6795b5c4df3dcd5bc083eb78ea56386955609beffe047cb72348cdf975ec7a3_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_8c2469f88c8959474c6e00570c7f8757a52baab862f88fc1b36be7a4db2ae2ab = $this->env->getExtension("native_profiler");
        $__internal_8c2469f88c8959474c6e00570c7f8757a52baab862f88fc1b36be7a4db2ae2ab->enter($__internal_8c2469f88c8959474c6e00570c7f8757a52baab862f88fc1b36be7a4db2ae2ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_8c2469f88c8959474c6e00570c7f8757a52baab862f88fc1b36be7a4db2ae2ab->leave($__internal_8c2469f88c8959474c6e00570c7f8757a52baab862f88fc1b36be7a4db2ae2ab_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
