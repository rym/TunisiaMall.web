<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_e5c2c6b126f9f0b651bc5eb8d885ab8bd41f81d0f80670ac083cde6db20168e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c328838e3b4947a8dc9ed881f76575ee2ff9852953ef6c472965e029e1b21db = $this->env->getExtension("native_profiler");
        $__internal_3c328838e3b4947a8dc9ed881f76575ee2ff9852953ef6c472965e029e1b21db->enter($__internal_3c328838e3b4947a8dc9ed881f76575ee2ff9852953ef6c472965e029e1b21db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3c328838e3b4947a8dc9ed881f76575ee2ff9852953ef6c472965e029e1b21db->leave($__internal_3c328838e3b4947a8dc9ed881f76575ee2ff9852953ef6c472965e029e1b21db_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_f498f613520d7d57051838b77853b335dc4b1c265a7e110ea46ff3685af4d672 = $this->env->getExtension("native_profiler");
        $__internal_f498f613520d7d57051838b77853b335dc4b1c265a7e110ea46ff3685af4d672->enter($__internal_f498f613520d7d57051838b77853b335dc4b1c265a7e110ea46ff3685af4d672_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_f498f613520d7d57051838b77853b335dc4b1c265a7e110ea46ff3685af4d672->leave($__internal_f498f613520d7d57051838b77853b335dc4b1c265a7e110ea46ff3685af4d672_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_0cb6b185864d6e0c125ee5032940df55dc8a8b6f4d95dcec7b196bc40603c430 = $this->env->getExtension("native_profiler");
        $__internal_0cb6b185864d6e0c125ee5032940df55dc8a8b6f4d95dcec7b196bc40603c430->enter($__internal_0cb6b185864d6e0c125ee5032940df55dc8a8b6f4d95dcec7b196bc40603c430_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_0cb6b185864d6e0c125ee5032940df55dc8a8b6f4d95dcec7b196bc40603c430->leave($__internal_0cb6b185864d6e0c125ee5032940df55dc8a8b6f4d95dcec7b196bc40603c430_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_f772b25c050e995f39a076d9a78138e5b3590e5aa25b3501e53a2879bff5a7f7 = $this->env->getExtension("native_profiler");
        $__internal_f772b25c050e995f39a076d9a78138e5b3590e5aa25b3501e53a2879bff5a7f7->enter($__internal_f772b25c050e995f39a076d9a78138e5b3590e5aa25b3501e53a2879bff5a7f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_f772b25c050e995f39a076d9a78138e5b3590e5aa25b3501e53a2879bff5a7f7->leave($__internal_f772b25c050e995f39a076d9a78138e5b3590e5aa25b3501e53a2879bff5a7f7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
