<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_96727b31b690e29c1a61603fd14cd760c35a324889045f051027db7c50015e16 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout_front_office.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout_front_office.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_99f28aabf2b03f2de4404f5d3fac8f10d3a9d915e3b9af18083ef7c6a668c528 = $this->env->getExtension("native_profiler");
        $__internal_99f28aabf2b03f2de4404f5d3fac8f10d3a9d915e3b9af18083ef7c6a668c528->enter($__internal_99f28aabf2b03f2de4404f5d3fac8f10d3a9d915e3b9af18083ef7c6a668c528_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_99f28aabf2b03f2de4404f5d3fac8f10d3a9d915e3b9af18083ef7c6a668c528->leave($__internal_99f28aabf2b03f2de4404f5d3fac8f10d3a9d915e3b9af18083ef7c6a668c528_prof);

    }

    // line 5
    public function block_container($context, array $blocks = array())
    {
        $__internal_8438ad711015f162c9e65abbc4c314d163d5d391001bdf75c62dbb2eddcf2759 = $this->env->getExtension("native_profiler");
        $__internal_8438ad711015f162c9e65abbc4c314d163d5d391001bdf75c62dbb2eddcf2759->enter($__internal_8438ad711015f162c9e65abbc4c314d163d5d391001bdf75c62dbb2eddcf2759_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_8438ad711015f162c9e65abbc4c314d163d5d391001bdf75c62dbb2eddcf2759->leave($__internal_8438ad711015f162c9e65abbc4c314d163d5d391001bdf75c62dbb2eddcf2759_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 8,  45 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout_front_office.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block container %}*/
/*     <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>*/
/*     {% if targetUrl %}*/
/*     <p><a href="{{ targetUrl }}">{{ 'registration.back'|trans }}</a></p>*/
/*     {% endif %}*/
/* {% endblock container %}*/
/* */
