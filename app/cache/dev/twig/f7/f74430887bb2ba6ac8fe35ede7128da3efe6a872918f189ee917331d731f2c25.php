<?php

/* MyAppUserBundle:Profile:show.html.twig */
class __TwigTemplate_b2ed958328c3e6bac19e6f5af1841cfbb948dfe6b8c50350e9f02b604fcb5c2b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "MyAppUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_89f89d89cb4944116bebb7e937c6e4cf8dca5e40b4eecd83703aa1b4033e2436 = $this->env->getExtension("native_profiler");
        $__internal_89f89d89cb4944116bebb7e937c6e4cf8dca5e40b4eecd83703aa1b4033e2436->enter($__internal_89f89d89cb4944116bebb7e937c6e4cf8dca5e40b4eecd83703aa1b4033e2436_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_89f89d89cb4944116bebb7e937c6e4cf8dca5e40b4eecd83703aa1b4033e2436->leave($__internal_89f89d89cb4944116bebb7e937c6e4cf8dca5e40b4eecd83703aa1b4033e2436_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c6fbcd1e6d32fa3efab2a06b14799ea9adb67ed011cd4d8724837a3941afb684 = $this->env->getExtension("native_profiler");
        $__internal_c6fbcd1e6d32fa3efab2a06b14799ea9adb67ed011cd4d8724837a3941afb684->enter($__internal_c6fbcd1e6d32fa3efab2a06b14799ea9adb67ed011cd4d8724837a3941afb684_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:show_content.html.twig", "MyAppUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_c6fbcd1e6d32fa3efab2a06b14799ea9adb67ed011cd4d8724837a3941afb684->leave($__internal_c6fbcd1e6d32fa3efab2a06b14799ea9adb67ed011cd4d8724837a3941afb684_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
