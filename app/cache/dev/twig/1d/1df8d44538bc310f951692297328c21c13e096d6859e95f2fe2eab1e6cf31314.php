<?php

/* MyAppAdminBundle:Admin:clients.html.twig */
class __TwigTemplate_9e804e6b29a978f5e94acb4a1d2404b6063093ee446f335c113dd7f8eb689ee6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MyAppAdminBundle::layout.html.twig", "MyAppAdminBundle:Admin:clients.html.twig", 1);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MyAppAdminBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2e8c393e837fe380581374d5173b508b79b4ee8de7f259bb69b17bd320181313 = $this->env->getExtension("native_profiler");
        $__internal_2e8c393e837fe380581374d5173b508b79b4ee8de7f259bb69b17bd320181313->enter($__internal_2e8c393e837fe380581374d5173b508b79b4ee8de7f259bb69b17bd320181313_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppAdminBundle:Admin:clients.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2e8c393e837fe380581374d5173b508b79b4ee8de7f259bb69b17bd320181313->leave($__internal_2e8c393e837fe380581374d5173b508b79b4ee8de7f259bb69b17bd320181313_prof);

    }

    // line 2
    public function block_container($context, array $blocks = array())
    {
        $__internal_ff134501386309679e20fb966011bffaf024fe12cb6e2da79eb368eb0452e78d = $this->env->getExtension("native_profiler");
        $__internal_ff134501386309679e20fb966011bffaf024fe12cb6e2da79eb368eb0452e78d->enter($__internal_ff134501386309679e20fb966011bffaf024fe12cb6e2da79eb368eb0452e78d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 3
        echo "    <div class=\"table-responsive\">          
        <table class=\"table\">
            <thead>
                <tr>
                    <th> nom</th>
                    <th> prenom</th>
                    <th> user name</th>
                    <th> Email</th>
                    <th colspan=\"2\">Action</th>
                </tr>
                ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clients"]) ? $context["clients"] : $this->getContext($context, "clients")));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 14
            echo "                    <tr>
                        <td> ";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "nom", array()), "html", null, true);
            echo "</td>
                        <td> ";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "prenom", array()), "html", null, true);
            echo "</td>
                        <td> ";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "username", array()), "html", null, true);
            echo "</td>
                        <td> ";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "email", array()), "html", null, true);
            echo "</td>
                        <td> <a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_app_admin_delete_client", array("id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-danger\" >Supprimer</a></td>
                        ";
            // line 20
            if (($this->getAttribute($context["client"], "locked", array()) == true)) {
                // line 21
                echo "                            <td> <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_app_admin_debloquer_client", array("id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-info\" >Activer</a></td>
                        ";
            } else {
                // line 23
                echo "                            <td> <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_app_admin_bloquer_client", array("id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-danger\" >Bloquer</a></td>
                        ";
            }
            // line 25
            echo "                            
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
            </thead>
        </table>
    </div>

";
        
        $__internal_ff134501386309679e20fb966011bffaf024fe12cb6e2da79eb368eb0452e78d->leave($__internal_ff134501386309679e20fb966011bffaf024fe12cb6e2da79eb368eb0452e78d_prof);

    }

    public function getTemplateName()
    {
        return "MyAppAdminBundle:Admin:clients.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 28,  93 => 25,  87 => 23,  81 => 21,  79 => 20,  75 => 19,  71 => 18,  67 => 17,  63 => 16,  59 => 15,  56 => 14,  52 => 13,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "MyAppAdminBundle::layout.html.twig" %}*/
/* {% block container %}*/
/*     <div class="table-responsive">          */
/*         <table class="table">*/
/*             <thead>*/
/*                 <tr>*/
/*                     <th> nom</th>*/
/*                     <th> prenom</th>*/
/*                     <th> user name</th>*/
/*                     <th> Email</th>*/
/*                     <th colspan="2">Action</th>*/
/*                 </tr>*/
/*                 {% for client in clients %}*/
/*                     <tr>*/
/*                         <td> {{client.nom}}</td>*/
/*                         <td> {{client.prenom}}</td>*/
/*                         <td> {{client.username}}</td>*/
/*                         <td> {{client.email}}</td>*/
/*                         <td> <a href="{{path('my_app_admin_delete_client',{'id':client.id})}}" class="btn btn-danger" >Supprimer</a></td>*/
/*                         {%if client.locked == true%}*/
/*                             <td> <a href="{{path('my_app_admin_debloquer_client',{'id':client.id})}}" class="btn btn-info" >Activer</a></td>*/
/*                         {%else%}*/
/*                             <td> <a href="{{path('my_app_admin_bloquer_client',{'id':client.id})}}" class="btn btn-danger" >Bloquer</a></td>*/
/*                         {% endif %}*/
/*                             */
/*                     </tr>*/
/*                 {% endfor %}*/
/* */
/*             </thead>*/
/*         </table>*/
/*     </div>*/
/* */
/* {% endblock container %}*/
/* */
/* */
