<?php

/* MyAppUserBundle:Group:list.html.twig */
class __TwigTemplate_5fb9118b6d50572b2040ae63a44c9f28bde66d304db3ef00e2d009fcaa10138c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "MyAppUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c93139b675dd94663742e50a4df3707452758ff575885bdb13de243edb59c1d5 = $this->env->getExtension("native_profiler");
        $__internal_c93139b675dd94663742e50a4df3707452758ff575885bdb13de243edb59c1d5->enter($__internal_c93139b675dd94663742e50a4df3707452758ff575885bdb13de243edb59c1d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c93139b675dd94663742e50a4df3707452758ff575885bdb13de243edb59c1d5->leave($__internal_c93139b675dd94663742e50a4df3707452758ff575885bdb13de243edb59c1d5_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ad5b70ca5cf9103903a8af4d8a1425539decccd6e6492bfba97c4a5fa7858716 = $this->env->getExtension("native_profiler");
        $__internal_ad5b70ca5cf9103903a8af4d8a1425539decccd6e6492bfba97c4a5fa7858716->enter($__internal_ad5b70ca5cf9103903a8af4d8a1425539decccd6e6492bfba97c4a5fa7858716_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:list_content.html.twig", "MyAppUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_ad5b70ca5cf9103903a8af4d8a1425539decccd6e6492bfba97c4a5fa7858716->leave($__internal_ad5b70ca5cf9103903a8af4d8a1425539decccd6e6492bfba97c4a5fa7858716_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:list_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
