<?php

/* SensioDistributionBundle::Configurator/layout.html.twig */
class __TwigTemplate_03b87115d8ac80a322a8867c1f74b58590b16d05eba78b9d32f937719fd7a4f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "SensioDistributionBundle::Configurator/layout.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b02c89941f5697adc26a7dd9668d461d0b0f910cf28d583fe1723d50dd08d944 = $this->env->getExtension("native_profiler");
        $__internal_b02c89941f5697adc26a7dd9668d461d0b0f910cf28d583fe1723d50dd08d944->enter($__internal_b02c89941f5697adc26a7dd9668d461d0b0f910cf28d583fe1723d50dd08d944_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SensioDistributionBundle::Configurator/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b02c89941f5697adc26a7dd9668d461d0b0f910cf28d583fe1723d50dd08d944->leave($__internal_b02c89941f5697adc26a7dd9668d461d0b0f910cf28d583fe1723d50dd08d944_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_dc61f4c15b47c959a9afebfb20be8018dece1a1edcef72538e63d03b3ea4a0fe = $this->env->getExtension("native_profiler");
        $__internal_dc61f4c15b47c959a9afebfb20be8018dece1a1edcef72538e63d03b3ea4a0fe->enter($__internal_dc61f4c15b47c959a9afebfb20be8018dece1a1edcef72538e63d03b3ea4a0fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/sensiodistribution/webconfigurator/css/configurator.css"), "html", null, true);
        echo "\" />
";
        
        $__internal_dc61f4c15b47c959a9afebfb20be8018dece1a1edcef72538e63d03b3ea4a0fe->leave($__internal_dc61f4c15b47c959a9afebfb20be8018dece1a1edcef72538e63d03b3ea4a0fe_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_cd3f998a25aa22d751badc328530c8cfcae6e960496c203313a4bfab42cf88bc = $this->env->getExtension("native_profiler");
        $__internal_cd3f998a25aa22d751badc328530c8cfcae6e960496c203313a4bfab42cf88bc->enter($__internal_cd3f998a25aa22d751badc328530c8cfcae6e960496c203313a4bfab42cf88bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Web Configurator Bundle";
        
        $__internal_cd3f998a25aa22d751badc328530c8cfcae6e960496c203313a4bfab42cf88bc->leave($__internal_cd3f998a25aa22d751badc328530c8cfcae6e960496c203313a4bfab42cf88bc_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_dc47f121a161d774abe874067462d9a3fff7e214ff1fa25b2669eb9ae6b807d1 = $this->env->getExtension("native_profiler");
        $__internal_dc47f121a161d774abe874067462d9a3fff7e214ff1fa25b2669eb9ae6b807d1->enter($__internal_dc47f121a161d774abe874067462d9a3fff7e214ff1fa25b2669eb9ae6b807d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "    <div class=\"block\">
        ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 12
        echo "    </div>
    <div class=\"version\">Symfony Standard Edition v.";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["version"]) ? $context["version"] : $this->getContext($context, "version")), "html", null, true);
        echo "</div>
";
        
        $__internal_dc47f121a161d774abe874067462d9a3fff7e214ff1fa25b2669eb9ae6b807d1->leave($__internal_dc47f121a161d774abe874067462d9a3fff7e214ff1fa25b2669eb9ae6b807d1_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_670772437bb5b09d2ddf2734f55db4eee774330b8e1f04541c95d0e7c53d2dde = $this->env->getExtension("native_profiler");
        $__internal_670772437bb5b09d2ddf2734f55db4eee774330b8e1f04541c95d0e7c53d2dde->enter($__internal_670772437bb5b09d2ddf2734f55db4eee774330b8e1f04541c95d0e7c53d2dde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_670772437bb5b09d2ddf2734f55db4eee774330b8e1f04541c95d0e7c53d2dde->leave($__internal_670772437bb5b09d2ddf2734f55db4eee774330b8e1f04541c95d0e7c53d2dde_prof);

    }

    public function getTemplateName()
    {
        return "SensioDistributionBundle::Configurator/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 11,  79 => 13,  76 => 12,  74 => 11,  71 => 10,  65 => 9,  53 => 7,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends "TwigBundle::layout.html.twig" %}*/
/* */
/* {% block head %}*/
/*     <link rel="stylesheet" href="{{ asset('bundles/sensiodistribution/webconfigurator/css/configurator.css') }}" />*/
/* {% endblock %}*/
/* */
/* {% block title 'Web Configurator Bundle' %}*/
/* */
/* {% block body %}*/
/*     <div class="block">*/
/*         {% block content %}{% endblock %}*/
/*     </div>*/
/*     <div class="version">Symfony Standard Edition v.{{ version }}</div>*/
/* {% endblock %}*/
/* */
