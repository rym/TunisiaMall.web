<?php

/* MyAppUserBundle:Group:show.html.twig */
class __TwigTemplate_08d9e967d7deb2624d85d5404b70474568761500a5a0bbccfdcdee2f35973c5b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "MyAppUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0521ff47ba49ebbe8df0feecd0c4a9f453333dfa3923e9e67e4c8dbc24524f42 = $this->env->getExtension("native_profiler");
        $__internal_0521ff47ba49ebbe8df0feecd0c4a9f453333dfa3923e9e67e4c8dbc24524f42->enter($__internal_0521ff47ba49ebbe8df0feecd0c4a9f453333dfa3923e9e67e4c8dbc24524f42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0521ff47ba49ebbe8df0feecd0c4a9f453333dfa3923e9e67e4c8dbc24524f42->leave($__internal_0521ff47ba49ebbe8df0feecd0c4a9f453333dfa3923e9e67e4c8dbc24524f42_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_bdcd3926f73b8aba4007e2371834b99dbace228812f00b507b7cd867c5157810 = $this->env->getExtension("native_profiler");
        $__internal_bdcd3926f73b8aba4007e2371834b99dbace228812f00b507b7cd867c5157810->enter($__internal_bdcd3926f73b8aba4007e2371834b99dbace228812f00b507b7cd867c5157810_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:show_content.html.twig", "MyAppUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_bdcd3926f73b8aba4007e2371834b99dbace228812f00b507b7cd867c5157810->leave($__internal_bdcd3926f73b8aba4007e2371834b99dbace228812f00b507b7cd867c5157810_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
