<?php

/* MyAppUserBundle:Group:new.html.twig */
class __TwigTemplate_09e5856c37740bef10f1adbba305d3f0cf5f35df7d4c284cc1ffcb302797f8dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "MyAppUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ccfbbac63d89ab02fba4c629ceefc64346c174111ec0dd85d853425c458a4f8 = $this->env->getExtension("native_profiler");
        $__internal_8ccfbbac63d89ab02fba4c629ceefc64346c174111ec0dd85d853425c458a4f8->enter($__internal_8ccfbbac63d89ab02fba4c629ceefc64346c174111ec0dd85d853425c458a4f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8ccfbbac63d89ab02fba4c629ceefc64346c174111ec0dd85d853425c458a4f8->leave($__internal_8ccfbbac63d89ab02fba4c629ceefc64346c174111ec0dd85d853425c458a4f8_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_e6f5954d429fac55f702e2ef7095a3ec78abe93a925aeb377e4fa2cdcdbbf155 = $this->env->getExtension("native_profiler");
        $__internal_e6f5954d429fac55f702e2ef7095a3ec78abe93a925aeb377e4fa2cdcdbbf155->enter($__internal_e6f5954d429fac55f702e2ef7095a3ec78abe93a925aeb377e4fa2cdcdbbf155_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:new_content.html.twig", "MyAppUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_e6f5954d429fac55f702e2ef7095a3ec78abe93a925aeb377e4fa2cdcdbbf155->leave($__internal_e6f5954d429fac55f702e2ef7095a3ec78abe93a925aeb377e4fa2cdcdbbf155_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:new_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
