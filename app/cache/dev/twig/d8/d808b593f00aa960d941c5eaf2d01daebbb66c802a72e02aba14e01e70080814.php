<?php

/* AcmeDemoBundle:Demo:contact.html.twig */
class __TwigTemplate_8b0d0246260e4338b94c4e3daa927a4e78fd8412ceef79683d0a12a466006e68 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AcmeDemoBundle::layout.html.twig", "AcmeDemoBundle:Demo:contact.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AcmeDemoBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_68e3041d88b8bc63cf43d77f25fe3152c90fa7c121d0acc47e3181ba3f1cff46 = $this->env->getExtension("native_profiler");
        $__internal_68e3041d88b8bc63cf43d77f25fe3152c90fa7c121d0acc47e3181ba3f1cff46->enter($__internal_68e3041d88b8bc63cf43d77f25fe3152c90fa7c121d0acc47e3181ba3f1cff46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AcmeDemoBundle:Demo:contact.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_68e3041d88b8bc63cf43d77f25fe3152c90fa7c121d0acc47e3181ba3f1cff46->leave($__internal_68e3041d88b8bc63cf43d77f25fe3152c90fa7c121d0acc47e3181ba3f1cff46_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_cfef3458b490ea50ffc4047a69647bf74a6818d81cb4ebcd1ddf5392a08fc06b = $this->env->getExtension("native_profiler");
        $__internal_cfef3458b490ea50ffc4047a69647bf74a6818d81cb4ebcd1ddf5392a08fc06b->enter($__internal_cfef3458b490ea50ffc4047a69647bf74a6818d81cb4ebcd1ddf5392a08fc06b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Symfony - Contact form";
        
        $__internal_cfef3458b490ea50ffc4047a69647bf74a6818d81cb4ebcd1ddf5392a08fc06b->leave($__internal_cfef3458b490ea50ffc4047a69647bf74a6818d81cb4ebcd1ddf5392a08fc06b_prof);

    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        $__internal_59fb2661b705edb56dd5b00b199e5d43bfb1d41841020033ef27a60ffe1dbf23 = $this->env->getExtension("native_profiler");
        $__internal_59fb2661b705edb56dd5b00b199e5d43bfb1d41841020033ef27a60ffe1dbf23->enter($__internal_59fb2661b705edb56dd5b00b199e5d43bfb1d41841020033ef27a60ffe1dbf23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <form action=\"";
        echo $this->env->getExtension('routing')->getPath("_demo_contact");
        echo "\" method=\"POST\" id=\"contact_form\">
        ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "

        ";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'row');
        echo "
        ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "message", array()), 'row');
        echo "

        ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        echo "
        <input type=\"submit\" value=\"Send\" class=\"symfony-button-grey\" />
    </form>
";
        
        $__internal_59fb2661b705edb56dd5b00b199e5d43bfb1d41841020033ef27a60ffe1dbf23->leave($__internal_59fb2661b705edb56dd5b00b199e5d43bfb1d41841020033ef27a60ffe1dbf23_prof);

    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle:Demo:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 12,  67 => 10,  63 => 9,  58 => 7,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends "AcmeDemoBundle::layout.html.twig" %}*/
/* */
/* {% block title "Symfony - Contact form" %}*/
/* */
/* {% block content %}*/
/*     <form action="{{ path('_demo_contact') }}" method="POST" id="contact_form">*/
/*         {{ form_errors(form) }}*/
/* */
/*         {{ form_row(form.email) }}*/
/*         {{ form_row(form.message) }}*/
/* */
/*         {{ form_rest(form) }}*/
/*         <input type="submit" value="Send" class="symfony-button-grey" />*/
/*     </form>*/
/* {% endblock %}*/
/* */
