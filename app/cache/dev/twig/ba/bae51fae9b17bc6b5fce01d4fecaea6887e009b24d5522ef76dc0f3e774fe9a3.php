<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_dca5fb139f80ddff341b344a3c2cb2726964867be83c67718281b4cf2d3738a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_052dca75c5e955766bf5a565fc93285b2d5722efe2f255b935ef525bfabbdcf7 = $this->env->getExtension("native_profiler");
        $__internal_052dca75c5e955766bf5a565fc93285b2d5722efe2f255b935ef525bfabbdcf7->enter($__internal_052dca75c5e955766bf5a565fc93285b2d5722efe2f255b935ef525bfabbdcf7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_052dca75c5e955766bf5a565fc93285b2d5722efe2f255b935ef525bfabbdcf7->leave($__internal_052dca75c5e955766bf5a565fc93285b2d5722efe2f255b935ef525bfabbdcf7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
