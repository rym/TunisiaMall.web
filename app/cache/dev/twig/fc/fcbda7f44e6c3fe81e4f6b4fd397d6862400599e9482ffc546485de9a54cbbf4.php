<?php

/* MyAppUserBundle:Registration:checkEmail.html.twig */
class __TwigTemplate_f4aee989ac800762f2c24c5e26121a106c9985527b06c9386e3041d7ac2b6e61 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "MyAppUserBundle:Registration:checkEmail.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2e565fd931334a55c9716e44eeaa4b977b80281b74d0502aa54efbac754667ce = $this->env->getExtension("native_profiler");
        $__internal_2e565fd931334a55c9716e44eeaa4b977b80281b74d0502aa54efbac754667ce->enter($__internal_2e565fd931334a55c9716e44eeaa4b977b80281b74d0502aa54efbac754667ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Registration:checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2e565fd931334a55c9716e44eeaa4b977b80281b74d0502aa54efbac754667ce->leave($__internal_2e565fd931334a55c9716e44eeaa4b977b80281b74d0502aa54efbac754667ce_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_e907437d23968ad2fe730d7e56ecbb94799e9f84d7ba44e53dfce1b0afb75d02 = $this->env->getExtension("native_profiler");
        $__internal_e907437d23968ad2fe730d7e56ecbb94799e9f84d7ba44e53dfce1b0afb75d02->enter($__internal_e907437d23968ad2fe730d7e56ecbb94799e9f84d7ba44e53dfce1b0afb75d02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_e907437d23968ad2fe730d7e56ecbb94799e9f84d7ba44e53dfce1b0afb75d02->leave($__internal_e907437d23968ad2fe730d7e56ecbb94799e9f84d7ba44e53dfce1b0afb75d02_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Registration:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>*/
/* {% endblock fos_user_content %}*/
/* */
