<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_23b1ba5787b5ae75af16364607d63c9cc0743906c22cf28c7716ecd93872c5dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8be2de77bbb1a89423ad7501ae7de69c2919780fee1c26f49be0ad42d3ae0fc = $this->env->getExtension("native_profiler");
        $__internal_d8be2de77bbb1a89423ad7501ae7de69c2919780fee1c26f49be0ad42d3ae0fc->enter($__internal_d8be2de77bbb1a89423ad7501ae7de69c2919780fee1c26f49be0ad42d3ae0fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d8be2de77bbb1a89423ad7501ae7de69c2919780fee1c26f49be0ad42d3ae0fc->leave($__internal_d8be2de77bbb1a89423ad7501ae7de69c2919780fee1c26f49be0ad42d3ae0fc_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_32b0acacedf784b08fe6a4e91e4b43b24ee2490fbe187a91dd8f289dd03ba740 = $this->env->getExtension("native_profiler");
        $__internal_32b0acacedf784b08fe6a4e91e4b43b24ee2490fbe187a91dd8f289dd03ba740->enter($__internal_32b0acacedf784b08fe6a4e91e4b43b24ee2490fbe187a91dd8f289dd03ba740_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Registration:register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_32b0acacedf784b08fe6a4e91e4b43b24ee2490fbe187a91dd8f289dd03ba740->leave($__internal_32b0acacedf784b08fe6a4e91e4b43b24ee2490fbe187a91dd8f289dd03ba740_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Registration:register_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
