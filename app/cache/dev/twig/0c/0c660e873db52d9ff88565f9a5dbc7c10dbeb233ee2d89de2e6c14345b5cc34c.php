<?php

/* AcmeDemoBundle:Demo:index.html.twig */
class __TwigTemplate_085364f78ffae187da49ff32d4f72cfc3b8452d3b445ef0d64f653ebe8916ffa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AcmeDemoBundle::layout.html.twig", "AcmeDemoBundle:Demo:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AcmeDemoBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc90e83dd9d7d37a0a0d18a6493583d0bbe8efa5c5b17a4b4b7929b6afddf59b = $this->env->getExtension("native_profiler");
        $__internal_cc90e83dd9d7d37a0a0d18a6493583d0bbe8efa5c5b17a4b4b7929b6afddf59b->enter($__internal_cc90e83dd9d7d37a0a0d18a6493583d0bbe8efa5c5b17a4b4b7929b6afddf59b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AcmeDemoBundle:Demo:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cc90e83dd9d7d37a0a0d18a6493583d0bbe8efa5c5b17a4b4b7929b6afddf59b->leave($__internal_cc90e83dd9d7d37a0a0d18a6493583d0bbe8efa5c5b17a4b4b7929b6afddf59b_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_9355c74a94ee858dca4e9dc962d623b3147144b47c87254873331f7b4a0f3493 = $this->env->getExtension("native_profiler");
        $__internal_9355c74a94ee858dca4e9dc962d623b3147144b47c87254873331f7b4a0f3493->enter($__internal_9355c74a94ee858dca4e9dc962d623b3147144b47c87254873331f7b4a0f3493_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Symfony - Demos";
        
        $__internal_9355c74a94ee858dca4e9dc962d623b3147144b47c87254873331f7b4a0f3493->leave($__internal_9355c74a94ee858dca4e9dc962d623b3147144b47c87254873331f7b4a0f3493_prof);

    }

    // line 5
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_7c28aa797619da9b6781352ed1c7bec4a67617af93484f3b0f3cba7820716144 = $this->env->getExtension("native_profiler");
        $__internal_7c28aa797619da9b6781352ed1c7bec4a67617af93484f3b0f3cba7820716144->enter($__internal_7c28aa797619da9b6781352ed1c7bec4a67617af93484f3b0f3cba7820716144_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        echo "";
        
        $__internal_7c28aa797619da9b6781352ed1c7bec4a67617af93484f3b0f3cba7820716144->leave($__internal_7c28aa797619da9b6781352ed1c7bec4a67617af93484f3b0f3cba7820716144_prof);

    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        $__internal_7a0862a9d3d09cd92b733f87a7366b3ba53df70f3f50532f7449a42849f6322e = $this->env->getExtension("native_profiler");
        $__internal_7a0862a9d3d09cd92b733f87a7366b3ba53df70f3f50532f7449a42849f6322e->enter($__internal_7a0862a9d3d09cd92b733f87a7366b3ba53df70f3f50532f7449a42849f6322e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 8
        echo "    <h1 class=\"title\">Available demos</h1>
    <ul id=\"demo-list\">
        <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("_demo_hello", array("name" => "World"));
        echo "\">Hello World</a></li>
        <li><a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("_demo_secured_hello", array("name" => "World"));
        echo "\">Access the secured area</a> <a href=\"";
        echo $this->env->getExtension('routing')->getPath("_demo_login");
        echo "\">Go to the login page</a></li>
        ";
        // line 13
        echo "    </ul>
";
        
        $__internal_7a0862a9d3d09cd92b733f87a7366b3ba53df70f3f50532f7449a42849f6322e->leave($__internal_7a0862a9d3d09cd92b733f87a7366b3ba53df70f3f50532f7449a42849f6322e_prof);

    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle:Demo:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 13,  74 => 11,  70 => 10,  66 => 8,  60 => 7,  48 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends "AcmeDemoBundle::layout.html.twig" %}*/
/* */
/* {% block title "Symfony - Demos" %}*/
/* */
/* {% block content_header '' %}*/
/* */
/* {% block content %}*/
/*     <h1 class="title">Available demos</h1>*/
/*     <ul id="demo-list">*/
/*         <li><a href="{{ path('_demo_hello', {'name': 'World'}) }}">Hello World</a></li>*/
/*         <li><a href="{{ path('_demo_secured_hello', {'name': 'World'}) }}">Access the secured area</a> <a href="{{ path('_demo_login') }}">Go to the login page</a></li>*/
/*         {# <li><a href="{{ path('_demo_contact') }}">Send a Message</a></li> #}*/
/*     </ul>*/
/* {% endblock %}*/
/* */
