<?php

/* MyAppUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_41bb3a016c90cf002d96eaf19f6d328734404f963d6747eb4dd9896b4d2c5c10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "MyAppUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_44554e358320a92ebb0a41fda4e9fb2a442442149cdd4abc443a2bd733b6508f = $this->env->getExtension("native_profiler");
        $__internal_44554e358320a92ebb0a41fda4e9fb2a442442149cdd4abc443a2bd733b6508f->enter($__internal_44554e358320a92ebb0a41fda4e9fb2a442442149cdd4abc443a2bd733b6508f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_44554e358320a92ebb0a41fda4e9fb2a442442149cdd4abc443a2bd733b6508f->leave($__internal_44554e358320a92ebb0a41fda4e9fb2a442442149cdd4abc443a2bd733b6508f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_1ea408aa02952a96ce46d07290bd498c87899d42fd94b79f466c37ebde2775b6 = $this->env->getExtension("native_profiler");
        $__internal_1ea408aa02952a96ce46d07290bd498c87899d42fd94b79f466c37ebde2775b6->enter($__internal_1ea408aa02952a96ce46d07290bd498c87899d42fd94b79f466c37ebde2775b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:reset_content.html.twig", "MyAppUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_1ea408aa02952a96ce46d07290bd498c87899d42fd94b79f466c37ebde2775b6->leave($__internal_1ea408aa02952a96ce46d07290bd498c87899d42fd94b79f466c37ebde2775b6_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:reset_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
