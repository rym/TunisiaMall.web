<?php

/* FOSUserBundle:ChangePassword:changePassword_content.html.twig */
class __TwigTemplate_94796d1331448a4dccf38bba951f63dd9de82c4010b5673175dbdc691444c2d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f0d2dfc040ca8d14d70401c8d299f79154bf7b1b02f1aab0f4d9d1d85ccf5855 = $this->env->getExtension("native_profiler");
        $__internal_f0d2dfc040ca8d14d70401c8d299f79154bf7b1b02f1aab0f4d9d1d85ccf5855->enter($__internal_f0d2dfc040ca8d14d70401c8d299f79154bf7b1b02f1aab0f4d9d1d85ccf5855_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:changePassword_content.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('routing')->getPath("fos_user_change_password"), "attr" => array("class" => "fos_user_change_password")));
        echo "
";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token", array()), 'row');
        echo "
<div class=\"login-mail\">
    ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "current_password", array()), 'widget', array("attr" => array("placeholder" => "ancien mot de passe")));
        echo "
    <i class=\"fa fa-user\"></i>
</div>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["passwordField"]) {
            // line 10
            echo "    <div class=\"login-mail\">
        ";
            // line 11
            if (($this->getAttribute($context["loop"], "index", array()) == 1)) {
                // line 12
                echo "            ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["passwordField"], 'widget', array("attr" => array("placeholder" => "Mot de passe")));
                echo "
            <i class=\"fa fa-lock\"></i>
        ";
            } else {
                // line 15
                echo "            ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["passwordField"], 'widget', array("attr" => array("placeholder" => "confirmation")));
                echo "
            <i class=\"fa fa-lock\"></i>
        ";
            }
            // line 18
            echo "    </div>
";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['passwordField'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "<div class=\"login-do\">
    <label class=\"hvr-shutter-in-horizontal login-sub\">
        <input type=\"submit\" id=\"_submit\" class=\"btn btn-success\" name=\"_submit\" value=\"Modifier mot de passe\">
    </label>
</div>
";
        // line 25
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_f0d2dfc040ca8d14d70401c8d299f79154bf7b1b02f1aab0f4d9d1d85ccf5855->leave($__internal_f0d2dfc040ca8d14d70401c8d299f79154bf7b1b02f1aab0f4d9d1d85ccf5855_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:changePassword_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 25,  91 => 20,  76 => 18,  69 => 15,  62 => 12,  60 => 11,  57 => 10,  40 => 9,  34 => 6,  29 => 4,  25 => 3,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {{ form_start(form, { 'action': path('fos_user_change_password'), 'attr': { 'class': 'fos_user_change_password' } }) }}*/
/* {{ form_row(form._token) }}*/
/* <div class="login-mail">*/
/*     {{ form_widget(form.current_password,{ 'attr': {'placeholder': 'ancien mot de passe'} }) }}*/
/*     <i class="fa fa-user"></i>*/
/* </div>*/
/* {% for passwordField in form.plainPassword %}*/
/*     <div class="login-mail">*/
/*         {% if loop.index == 1 %}*/
/*             {{form_widget(passwordField,{ 'attr': {'placeholder': 'Mot de passe'} })}}*/
/*             <i class="fa fa-lock"></i>*/
/*         {%else%}*/
/*             {{form_widget(passwordField,{ 'attr': {'placeholder': 'confirmation'} })}}*/
/*             <i class="fa fa-lock"></i>*/
/*         {%endif%}*/
/*     </div>*/
/* {% endfor %}*/
/* <div class="login-do">*/
/*     <label class="hvr-shutter-in-horizontal login-sub">*/
/*         <input type="submit" id="_submit" class="btn btn-success" name="_submit" value="Modifier mot de passe">*/
/*     </label>*/
/* </div>*/
/* {{ form_end(form) }}*/
/* */
