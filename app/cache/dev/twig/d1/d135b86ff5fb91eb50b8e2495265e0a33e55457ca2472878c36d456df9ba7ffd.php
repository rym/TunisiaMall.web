<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_4465a18bea43facc25daa350ad79f98eeb0f57da35f34fbdc0ef56b7ccea9151 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3bb22fae19c975f491b869ddc32cb1a0527a432b7a6ef7a211c8e7d1b0ae704 = $this->env->getExtension("native_profiler");
        $__internal_b3bb22fae19c975f491b869ddc32cb1a0527a432b7a6ef7a211c8e7d1b0ae704->enter($__internal_b3bb22fae19c975f491b869ddc32cb1a0527a432b7a6ef7a211c8e7d1b0ae704_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_b3bb22fae19c975f491b869ddc32cb1a0527a432b7a6ef7a211c8e7d1b0ae704->leave($__internal_b3bb22fae19c975f491b869ddc32cb1a0527a432b7a6ef7a211c8e7d1b0ae704_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
