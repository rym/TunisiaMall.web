<?php

/* AcmeDemoBundle:Secured:hello.html.twig */
class __TwigTemplate_14c68ce72d25438623abf623457de92115ded4d51fb3ade240ce39780ba20d13 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AcmeDemoBundle:Secured:layout.html.twig", "AcmeDemoBundle:Secured:hello.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AcmeDemoBundle:Secured:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_386fe9e496d4a2d482bd948d33e2097553aa5fa703f5bac6e87ba76b579f37ae = $this->env->getExtension("native_profiler");
        $__internal_386fe9e496d4a2d482bd948d33e2097553aa5fa703f5bac6e87ba76b579f37ae->enter($__internal_386fe9e496d4a2d482bd948d33e2097553aa5fa703f5bac6e87ba76b579f37ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AcmeDemoBundle:Secured:hello.html.twig"));

        // line 11
        $context["code"] = $this->env->getExtension('demo')->getCode($this);
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_386fe9e496d4a2d482bd948d33e2097553aa5fa703f5bac6e87ba76b579f37ae->leave($__internal_386fe9e496d4a2d482bd948d33e2097553aa5fa703f5bac6e87ba76b579f37ae_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_4494c45ce0bb81cd25464d5acc1eb37c069e95e87ec01561a31ac9302c379cdc = $this->env->getExtension("native_profiler");
        $__internal_4494c45ce0bb81cd25464d5acc1eb37c069e95e87ec01561a31ac9302c379cdc->enter($__internal_4494c45ce0bb81cd25464d5acc1eb37c069e95e87ec01561a31ac9302c379cdc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, ("Hello " . (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name"))), "html", null, true);
        
        $__internal_4494c45ce0bb81cd25464d5acc1eb37c069e95e87ec01561a31ac9302c379cdc->leave($__internal_4494c45ce0bb81cd25464d5acc1eb37c069e95e87ec01561a31ac9302c379cdc_prof);

    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        $__internal_4bd7c05e77b5a1002fd35cd7057c4f209051df4e522c082049522df65d9afb11 = $this->env->getExtension("native_profiler");
        $__internal_4bd7c05e77b5a1002fd35cd7057c4f209051df4e522c082049522df65d9afb11->enter($__internal_4bd7c05e77b5a1002fd35cd7057c4f209051df4e522c082049522df65d9afb11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <h1 class=\"title\">Hello ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "!</h1>

    <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_demo_secured_hello_admin", array("name" => (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")))), "html", null, true);
        echo "\">Hello resource secured for <strong>admin</strong> only.</a>
";
        
        $__internal_4bd7c05e77b5a1002fd35cd7057c4f209051df4e522c082049522df65d9afb11->leave($__internal_4bd7c05e77b5a1002fd35cd7057c4f209051df4e522c082049522df65d9afb11_prof);

    }

    public function getTemplateName()
    {
        return "AcmeDemoBundle:Secured:hello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 8,  56 => 6,  50 => 5,  38 => 3,  31 => 1,  29 => 11,  11 => 1,);
    }
}
/* {% extends "AcmeDemoBundle:Secured:layout.html.twig" %}*/
/* */
/* {% block title "Hello " ~ name %}*/
/* */
/* {% block content %}*/
/*     <h1 class="title">Hello {{ name }}!</h1>*/
/* */
/*     <a href="{{ path('_demo_secured_hello_admin', { 'name': name }) }}">Hello resource secured for <strong>admin</strong> only.</a>*/
/* {% endblock %}*/
/* */
/* {% set code = code(_self) %}*/
/* */
