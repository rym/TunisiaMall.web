<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_33b8c092bcf2cec2b98788f3934fea11e9dc0198ef73342d55274f854d0593a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8f5b85c9dff6f53d732035772bbbde8246a280b676ee4c0a15d583642f3c595 = $this->env->getExtension("native_profiler");
        $__internal_d8f5b85c9dff6f53d732035772bbbde8246a280b676ee4c0a15d583642f3c595->enter($__internal_d8f5b85c9dff6f53d732035772bbbde8246a280b676ee4c0a15d583642f3c595_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("TwigBundle:Exception:exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_d8f5b85c9dff6f53d732035772bbbde8246a280b676ee4c0a15d583642f3c595->leave($__internal_d8f5b85c9dff6f53d732035772bbbde8246a280b676ee4c0a15d583642f3c595_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include 'TwigBundle:Exception:exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
