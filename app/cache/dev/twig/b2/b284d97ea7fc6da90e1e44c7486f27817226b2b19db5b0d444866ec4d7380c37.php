<?php

/* MyAppUserBundle:Profile:edit.html.twig */
class __TwigTemplate_05232256d0bc59f48356939bda206e1b96d4bb6f2d9fc5e9c24e47f8510499d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "MyAppUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a3ea00f62c768fe684367d5dfcb6a7c7f615311d2d47527f7a5d95b8f4e66084 = $this->env->getExtension("native_profiler");
        $__internal_a3ea00f62c768fe684367d5dfcb6a7c7f615311d2d47527f7a5d95b8f4e66084->enter($__internal_a3ea00f62c768fe684367d5dfcb6a7c7f615311d2d47527f7a5d95b8f4e66084_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a3ea00f62c768fe684367d5dfcb6a7c7f615311d2d47527f7a5d95b8f4e66084->leave($__internal_a3ea00f62c768fe684367d5dfcb6a7c7f615311d2d47527f7a5d95b8f4e66084_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a9d7dd0e308c4c9d1ede80208f8839a1bcd275bad55e9ed5979705e92ae8cc72 = $this->env->getExtension("native_profiler");
        $__internal_a9d7dd0e308c4c9d1ede80208f8839a1bcd275bad55e9ed5979705e92ae8cc72->enter($__internal_a9d7dd0e308c4c9d1ede80208f8839a1bcd275bad55e9ed5979705e92ae8cc72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:edit_content.html.twig", "MyAppUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_a9d7dd0e308c4c9d1ede80208f8839a1bcd275bad55e9ed5979705e92ae8cc72->leave($__internal_a9d7dd0e308c4c9d1ede80208f8839a1bcd275bad55e9ed5979705e92ae8cc72_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
