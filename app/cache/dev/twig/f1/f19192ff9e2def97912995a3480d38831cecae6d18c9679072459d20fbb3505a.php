<?php

/* MyAppUserBundle:Resetting:checkEmail.html.twig */
class __TwigTemplate_16621412bdc89519da8c366121120b8303f795f161448f5b03ed912840577f50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "MyAppUserBundle:Resetting:checkEmail.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7568b943cdfe46626ca3fc127c15fe92f492b4b9301002826a0a24992643d8f5 = $this->env->getExtension("native_profiler");
        $__internal_7568b943cdfe46626ca3fc127c15fe92f492b4b9301002826a0a24992643d8f5->enter($__internal_7568b943cdfe46626ca3fc127c15fe92f492b4b9301002826a0a24992643d8f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:Resetting:checkEmail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7568b943cdfe46626ca3fc127c15fe92f492b4b9301002826a0a24992643d8f5->leave($__internal_7568b943cdfe46626ca3fc127c15fe92f492b4b9301002826a0a24992643d8f5_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_10a52a1457b1fae7bd114d38c5a2cff4f64039a54ab1ca286097a363e8476b3b = $this->env->getExtension("native_profiler");
        $__internal_10a52a1457b1fae7bd114d38c5a2cff4f64039a54ab1ca286097a363e8476b3b->enter($__internal_10a52a1457b1fae7bd114d38c5a2cff4f64039a54ab1ca286097a363e8476b3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.check_email", array("%email%" => (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email"))), "FOSUserBundle"), "html", null, true);
        echo "
</p>
";
        
        $__internal_10a52a1457b1fae7bd114d38c5a2cff4f64039a54ab1ca286097a363e8476b3b->leave($__internal_10a52a1457b1fae7bd114d38c5a2cff4f64039a54ab1ca286097a363e8476b3b_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:Resetting:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* <p>*/
/* {{ 'resetting.check_email'|trans({'%email%': email}) }}*/
/* </p>*/
/* {% endblock %}*/
/* */
