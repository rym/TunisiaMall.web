<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_4049e0dee4982050217635b16ec9aca56f9a12582efdc5568e1403c73b807bb8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f44e232488d3141c95977d6133970015d072edf882f896a1977ac3bdca6dbb3b = $this->env->getExtension("native_profiler");
        $__internal_f44e232488d3141c95977d6133970015d072edf882f896a1977ac3bdca6dbb3b->enter($__internal_f44e232488d3141c95977d6133970015d072edf882f896a1977ac3bdca6dbb3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("TwigBundle:Exception:exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_f44e232488d3141c95977d6133970015d072edf882f896a1977ac3bdca6dbb3b->leave($__internal_f44e232488d3141c95977d6133970015d072edf882f896a1977ac3bdca6dbb3b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include 'TwigBundle:Exception:exception.xml.twig' with { 'exception': exception } %}*/
/* */
