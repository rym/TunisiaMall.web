<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/admin')) {
            // my_app_admin_homepage
            if (rtrim($pathinfo, '/') === '/admin') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'my_app_admin_homepage');
                }

                return array (  '_controller' => 'MyApp\\AdminBundle\\Controller\\AdminController::indexAction',  '_route' => 'my_app_admin_homepage',);
            }

            // my_app_admin_clients
            if ($pathinfo === '/admin/clients') {
                return array (  '_controller' => 'MyApp\\AdminBundle\\Controller\\AdminController::clientsAction',  '_route' => 'my_app_admin_clients',);
            }

            if (0 === strpos($pathinfo, '/admin/de')) {
                // my_app_admin_delete_client
                if (0 === strpos($pathinfo, '/admin/delete-client') && preg_match('#^/admin/delete\\-client/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_admin_delete_client')), array (  '_controller' => 'MyApp\\AdminBundle\\Controller\\AdminController::deleteClientAction',));
                }

                // my_app_admin_debloquer_client
                if (0 === strpos($pathinfo, '/admin/debloquer-client') && preg_match('#^/admin/debloquer\\-client/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_admin_debloquer_client')), array (  '_controller' => 'MyApp\\AdminBundle\\Controller\\AdminController::debloquerClientAction',));
                }

            }

            // my_app_admin_bloquer_client
            if (0 === strpos($pathinfo, '/admin/bloquer-client') && preg_match('#^/admin/bloquer\\-client/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_admin_bloquer_client')), array (  '_controller' => 'MyApp\\AdminBundle\\Controller\\AdminController::bloquerClientAction',));
            }

            // my_app_admin_responsables
            if ($pathinfo === '/admin/responsables') {
                return array (  '_controller' => 'MyApp\\AdminBundle\\Controller\\AdminController::responsablesAction',  '_route' => 'my_app_admin_responsables',);
            }

            if (0 === strpos($pathinfo, '/admin/de')) {
                // my_app_admin_delete_responsable
                if (0 === strpos($pathinfo, '/admin/delete-responsable') && preg_match('#^/admin/delete\\-responsable/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_admin_delete_responsable')), array (  '_controller' => 'MyApp\\AdminBundle\\Controller\\AdminController::deleteResponsableAction',));
                }

                // my_app_admin_debloquer_responsable
                if (0 === strpos($pathinfo, '/admin/debloquer-responsable') && preg_match('#^/admin/debloquer\\-responsable/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_admin_debloquer_responsable')), array (  '_controller' => 'MyApp\\AdminBundle\\Controller\\AdminController::debloquerResponsableAction',));
                }

            }

            // my_app_admin_bloquer_responsable
            if (0 === strpos($pathinfo, '/admin/bloquer-responsable') && preg_match('#^/admin/bloquer\\-responsable/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_admin_bloquer_responsable')), array (  '_controller' => 'MyApp\\AdminBundle\\Controller\\AdminController::bloquerResponsableAction',));
            }

        }

        // my_app_user_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_user_homepage')), array (  '_controller' => 'MyAppUserBundle:Default:index',));
        }

        // my_app_register_client
        if ($pathinfo === '/register/client') {
            return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\RegistrationController::resgisterClientAction',  '_route' => 'my_app_register_client',);
        }

        // my_app_home
        if ($pathinfo === '/home') {
            return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\TunisiaMallController::indexAction',  '_route' => 'my_app_home',);
        }

        // my_app_profile
        if ($pathinfo === '/profile/responsable') {
            return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\TunisiaMallController::profileAction',  '_route' => 'my_app_profile',);
        }

        // my_app_register_responsable
        if ($pathinfo === '/register/responsable') {
            return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\RegistrationController::resgisterResponsableAction',  '_route' => 'my_app_register_responsable',);
        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'MyApp\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'MyApp\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        if (0 === strpos($pathinfo, '/l')) {
            if (0 === strpos($pathinfo, '/log')) {
                if (0 === strpos($pathinfo, '/login')) {
                    // fos_user_security_login
                    if ($pathinfo === '/login') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_fos_user_security_login;
                        }

                        return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                    }
                    not_fos_user_security_login:

                    // fos_user_security_check
                    if ($pathinfo === '/login_check') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_fos_user_security_check;
                        }

                        return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                    }
                    not_fos_user_security_check:

                }

                // fos_user_security_logout
                if ($pathinfo === '/logout') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_security_logout;
                    }

                    return array (  '_controller' => 'MyApp\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
                }
                not_fos_user_security_logout:

            }

            if (0 === strpos($pathinfo, '/li')) {
                // my_app_user_liste
                if ($pathinfo === '/li') {
                    return array (  '_controller' => 'MyAppUserBundle:User:liste',  '_route' => 'my_app_user_liste',);
                }

                // my_app_user_liste2
                if ($pathinfo === '/lin') {
                    return array (  '_controller' => 'MyAppUserBundle:User:list',  '_route' => 'my_app_user_liste2',);
                }

            }

        }

        // my_app_user_accept
        if (0 === strpos($pathinfo, '/accept') && preg_match('#^/accept/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_user_accept')), array (  '_controller' => 'MyAppUserBundle:User:accept',));
        }

        // my_app_user_refus
        if (0 === strpos($pathinfo, '/refus') && preg_match('#^/refus/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_user_refus')), array (  '_controller' => 'MyAppUserBundle:User:refus',));
        }

        // my_app_user_bloquer
        if (0 === strpos($pathinfo, '/bloquer') && preg_match('#^/bloquer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_user_bloquer')), array (  '_controller' => 'MyAppUserBundle:User:bloquer',));
        }

        if (0 === strpos($pathinfo, '/debloquer')) {
            // my_app_user_debloquer
            if (preg_match('#^/debloquer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_user_debloquer')), array (  '_controller' => 'MyAppUserBundle:User:debloquer',));
            }

            // my_app_user_Gerer
            if (preg_match('#^/debloquer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_app_user_Gerer')), array (  '_controller' => 'MyAppUserBundle:User:debloquer',));
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
