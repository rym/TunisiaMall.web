<?php

/* MyAppAdminBundle:Admin:clients.html.twig */
class __TwigTemplate_cbf671ffde6118264244cb2db020c1ae43bfd6cbda48092da18501485254de31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MyAppAdminBundle::layout.html.twig", "MyAppAdminBundle:Admin:clients.html.twig", 1);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MyAppAdminBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d47b4b88400fd19f210b9e2ba60b918d4abdd83d1f08c3b2c57ee13435fba763 = $this->env->getExtension("native_profiler");
        $__internal_d47b4b88400fd19f210b9e2ba60b918d4abdd83d1f08c3b2c57ee13435fba763->enter($__internal_d47b4b88400fd19f210b9e2ba60b918d4abdd83d1f08c3b2c57ee13435fba763_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppAdminBundle:Admin:clients.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d47b4b88400fd19f210b9e2ba60b918d4abdd83d1f08c3b2c57ee13435fba763->leave($__internal_d47b4b88400fd19f210b9e2ba60b918d4abdd83d1f08c3b2c57ee13435fba763_prof);

    }

    // line 2
    public function block_container($context, array $blocks = array())
    {
        $__internal_01170efbcc4bdd8fae17ef1a6192cad133b79954ce6f00d9f12abfdd31e5cf5d = $this->env->getExtension("native_profiler");
        $__internal_01170efbcc4bdd8fae17ef1a6192cad133b79954ce6f00d9f12abfdd31e5cf5d->enter($__internal_01170efbcc4bdd8fae17ef1a6192cad133b79954ce6f00d9f12abfdd31e5cf5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 3
        echo "    <div class=\"table-responsive\">          
        <table class=\"table\">
            <thead>
                <tr>
                    <th> nom</th>
                    <th> prenom</th>
                    <th> user name</th>
                    <th> Email</th>
                    <th colspan=\"2\">Action</th>
                </tr>
                ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clients"]) ? $context["clients"] : $this->getContext($context, "clients")));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 14
            echo "                    <tr>
                        <td> ";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "nom", array()), "html", null, true);
            echo "</td>
                        <td> ";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "prenom", array()), "html", null, true);
            echo "</td>
                        <td> ";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "username", array()), "html", null, true);
            echo "</td>
                        <td> ";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "email", array()), "html", null, true);
            echo "</td>
                        <td> <a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_app_admin_delete_client", array("id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-danger\" >Supprimer</a></td>
                        ";
            // line 20
            if (($this->getAttribute($context["client"], "locked", array()) == true)) {
                // line 21
                echo "                            <td> <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_app_admin_debloquer_client", array("id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-info\" >Activer</a></td>
                        ";
            } else {
                // line 23
                echo "                            <td> <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_app_admin_bloquer_client", array("id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-danger\" >Bloquer</a></td>
                        ";
            }
            // line 25
            echo "                            
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
            </thead>
        </table>
    </div>

";
        
        $__internal_01170efbcc4bdd8fae17ef1a6192cad133b79954ce6f00d9f12abfdd31e5cf5d->leave($__internal_01170efbcc4bdd8fae17ef1a6192cad133b79954ce6f00d9f12abfdd31e5cf5d_prof);

    }

    public function getTemplateName()
    {
        return "MyAppAdminBundle:Admin:clients.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 28,  93 => 25,  87 => 23,  81 => 21,  79 => 20,  75 => 19,  71 => 18,  67 => 17,  63 => 16,  59 => 15,  56 => 14,  52 => 13,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "MyAppAdminBundle::layout.html.twig" %}*/
/* {% block container %}*/
/*     <div class="table-responsive">          */
/*         <table class="table">*/
/*             <thead>*/
/*                 <tr>*/
/*                     <th> nom</th>*/
/*                     <th> prenom</th>*/
/*                     <th> user name</th>*/
/*                     <th> Email</th>*/
/*                     <th colspan="2">Action</th>*/
/*                 </tr>*/
/*                 {% for client in clients %}*/
/*                     <tr>*/
/*                         <td> {{client.nom}}</td>*/
/*                         <td> {{client.prenom}}</td>*/
/*                         <td> {{client.username}}</td>*/
/*                         <td> {{client.email}}</td>*/
/*                         <td> <a href="{{path('my_app_admin_delete_client',{'id':client.id})}}" class="btn btn-danger" >Supprimer</a></td>*/
/*                         {%if client.locked == true%}*/
/*                             <td> <a href="{{path('my_app_admin_debloquer_client',{'id':client.id})}}" class="btn btn-info" >Activer</a></td>*/
/*                         {%else%}*/
/*                             <td> <a href="{{path('my_app_admin_bloquer_client',{'id':client.id})}}" class="btn btn-danger" >Bloquer</a></td>*/
/*                         {% endif %}*/
/*                             */
/*                     </tr>*/
/*                 {% endfor %}*/
/* */
/*             </thead>*/
/*         </table>*/
/*     </div>*/
/* */
/* {% endblock container %}*/
/* */
/* */
