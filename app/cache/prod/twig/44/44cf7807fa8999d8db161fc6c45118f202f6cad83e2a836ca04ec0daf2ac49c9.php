<?php

/* MyAppUserBundle:public:public.html.twig */
class __TwigTemplate_1e12c64559fbdba1353edb8ebf837a6e242ede9129fae7acc0c042a1cf693efa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1ba829efb081992d8c200d79048260bce8b593fb153a77df958298e2628ce524 = $this->env->getExtension("native_profiler");
        $__internal_1ba829efb081992d8c200d79048260bce8b593fb153a77df958298e2628ce524->enter($__internal_1ba829efb081992d8c200d79048260bce8b593fb153a77df958298e2628ce524_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppUserBundle:public:public.html.twig"));

        // line 1
        echo "<!DOCTYPE HTML>
<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"fr\" xmlns:fb=\"http://www.facebook.com/2008/fbml\">
    <head>
        <title>Minimal an Admin Panel Category Flat Bootstrap Responsive Website Template | Signin :: w3layouts</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
            <meta name=\"keywords\" content=\"Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
                  Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\" />
            <script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
            <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
            <!-- Custom Theme files -->
            <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/style.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
            <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 
                <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
                <script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
                <meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />
                <meta charset=\"UTF-8\">

                    ";
        // line 19
        $this->displayBlock('title', $context, $blocks);
        // line 20
        echo "                    ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 35
        echo "                    <style> 


                    </style>


                    </head>
                    <body>
                        <div id=\"container\" class=\"\" >
                            <header class=\"header-home\"><!--header site global-->

                                <div class=\"int\">
                                    <div id=\"logo\">

                                        <img src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/images/logo.png"), "html", null, true);
        echo "\" />

                                    </div>

                                    <ul  class=\"rslides rslides1\"  id=\"opening\"><!--NEW BLOC horaires-->
                                        <li class=\"\" style=\"display: list-item; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;\" id=\"rslides1_s0\">

                                        </li>
                                    </ul><!--FIN NEW BLOC-->
                                </div>
                                <span id=\"accessMobile\">Menu</span>
                                <div id=\"navigation\"><!--englobe les 2 nav pour responsive-->
                                    <nav id=\"mainNav\"><!--navigation principale site-->
                                        <ul class=\"int\">
                                            <li>
                                                <a href=\"#\">Acceuil</a>
                                            </li>
                                            <li>
                                                <a href=\"#\">Boutiques</a>
                                            </li>
                                            <li>
                                                <a href=\"#\">Catalogues</a>
                                            </li>

                                            ";
        // line 73
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 74
            echo "                                                <li class=\"right wish-list\">
                                                    <a rel=\"nofollow\" id=\"regZone_off\" href=\"";
            // line 75
            echo $this->env->getExtension('routing')->getPath("my_app_profile");
            echo "\">Profil</a>
                                                </li>
                                                <li class=\"right wish-list\">
                                                    <a rel=\"nofollow\" id=\"regZone_off\" href=\"";
            // line 78
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\">deconnexion</a>
                                                </li>
                                            ";
        } else {
            // line 81
            echo "                                                <li class=\"right wish-list\">
                                                    <a rel=\"nofollow\" id=\"regZone_off\" href=\"";
            // line 82
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\" >Connexion</a>
                                                </li>
                                                <li class=\"right wish-list\">
                                                    <a rel=\"nofollow\" id=\"regZone_off\" href=\"";
            // line 85
            echo $this->env->getExtension('routing')->getPath("my_app_register_client");
            echo "\">Inscription Client</a>
                                                </li>
                                                <li class=\"right wish-list\">
                                                    <a rel=\"nofollow\" id=\"regZone_off\" href=\"";
            // line 88
            echo $this->env->getExtension('routing')->getPath("my_app_register_responsable");
            echo "\">Inscription Responsable</a>
                                                </li>
                                            ";
        }
        // line 91
        echo "                                        </ul>
                                    </nav><!--navigation site-->
                                    <div id=\"topBarSite\"><!--2eme nav et reseaux sociaux-->
                                        <div class=\"int\">
                                            <ul id=\"secondaryNav\"><!--New BLOC-->

                                                <li>
                                                    <a href=\"Creteil-Soleil/Horaires-Acces.html\" >Horaires & Accès</a>
                                                </li>

                                                <li>
                                                    <a href=\"Creteil-Soleil/Plan-du-centre.html\" >Plan du centre</a>
                                                </li>

                                                <li>
                                                    <a href=\"Creteil-Soleil/Services.html\" >News</a>
                                                </li>


                                            </ul><!--New BLOC-->
                                            <ul class=\"blocSocialMedia\">
                                                <li class=\"facebook\">
                                                    <a href=\"https://www.facebook.com/groups/934963773260705/?fref=ts\" rel=\"nofollow\" target=\"_blank\">Facebook</a>
                                                    <div class=\"contentbloc\">
                                                        <a  href=\"#\">Follow @Facebook</a>
                                                    </div>
                                                </li>
                                                <li class=\"twitter\">
                                                    <a href=\"#\" rel=\"nofollow\" target=\"_blank\">Twitter</a>
                                                    <div class=\"contentbloc\">
                                                        <a href=\"#\">Follow @twitter</a>
                                                    </div>
                                                </li>
                                                <li class=\"instagram\">
                                                    <a href=\"#\" target=\"_blank\" rel=\"nofollow\" >Instagram</a>
                                                    <div class=\"contentbloc\">
                                                        <a href=\"#\">Follow @instagram</a>
                                                </li>
                                            </ul>        
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </header>
                        ";
        // line 136
        echo "

                        <div class=\"container\">
                            <div class=\"row\">
                                <div class=\"col-lg-12 col-md-4\">
                                    <div id=\"rym\">
                                        <div id=\"slider\">
                                            <input type=\"radio\" id=\"button-1\" name=\"controls\" />
                                            <input type=\"radio\" id=\"button-2\" name=\"controls\" checked />
                                            <input type=\"radio\" id=\"button-3\" name=\"controls\" />
                                            <input type=\"radio\" id=\"button-4\" name=\"controls\" />
                                            <input type=\"radio\" id=\"button-5\" name=\"controls\" />
                                            <label for=\"button-1\" class=\"arrows\" id=\"arrow-1\">&#x25BB;</label>
                                            <label for=\"button-2\" class=\"arrows\" id=\"arrow-2\">&#x25BB;</label>
                                            <label for=\"button-3\" class=\"arrows\" id=\"arrow-3\">&#x25BB;</label>
                                            <label for=\"button-4\" class=\"arrows\" id=\"arrow-4\">&#x25BB;</label>
                                            <label for=\"button-5\" class=\"arrows\" id=\"arrow-5\">&#x25BB;</label>
                                            <div id=\"slides\">
                                                <div class='tk-museo-sans'>
                                                    <span id=\"image-1\"> 
                                                        
                                                    </span>
                                                    <span id=\"image-2\"> 
                                                       
                                                    </span>
                                                    <span id=\"image-3\">
                                                        
                                                    </span>
                                                    <span id=\"image-4\"> 
                                                        
                                                    </span>
                                                    <span id=\"image-5\"> 
                                                       
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>




                        <style>
                            .login-bottom{
                                height: 280px;
                            }
                        </style>
                        <!---->
                        <div class=\"copy-right\">
                            <p> &copy; 2016 Tunisia Mall. All Rights Reserved | <a href=\"#\" target=\"_blank\">Tunisia Mall</a> </p>\t    
                        </div>  
                        <!--scrolling js-->
                        <script src=\"";
        // line 193
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.nicescroll.js"), "html", null, true);
        echo "\"></script>
                        <script src=\"";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/scripts.js"), "html", null, true);
        echo "\"></script>
                        <!--//scrolling js-->
                        <div id=\"container\" class=\"\" >
                            <footer ><!--footer-->
                                <div class=\"left\">
                                    <h2>Newsletter Tunisia Mall</h2>
                                    <p>Pour être informé sur:</p>
                                    <ul>
                                        <li>Les sorties de nouvelles collections</li>
                                        <li>Vos marques préférées</li>
                                    </ul>


                                    <form action=\"#\" id=\"newsletter_form\" method=\"post\">
                                        <fieldset>
                                            <input type=\"text\" name=\"email\" value=\"Votre email\" data-init-value=\"Votre email\" />
                                            <input type=\"hidden\" name=\"centreId\" value=\"399\"/>
                                            <input type=\"hidden\" name=\"centreName\" value=\"Créteil Soleil\"/>
                                            <input type=\"submit\" value=\"ok\" />
                                        </fieldset>
                                    </form>
                                </div>

                                <div class=\"right\">
                                    <h2>Application Mobile Tunisia Mall</h2>
                                    <p>Retrouvez votre centre Tunisia Mall sur votre smartphone et vos tablettes !</p>
                                    <a class=\"apple\" target=\"_blank\" href=\"#\"onClick=\"return xt_click(this, 'C', '75', 'Depuis_lien_footer:App_store', 'N');
                                            return false;\">Appstore</a>
                                    <a class=\"google\" target=\"_blank\" href=\"#\" onClick=\"return xt_click(this, 'C', '75', 'Depuis_lien_footer:Google_Play', 'N');
                                            return false;\"> Google play</a>
                                </div>


                                <nav>
                                    <ul>
                                        <li><a href=\"Creteil-Soleil/Nous-contacter.html\" title=\"Nous contacter\">Nous contacter</a></li>
                                        <li><a href=\"Creteil-Soleil/Mentions-Legales.html\" title=\"Mentions Légales\">Mentions Légales</a></li>
                                        <li><a href=\"Creteil-Soleil/Louer-un-emplacement-temporaire.html\" title=\"Louer un emplacement temporaire\">Visite guidee</a></li>
                                    </ul>
                                </nav>


                                <ul class=\"blocSocialMedia\">
                                    <li class=\"facebook\">
                                        <a href=\"https://www.facebook.com/groups/934963773260705/?fref=ts\" target=\"_blank\" rel=\"nofollow\">Facebook</a>
                                    </li>
                                    <li class=\"twitter\">
                                        <a href=\"#\" target=\"_blank\" rel=\"nofollow\">Twitter</a>
                                    </li>
                                    <li class=\"instagram\">
                                        <a href=\"#\" target=\"_blank\" rel=\"nofollow\" >Instagram</a>
                                    </li>
                                </ul>

                            </footer><!--Fin footer-->        
                        </div> </body>
                    </html>



";
        
        $__internal_1ba829efb081992d8c200d79048260bce8b593fb153a77df958298e2628ce524->leave($__internal_1ba829efb081992d8c200d79048260bce8b593fb153a77df958298e2628ce524_prof);

    }

    // line 19
    public function block_title($context, array $blocks = array())
    {
        $__internal_e579e7b42b7975130256b6fb0e543612d0b045504805ac6fe0c880eeeeb04dec = $this->env->getExtension("native_profiler");
        $__internal_e579e7b42b7975130256b6fb0e543612d0b045504805ac6fe0c880eeeeb04dec->enter($__internal_e579e7b42b7975130256b6fb0e543612d0b045504805ac6fe0c880eeeeb04dec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "<title>Tunisia Mall</title>";
        
        $__internal_e579e7b42b7975130256b6fb0e543612d0b045504805ac6fe0c880eeeeb04dec->leave($__internal_e579e7b42b7975130256b6fb0e543612d0b045504805ac6fe0c880eeeeb04dec_prof);

    }

    // line 20
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_301ffa6dd64d1ec158f2127f6e810aba60bfb203fb92f3b9283dfe0f85b7f25c = $this->env->getExtension("native_profiler");
        $__internal_301ffa6dd64d1ec158f2127f6e810aba60bfb203fb92f3b9283dfe0f85b7f25c->enter($__internal_301ffa6dd64d1ec158f2127f6e810aba60bfb203fb92f3b9283dfe0f85b7f25c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 21
        echo "
                        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/6c6c9e8995c7ee151b5a1c238da4a176_1453471972_all.css"), "html", null, true);
        echo "\" />
                        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/mobile.css"), "html", null, true);
        echo "\" media=\"screen and (max-width:1100px)\" />
                        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/taggage.css"), "html", null, true);
        echo "\" />
                        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/style.css"), "html", null, true);
        echo "\" />
                        <style type=\"text/css\">        
                            #mainNav >ul >li >a:hover,#accessCatalogue > li .active,#mainNav >ul >.active >a,.back .add-wishlist,.scroll-list
                            {background-color:#bb9854;}
                            .cat,#opening a,#sidebar-nav strong,.listingCatalogue li h3, .listingCatalogue li h2,.fidelityContent p a,#modal-inscription span,.globalInfos h3,.front             .club-offer strong,.back .club-offer h2, #modal-add-wishlist p,#modal-shopping-alert p,#slider h2 .first,.backlinks a
                            {color:#bb9854;}
                            li.soon {border-color:#bb9854 !important;}
                            li.soon .mask-bonplan, li.soon .accessOffer a.access-infos{background-color:#bb9854;}
                        </style>
                    ";
        
        $__internal_301ffa6dd64d1ec158f2127f6e810aba60bfb203fb92f3b9283dfe0f85b7f25c->leave($__internal_301ffa6dd64d1ec158f2127f6e810aba60bfb203fb92f3b9283dfe0f85b7f25c_prof);

    }

    public function getTemplateName()
    {
        return "MyAppUserBundle:public:public.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  355 => 25,  351 => 24,  347 => 23,  343 => 22,  340 => 21,  334 => 20,  322 => 19,  254 => 194,  250 => 193,  191 => 136,  145 => 91,  139 => 88,  133 => 85,  127 => 82,  124 => 81,  118 => 78,  112 => 75,  109 => 74,  107 => 73,  80 => 49,  64 => 35,  61 => 20,  59 => 19,  52 => 15,  48 => 14,  44 => 13,  40 => 12,  35 => 10,  24 => 1,);
    }
}
/* <!DOCTYPE HTML>*/
/* <html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xmlns:fb="http://www.facebook.com/2008/fbml">*/
/*     <head>*/
/*         <title>Minimal an Admin Panel Category Flat Bootstrap Responsive Website Template | Signin :: w3layouts</title>*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*             <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*             <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, */
/*                   Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />*/
/*             <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>*/
/*             <link href="{{asset('bundles/myappadmin/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />*/
/*             <!-- Custom Theme files -->*/
/*             <link href="{{asset('bundles/myappadmin/css/style.css')}}" rel='stylesheet' type='text/css' />*/
/*             <link href="{{asset('bundles/myappadmin/css/font-awesome.css')}}" rel="stylesheet"> */
/*                 <script src="{{asset('bundles/myappadmin/js/jquery.min.js')}}"></script>*/
/*                 <script src="{{asset('bundles/myappadmin/js/bootstrap.min.js')}}"></script>*/
/*                 <meta http-equiv="content-type" content="text/html;charset=utf-8" />*/
/*                 <meta charset="UTF-8">*/
/* */
/*                     {% block title %}<title>Tunisia Mall</title>{% endblock %}*/
/*                     {% block stylesheets %}*/
/* */
/*                         <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/6c6c9e8995c7ee151b5a1c238da4a176_1453471972_all.css') }}" />*/
/*                         <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/mobile.css') }}" media="screen and (max-width:1100px)" />*/
/*                         <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/taggage.css') }}" />*/
/*                         <link rel="stylesheet" type="text/css" href="{{asset('bundles/myappuser/css/style.css')}}" />*/
/*                         <style type="text/css">        */
/*                             #mainNav >ul >li >a:hover,#accessCatalogue > li .active,#mainNav >ul >.active >a,.back .add-wishlist,.scroll-list*/
/*                             {background-color:#bb9854;}*/
/*                             .cat,#opening a,#sidebar-nav strong,.listingCatalogue li h3, .listingCatalogue li h2,.fidelityContent p a,#modal-inscription span,.globalInfos h3,.front             .club-offer strong,.back .club-offer h2, #modal-add-wishlist p,#modal-shopping-alert p,#slider h2 .first,.backlinks a*/
/*                             {color:#bb9854;}*/
/*                             li.soon {border-color:#bb9854 !important;}*/
/*                             li.soon .mask-bonplan, li.soon .accessOffer a.access-infos{background-color:#bb9854;}*/
/*                         </style>*/
/*                     {% endblock %}*/
/*                     <style> */
/* */
/* */
/*                     </style>*/
/* */
/* */
/*                     </head>*/
/*                     <body>*/
/*                         <div id="container" class="" >*/
/*                             <header class="header-home"><!--header site global-->*/
/* */
/*                                 <div class="int">*/
/*                                     <div id="logo">*/
/* */
/*                                         <img src="{{ asset('bundles/myappuser/images/logo.png') }}" />*/
/* */
/*                                     </div>*/
/* */
/*                                     <ul  class="rslides rslides1"  id="opening"><!--NEW BLOC horaires-->*/
/*                                         <li class="" style="display: list-item; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;" id="rslides1_s0">*/
/* */
/*                                         </li>*/
/*                                     </ul><!--FIN NEW BLOC-->*/
/*                                 </div>*/
/*                                 <span id="accessMobile">Menu</span>*/
/*                                 <div id="navigation"><!--englobe les 2 nav pour responsive-->*/
/*                                     <nav id="mainNav"><!--navigation principale site-->*/
/*                                         <ul class="int">*/
/*                                             <li>*/
/*                                                 <a href="#">Acceuil</a>*/
/*                                             </li>*/
/*                                             <li>*/
/*                                                 <a href="#">Boutiques</a>*/
/*                                             </li>*/
/*                                             <li>*/
/*                                                 <a href="#">Catalogues</a>*/
/*                                             </li>*/
/* */
/*                                             {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*                                                 <li class="right wish-list">*/
/*                                                     <a rel="nofollow" id="regZone_off" href="{{ path('my_app_profile') }}">Profil</a>*/
/*                                                 </li>*/
/*                                                 <li class="right wish-list">*/
/*                                                     <a rel="nofollow" id="regZone_off" href="{{ path('fos_user_security_logout') }}">deconnexion</a>*/
/*                                                 </li>*/
/*                                             {% else %}*/
/*                                                 <li class="right wish-list">*/
/*                                                     <a rel="nofollow" id="regZone_off" href="{{ path('fos_user_security_login') }}" >Connexion</a>*/
/*                                                 </li>*/
/*                                                 <li class="right wish-list">*/
/*                                                     <a rel="nofollow" id="regZone_off" href="{{ path('my_app_register_client') }}">Inscription Client</a>*/
/*                                                 </li>*/
/*                                                 <li class="right wish-list">*/
/*                                                     <a rel="nofollow" id="regZone_off" href="{{ path('my_app_register_responsable') }}">Inscription Responsable</a>*/
/*                                                 </li>*/
/*                                             {% endif %}*/
/*                                         </ul>*/
/*                                     </nav><!--navigation site-->*/
/*                                     <div id="topBarSite"><!--2eme nav et reseaux sociaux-->*/
/*                                         <div class="int">*/
/*                                             <ul id="secondaryNav"><!--New BLOC-->*/
/* */
/*                                                 <li>*/
/*                                                     <a href="Creteil-Soleil/Horaires-Acces.html" >Horaires & Accès</a>*/
/*                                                 </li>*/
/* */
/*                                                 <li>*/
/*                                                     <a href="Creteil-Soleil/Plan-du-centre.html" >Plan du centre</a>*/
/*                                                 </li>*/
/* */
/*                                                 <li>*/
/*                                                     <a href="Creteil-Soleil/Services.html" >News</a>*/
/*                                                 </li>*/
/* */
/* */
/*                                             </ul><!--New BLOC-->*/
/*                                             <ul class="blocSocialMedia">*/
/*                                                 <li class="facebook">*/
/*                                                     <a href="https://www.facebook.com/groups/934963773260705/?fref=ts" rel="nofollow" target="_blank">Facebook</a>*/
/*                                                     <div class="contentbloc">*/
/*                                                         <a  href="#">Follow @Facebook</a>*/
/*                                                     </div>*/
/*                                                 </li>*/
/*                                                 <li class="twitter">*/
/*                                                     <a href="#" rel="nofollow" target="_blank">Twitter</a>*/
/*                                                     <div class="contentbloc">*/
/*                                                         <a href="#">Follow @twitter</a>*/
/*                                                     </div>*/
/*                                                 </li>*/
/*                                                 <li class="instagram">*/
/*                                                     <a href="#" target="_blank" rel="nofollow" >Instagram</a>*/
/*                                                     <div class="contentbloc">*/
/*                                                         <a href="#">Follow @instagram</a>*/
/*                                                 </li>*/
/*                                             </ul>        */
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                         </div>*/
/*                         </header>*/
/*                         {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* */
/*                         <div class="container">*/
/*                             <div class="row">*/
/*                                 <div class="col-lg-12 col-md-4">*/
/*                                     <div id="rym">*/
/*                                         <div id="slider">*/
/*                                             <input type="radio" id="button-1" name="controls" />*/
/*                                             <input type="radio" id="button-2" name="controls" checked />*/
/*                                             <input type="radio" id="button-3" name="controls" />*/
/*                                             <input type="radio" id="button-4" name="controls" />*/
/*                                             <input type="radio" id="button-5" name="controls" />*/
/*                                             <label for="button-1" class="arrows" id="arrow-1">&#x25BB;</label>*/
/*                                             <label for="button-2" class="arrows" id="arrow-2">&#x25BB;</label>*/
/*                                             <label for="button-3" class="arrows" id="arrow-3">&#x25BB;</label>*/
/*                                             <label for="button-4" class="arrows" id="arrow-4">&#x25BB;</label>*/
/*                                             <label for="button-5" class="arrows" id="arrow-5">&#x25BB;</label>*/
/*                                             <div id="slides">*/
/*                                                 <div class='tk-museo-sans'>*/
/*                                                     <span id="image-1"> */
/*                                                         */
/*                                                     </span>*/
/*                                                     <span id="image-2"> */
/*                                                        */
/*                                                     </span>*/
/*                                                     <span id="image-3">*/
/*                                                         */
/*                                                     </span>*/
/*                                                     <span id="image-4"> */
/*                                                         */
/*                                                     </span>*/
/*                                                     <span id="image-5"> */
/*                                                        */
/*                                                     </span>*/
/*                                                 </div>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/* */
/*                             </div>*/
/* */
/*                         </div>*/
/* */
/* */
/* */
/* */
/*                         <style>*/
/*                             .login-bottom{*/
/*                                 height: 280px;*/
/*                             }*/
/*                         </style>*/
/*                         <!---->*/
/*                         <div class="copy-right">*/
/*                             <p> &copy; 2016 Tunisia Mall. All Rights Reserved | <a href="#" target="_blank">Tunisia Mall</a> </p>	    */
/*                         </div>  */
/*                         <!--scrolling js-->*/
/*                         <script src="{{asset('bundles/myappadmin/js/jquery.nicescroll.js')}}"></script>*/
/*                         <script src="{{asset('bundles/myappadmin/js/scripts.js')}}"></script>*/
/*                         <!--//scrolling js-->*/
/*                         <div id="container" class="" >*/
/*                             <footer ><!--footer-->*/
/*                                 <div class="left">*/
/*                                     <h2>Newsletter Tunisia Mall</h2>*/
/*                                     <p>Pour être informé sur:</p>*/
/*                                     <ul>*/
/*                                         <li>Les sorties de nouvelles collections</li>*/
/*                                         <li>Vos marques préférées</li>*/
/*                                     </ul>*/
/* */
/* */
/*                                     <form action="#" id="newsletter_form" method="post">*/
/*                                         <fieldset>*/
/*                                             <input type="text" name="email" value="Votre email" data-init-value="Votre email" />*/
/*                                             <input type="hidden" name="centreId" value="399"/>*/
/*                                             <input type="hidden" name="centreName" value="Créteil Soleil"/>*/
/*                                             <input type="submit" value="ok" />*/
/*                                         </fieldset>*/
/*                                     </form>*/
/*                                 </div>*/
/* */
/*                                 <div class="right">*/
/*                                     <h2>Application Mobile Tunisia Mall</h2>*/
/*                                     <p>Retrouvez votre centre Tunisia Mall sur votre smartphone et vos tablettes !</p>*/
/*                                     <a class="apple" target="_blank" href="#"onClick="return xt_click(this, 'C', '75', 'Depuis_lien_footer:App_store', 'N');*/
/*                                             return false;">Appstore</a>*/
/*                                     <a class="google" target="_blank" href="#" onClick="return xt_click(this, 'C', '75', 'Depuis_lien_footer:Google_Play', 'N');*/
/*                                             return false;"> Google play</a>*/
/*                                 </div>*/
/* */
/* */
/*                                 <nav>*/
/*                                     <ul>*/
/*                                         <li><a href="Creteil-Soleil/Nous-contacter.html" title="Nous contacter">Nous contacter</a></li>*/
/*                                         <li><a href="Creteil-Soleil/Mentions-Legales.html" title="Mentions Légales">Mentions Légales</a></li>*/
/*                                         <li><a href="Creteil-Soleil/Louer-un-emplacement-temporaire.html" title="Louer un emplacement temporaire">Visite guidee</a></li>*/
/*                                     </ul>*/
/*                                 </nav>*/
/* */
/* */
/*                                 <ul class="blocSocialMedia">*/
/*                                     <li class="facebook">*/
/*                                         <a href="https://www.facebook.com/groups/934963773260705/?fref=ts" target="_blank" rel="nofollow">Facebook</a>*/
/*                                     </li>*/
/*                                     <li class="twitter">*/
/*                                         <a href="#" target="_blank" rel="nofollow">Twitter</a>*/
/*                                     </li>*/
/*                                     <li class="instagram">*/
/*                                         <a href="#" target="_blank" rel="nofollow" >Instagram</a>*/
/*                                     </li>*/
/*                                 </ul>*/
/* */
/*                             </footer><!--Fin footer-->        */
/*                         </div> </body>*/
/*                     </html>*/
/* */
/* */
/* */
/* */
