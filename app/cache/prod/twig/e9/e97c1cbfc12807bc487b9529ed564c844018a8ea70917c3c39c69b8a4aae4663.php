<?php

/* MyAppAdminBundle::layout.html.twig */
class __TwigTemplate_53c79bb2cab9d53270fdc5fb045da31ee969570d36715c7e5e29698b03eefd9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e763a8ecf688af88c26ad047fc0081d13682ac15ee53b65e800d7c59e08e1bf6 = $this->env->getExtension("native_profiler");
        $__internal_e763a8ecf688af88c26ad047fc0081d13682ac15ee53b65e800d7c59e08e1bf6->enter($__internal_e763a8ecf688af88c26ad047fc0081d13682ac15ee53b65e800d7c59e08e1bf6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppAdminBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE HTML>
<html>
    <head>
        <title>Tunisia Mall</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <meta name=\"keywords\" content=\"Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\" />
        <script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/style.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
        <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 
        <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Mainly scripts -->
        <script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.metisMenu.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.slimscroll.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Custom and plugin javascript -->
        <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/custom.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/screenfull.js"), "html", null, true);
        echo "\"></script>
        <script>
            \$(function() {
                \$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

                if (!screenfull.enabled) {
                    return false;
                }



                \$('#toggle').click(function() {
                    screenfull.toggle(\$('#container')[0]);
                });

            });
        </script>

        <!----->

        <!--pie-chart--->
        <script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/pie-chart.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <script type=\"text/javascript\">

            \$(document).ready(function() {
                \$('#demo-pie-1').pieChart({
                    barColor: '#3bb2d0',
                    trackColor: '#eee',
                    lineCap: 'round',
                    lineWidth: 8,
                    onStep: function(from, to, percent) {
                        \$(this.element).find('.pie-value').text(Math.round(percent) + '%');
                    }
                });

                \$('#demo-pie-2').pieChart({
                    barColor: '#fbb03b',
                    trackColor: '#eee',
                    lineCap: 'butt',
                    lineWidth: 8,
                    onStep: function(from, to, percent) {
                        \$(this.element).find('.pie-value').text(Math.round(percent) + '%');
                    }
                });

                \$('#demo-pie-3').pieChart({
                    barColor: '#ed6498',
                    trackColor: '#eee',
                    lineCap: 'square',
                    lineWidth: 8,
                    onStep: function(from, to, percent) {
                        \$(this.element).find('.pie-value').text(Math.round(percent) + '%');
                    }
                });


            });

        </script>
        <!--skycons-icons-->
        <script src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/skycons.js"), "html", null, true);
        echo "\"></script>
        <!--//skycons-icons-->
    </head>
    <body>

        <div id=\"wrapper\">

            <!----->
            <nav class=\"navbar-default navbar-static-top\" role=\"navigation\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <h1> <a class=\"navbar-brand\" href=\"#\">Tunisia Mall</a></h1>         
                </div>
                <div class=\" border-bottom\">
                    <div class=\"full-left\">
                        <section class=\"full-top\">
                            <button id=\"toggle\"><i class=\"fa fa-arrows-alt\"></i></button>\t
                        </section>
                        <div class=\"clearfix\"> </div>
                    </div>


                    <!-- Brand and toggle get grouped for better mobile display -->

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class=\"drop-men\" >
                        <ul class=\" nav_1\">
                            <a href=\"#\" class=\"dropdown-toggle dropdown-at\" data-toggle=\"dropdown\"><span class=\" name-caret\">Admin</span><img src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/images/wo.jpg"), "html", null, true);
        echo "\"></a>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                    <div class=\"clearfix\">

                    </div>

                    <div class=\"navbar-default sidebar\" role=\"navigation\">
                        <div class=\"sidebar-nav navbar-collapse\">
                            <ul class=\"nav\" id=\"side-menu\">

                                <li>
                                    <a href=\"";
        // line 125
        echo $this->env->getExtension('routing')->getPath("my_app_admin_homepage");
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-dashboard nav_icon \"></i><span class=\"nav-label\">Dashboards</span> </a>
                                </li>

                                <li>
                                    <a href=\"";
        // line 129
        echo $this->env->getExtension('routing')->getPath("my_app_admin_clients");
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-indent nav_icon\"></i> <span class=\"nav-label\">Clients</span></a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 132
        echo $this->env->getExtension('routing')->getPath("my_app_admin_responsables");
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-indent nav_icon\"></i> <span class=\"nav-label\">Responsable d'enseigne</span> </a>
                                </li>
                            </ul>
                        </div>
                    </div>

            </nav>
            <div id=\"page-wrapper\" class=\"gray-bg dashbard-1\">
                <div class=\"content-main\">

                    <!--banner-->\t
                    <div class=\"banner\">

                        <h2>
                            <a href=\"#\">Home</a>
                            <i class=\"fa fa-angle-right\"></i>
                            <span>Dashboard</span>
                        </h2>
                    </div>
                    <!--//banner-->
                     <div class=\"blank\">
                         <div class=\"blank-page\">
                    ";
        // line 154
        $this->displayBlock('container', $context, $blocks);
        // line 158
        echo "                         </div>
                     </div>
                         <div class=\"copy\">
                        <p> &copy; 2016 Tunisia Mall. All Rights Reserved | <a href=\"#\" target=\"_blank\">Tunisa Mall</a> </p>\t    </div>
                    <!----Calender -------->
                    <link rel=\"stylesheet\" href=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/clndr.css"), "html", null, true);
        echo "\" type=\"text/css\" />
                    <script src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/underscore-min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
                    <script src= \"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/moment-2.2.1.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
                    <script src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/clndr.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
                    <script src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/site.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
                    <!----End Calender -------->
                </div>

                <div class=\"clearfix\"> </div>
            </div>


        </div>
        <div class=\"clearfix\"> </div>
    </div>
</div>
<!---->
<!--scrolling js-->
<script src=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.nicescroll.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/scripts.js"), "html", null, true);
        echo "\"></script>
<!--//scrolling js-->
<script src=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

</body>
</html>

";
        
        $__internal_e763a8ecf688af88c26ad047fc0081d13682ac15ee53b65e800d7c59e08e1bf6->leave($__internal_e763a8ecf688af88c26ad047fc0081d13682ac15ee53b65e800d7c59e08e1bf6_prof);

    }

    // line 154
    public function block_container($context, array $blocks = array())
    {
        $__internal_d0fd688e8716c50ed0dc9737a4a72aafdaf658e6991242addcfe4923779d05c2 = $this->env->getExtension("native_profiler");
        $__internal_d0fd688e8716c50ed0dc9737a4a72aafdaf658e6991242addcfe4923779d05c2->enter($__internal_d0fd688e8716c50ed0dc9737a4a72aafdaf658e6991242addcfe4923779d05c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        echo " 


                    ";
        
        $__internal_d0fd688e8716c50ed0dc9737a4a72aafdaf658e6991242addcfe4923779d05c2->leave($__internal_d0fd688e8716c50ed0dc9737a4a72aafdaf658e6991242addcfe4923779d05c2_prof);

    }

    public function getTemplateName()
    {
        return "MyAppAdminBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  287 => 154,  274 => 184,  269 => 182,  265 => 181,  248 => 167,  244 => 166,  240 => 165,  236 => 164,  232 => 163,  225 => 158,  223 => 154,  198 => 132,  192 => 129,  185 => 125,  170 => 113,  135 => 81,  93 => 42,  69 => 21,  65 => 20,  61 => 19,  56 => 17,  52 => 16,  47 => 14,  43 => 13,  39 => 12,  34 => 10,  23 => 1,);
    }
}
/* <!DOCTYPE HTML>*/
/* <html>*/
/*     <head>*/
/*         <title>Tunisia Mall</title>*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*         <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, */
/*               Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />*/
/*         <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>*/
/*         <link href="{{asset('bundles/myappadmin/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />*/
/*         <!-- Custom Theme files -->*/
/*         <link href="{{asset('bundles/myappadmin/css/style.css')}}" rel='stylesheet' type='text/css' />*/
/*         <link href="{{asset('bundles/myappadmin/css/font-awesome.css')}}" rel="stylesheet"> */
/*         <script src="{{asset('bundles/myappadmin/js/jquery.min.js')}}"></script>*/
/*         <!-- Mainly scripts -->*/
/*         <script src="{{asset('bundles/myappadmin/js/jquery.metisMenu.js')}}"></script>*/
/*         <script src="{{asset('bundles/myappadmin/js/jquery.slimscroll.min.js')}}"></script>*/
/*         <!-- Custom and plugin javascript -->*/
/*         <link href="{{asset('bundles/myappadmin/css/custom.css')}}" rel="stylesheet">*/
/*         <script src="{{asset('bundles/myappadmin/js/custom.js')}}"></script>*/
/*         <script src="{{asset('bundles/myappadmin/js/screenfull.js')}}"></script>*/
/*         <script>*/
/*             $(function() {*/
/*                 $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);*/
/* */
/*                 if (!screenfull.enabled) {*/
/*                     return false;*/
/*                 }*/
/* */
/* */
/* */
/*                 $('#toggle').click(function() {*/
/*                     screenfull.toggle($('#container')[0]);*/
/*                 });*/
/* */
/*             });*/
/*         </script>*/
/* */
/*         <!----->*/
/* */
/*         <!--pie-chart--->*/
/*         <script src="{{asset('bundles/myappadmin/js/pie-chart.js')}}" type="text/javascript"></script>*/
/*         <script type="text/javascript">*/
/* */
/*             $(document).ready(function() {*/
/*                 $('#demo-pie-1').pieChart({*/
/*                     barColor: '#3bb2d0',*/
/*                     trackColor: '#eee',*/
/*                     lineCap: 'round',*/
/*                     lineWidth: 8,*/
/*                     onStep: function(from, to, percent) {*/
/*                         $(this.element).find('.pie-value').text(Math.round(percent) + '%');*/
/*                     }*/
/*                 });*/
/* */
/*                 $('#demo-pie-2').pieChart({*/
/*                     barColor: '#fbb03b',*/
/*                     trackColor: '#eee',*/
/*                     lineCap: 'butt',*/
/*                     lineWidth: 8,*/
/*                     onStep: function(from, to, percent) {*/
/*                         $(this.element).find('.pie-value').text(Math.round(percent) + '%');*/
/*                     }*/
/*                 });*/
/* */
/*                 $('#demo-pie-3').pieChart({*/
/*                     barColor: '#ed6498',*/
/*                     trackColor: '#eee',*/
/*                     lineCap: 'square',*/
/*                     lineWidth: 8,*/
/*                     onStep: function(from, to, percent) {*/
/*                         $(this.element).find('.pie-value').text(Math.round(percent) + '%');*/
/*                     }*/
/*                 });*/
/* */
/* */
/*             });*/
/* */
/*         </script>*/
/*         <!--skycons-icons-->*/
/*         <script src="{{asset('bundles/myappadmin/js/skycons.js')}}"></script>*/
/*         <!--//skycons-icons-->*/
/*     </head>*/
/*     <body>*/
/* */
/*         <div id="wrapper">*/
/* */
/*             <!----->*/
/*             <nav class="navbar-default navbar-static-top" role="navigation">*/
/*                 <div class="navbar-header">*/
/*                     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                         <span class="sr-only">Toggle navigation</span>*/
/*                         <span class="icon-bar"></span>*/
/*                         <span class="icon-bar"></span>*/
/*                         <span class="icon-bar"></span>*/
/*                     </button>*/
/*                     <h1> <a class="navbar-brand" href="#">Tunisia Mall</a></h1>         */
/*                 </div>*/
/*                 <div class=" border-bottom">*/
/*                     <div class="full-left">*/
/*                         <section class="full-top">*/
/*                             <button id="toggle"><i class="fa fa-arrows-alt"></i></button>	*/
/*                         </section>*/
/*                         <div class="clearfix"> </div>*/
/*                     </div>*/
/* */
/* */
/*                     <!-- Brand and toggle get grouped for better mobile display -->*/
/* */
/*                     <!-- Collect the nav links, forms, and other content for toggling -->*/
/*                     <div class="drop-men" >*/
/*                         <ul class=" nav_1">*/
/*                             <a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown"><span class=" name-caret">Admin</span><img src="{{asset('bundles/myappadmin/images/wo.jpg')}}"></a>*/
/*                         </ul>*/
/*                     </div><!-- /.navbar-collapse -->*/
/*                     <div class="clearfix">*/
/* */
/*                     </div>*/
/* */
/*                     <div class="navbar-default sidebar" role="navigation">*/
/*                         <div class="sidebar-nav navbar-collapse">*/
/*                             <ul class="nav" id="side-menu">*/
/* */
/*                                 <li>*/
/*                                     <a href="{{path('my_app_admin_homepage')}}" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Dashboards</span> </a>*/
/*                                 </li>*/
/* */
/*                                 <li>*/
/*                                     <a href="{{path('my_app_admin_clients')}}" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Clients</span></a>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="{{path('my_app_admin_responsables')}}" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Responsable d'enseigne</span> </a>*/
/*                                 </li>*/
/*                             </ul>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*             </nav>*/
/*             <div id="page-wrapper" class="gray-bg dashbard-1">*/
/*                 <div class="content-main">*/
/* */
/*                     <!--banner-->	*/
/*                     <div class="banner">*/
/* */
/*                         <h2>*/
/*                             <a href="#">Home</a>*/
/*                             <i class="fa fa-angle-right"></i>*/
/*                             <span>Dashboard</span>*/
/*                         </h2>*/
/*                     </div>*/
/*                     <!--//banner-->*/
/*                      <div class="blank">*/
/*                          <div class="blank-page">*/
/*                     {% block  container %} */
/* */
/* */
/*                     {% endblock container %}*/
/*                          </div>*/
/*                      </div>*/
/*                          <div class="copy">*/
/*                         <p> &copy; 2016 Tunisia Mall. All Rights Reserved | <a href="#" target="_blank">Tunisa Mall</a> </p>	    </div>*/
/*                     <!----Calender -------->*/
/*                     <link rel="stylesheet" href="{{asset('bundles/myappadmin/css/clndr.css')}}" type="text/css" />*/
/*                     <script src="{{asset('bundles/myappadmin/js/underscore-min.js')}}" type="text/javascript"></script>*/
/*                     <script src= "{{asset('bundles/myappadmin/js/moment-2.2.1.js')}}" type="text/javascript"></script>*/
/*                     <script src="{{asset('bundles/myappadmin/js/clndr.js')}}" type="text/javascript"></script>*/
/*                     <script src="{{asset('bundles/myappadmin/js/site.js')}}" type="text/javascript"></script>*/
/*                     <!----End Calender -------->*/
/*                 </div>*/
/* */
/*                 <div class="clearfix"> </div>*/
/*             </div>*/
/* */
/* */
/*         </div>*/
/*         <div class="clearfix"> </div>*/
/*     </div>*/
/* </div>*/
/* <!---->*/
/* <!--scrolling js-->*/
/* <script src="{{asset('bundles/myappadmin/js/jquery.nicescroll.js')}}"></script>*/
/* <script src="{{asset('bundles/myappadmin/js/scripts.js')}}"></script>*/
/* <!--//scrolling js-->*/
/* <script src="{{asset('bundles/myappadmin/js/bootstrap.min.js')}}"></script>*/
/* */
/* </body>*/
/* </html>*/
/* */
/* */
