<?php

/* MyAppAdminBundle:Admin:statistique.html.twig */
class __TwigTemplate_3662e847b0cd5abc617e9582b854c00033ce157ef2ccebb9f5b6fa69cd154833 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MyAppAdminBundle::layout.html.twig", "MyAppAdminBundle:Admin:statistique.html.twig", 1);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MyAppAdminBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_06dcb676b6425253d521e1703ebe7e822fe180aa18c54a3f6c33ca5f25b65db5 = $this->env->getExtension("native_profiler");
        $__internal_06dcb676b6425253d521e1703ebe7e822fe180aa18c54a3f6c33ca5f25b65db5->enter($__internal_06dcb676b6425253d521e1703ebe7e822fe180aa18c54a3f6c33ca5f25b65db5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppAdminBundle:Admin:statistique.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_06dcb676b6425253d521e1703ebe7e822fe180aa18c54a3f6c33ca5f25b65db5->leave($__internal_06dcb676b6425253d521e1703ebe7e822fe180aa18c54a3f6c33ca5f25b65db5_prof);

    }

    // line 2
    public function block_container($context, array $blocks = array())
    {
        $__internal_b7fe6738ee5a4646c5e4da7d484b005d90a8b31dd6455252d74e1ed3d21fc041 = $this->env->getExtension("native_profiler");
        $__internal_b7fe6738ee5a4646c5e4da7d484b005d90a8b31dd6455252d74e1ed3d21fc041->enter($__internal_b7fe6738ee5a4646c5e4da7d484b005d90a8b31dd6455252d74e1ed3d21fc041_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 3
        echo "    statistique
";
        
        $__internal_b7fe6738ee5a4646c5e4da7d484b005d90a8b31dd6455252d74e1ed3d21fc041->leave($__internal_b7fe6738ee5a4646c5e4da7d484b005d90a8b31dd6455252d74e1ed3d21fc041_prof);

    }

    public function getTemplateName()
    {
        return "MyAppAdminBundle:Admin:statistique.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "MyAppAdminBundle::layout.html.twig" %}*/
/* {% block container %}*/
/*     statistique*/
/* {% endblock container %}*/
