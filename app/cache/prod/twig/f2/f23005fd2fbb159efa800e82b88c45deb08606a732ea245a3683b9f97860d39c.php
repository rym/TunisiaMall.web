<?php

/* FOSUserBundle::layout.html.twig */
class __TwigTemplate_59d42ed48a03da83ab013b9bee6bb9c526a84df13b3c571219a6ae76bdd0d65b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_666c7ed93bb07a0c501cf6ab45d4f91c35edcff5eeee4940c65a2d61d9a5a251 = $this->env->getExtension("native_profiler");
        $__internal_666c7ed93bb07a0c501cf6ab45d4f91c35edcff5eeee4940c65a2d61d9a5a251->enter($__internal_666c7ed93bb07a0c501cf6ab45d4f91c35edcff5eeee4940c65a2d61d9a5a251_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE HTML>
<html>
    <head>
        <title>Tunisia Mall</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <meta name=\"keywords\" content=\"Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\" />
        <script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/style.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
        <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 
        <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Mainly scripts -->
        <script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.metisMenu.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.slimscroll.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Custom and plugin javascript -->
        <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
        <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/custom.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/screenfull.js"), "html", null, true);
        echo "\"></script>
        <script>
\$(function() {
\$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

if (!screenfull.enabled) {
return false;
}



\$('#toggle').click(function() {
screenfull.toggle(\$('#container')[0]);
});

});
        </script>

        <!----->

        <!--pie-chart--->
        <script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/pie-chart.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <script type=\"text/javascript\">

            \$(document).ready(function() {
                \$('#demo-pie-1').pieChart({
                    barColor: '#3bb2d0',
                    trackColor: '#eee',
                    lineCap: 'round',
                    lineWidth: 8,
                    onStep: function(from, to, percent) {
                        \$(this.element).find('.pie-value').text(Math.round(percent) + '%');
                    }
                });

                \$('#demo-pie-2').pieChart({
                    barColor: '#fbb03b',
                    trackColor: '#eee',
                    lineCap: 'butt',
                    lineWidth: 8,
                    onStep: function(from, to, percent) {
                        \$(this.element).find('.pie-value').text(Math.round(percent) + '%');
                    }
                });

                \$('#demo-pie-3').pieChart({
                    barColor: '#ed6498',
                    trackColor: '#eee',
                    lineCap: 'square',
                    lineWidth: 8,
                    onStep: function(from, to, percent) {
                        \$(this.element).find('.pie-value').text(Math.round(percent) + '%');
                    }
                });


            });

        </script>
        <!--skycons-icons-->
        <script src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/skycons.js"), "html", null, true);
        echo "\"></script>
        <!--//skycons-icons-->
    </head>
    <body>

        <div id=\"wrapper\">

            <!----->
            <nav class=\"navbar-default navbar-static-top\" role=\"navigation\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <h1> <a class=\"navbar-brand\" href=\"#\">Minimal</a></h1>         
                </div>
                <div class=\" border-bottom\">
                    <div class=\"full-left\">
                        <section class=\"full-top\">
                            <button id=\"toggle\"><i class=\"fa fa-arrows-alt\"></i></button>\t
                        </section>
                        <form class=\" navbar-left-right\">
                            <input type=\"text\"  value=\"Search...\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {
                          this.value = 'Search...';
                      }\">
                            <input type=\"submit\" value=\"\" class=\"fa fa-search\">
                        </form>
                        <div class=\"clearfix\"> </div>
                    </div>


                    <!-- Brand and toggle get grouped for better mobile display -->

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class=\"drop-men\" >
                        <ul class=\" nav_1\">

                            <li class=\"dropdown at-drop\">
                                <a href=\"#\" class=\"dropdown-toggle dropdown-at \" data-toggle=\"dropdown\"><i class=\"fa fa-globe\"></i> <span class=\"number\">5</span></a>
                                <ul class=\"dropdown-menu menu1 \" role=\"menu\">
                                    <li><a href=\"#\">

                                            <div class=\"user-new\">
                                                <p>New user registered</p>
                                                <span>40 seconds ago</span>
                                            </div>
                                            <div class=\"user-new-left\">

                                                <i class=\"fa fa-user-plus\"></i>
                                            </div>
                                            <div class=\"clearfix\"> </div>
                                        </a></li>
                                    <li><a href=\"#\">
                                            <div class=\"user-new\">
                                                <p>Someone special liked this</p>
                                                <span>3 minutes ago</span>
                                            </div>
                                            <div class=\"user-new-left\">

                                                <i class=\"fa fa-heart\"></i>
                                            </div>
                                            <div class=\"clearfix\"> </div>
                                        </a></li>
                                    <li><a href=\"#\">
                                            <div class=\"user-new\">
                                                <p>John cancelled the event</p>
                                                <span>4 hours ago</span>
                                            </div>
                                            <div class=\"user-new-left\">

                                                <i class=\"fa fa-times\"></i>
                                            </div>
                                            <div class=\"clearfix\"> </div>
                                        </a></li>
                                    <li><a href=\"#\">
                                            <div class=\"user-new\">
                                                <p>The server is status is stable</p>
                                                <span>yesterday at 08:30am</span>
                                            </div>
                                            <div class=\"user-new-left\">

                                                <i class=\"fa fa-info\"></i>
                                            </div>
                                            <div class=\"clearfix\"> </div>
                                        </a></li>
                                    <li><a href=\"#\">
                                            <div class=\"user-new\">
                                                <p>New comments waiting approval</p>
                                                <span>Last Week</span>
                                            </div>
                                            <div class=\"user-new-left\">

                                                <i class=\"fa fa-rss\"></i>
                                            </div>
                                            <div class=\"clearfix\"> </div>
                                        </a></li>
                                    <li><a href=\"#\" class=\"view\">View all messages</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle dropdown-at\" data-toggle=\"dropdown\"><span class=\" name-caret\">Rackham<i class=\"caret\"></i></span><img src=\"images/wo.jpg\"></a>
                                <ul class=\"dropdown-menu \" role=\"menu\">
                                    <li><a href=\"";
        // line 185
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("profile.html"), "html", null, true);
        echo "\"><i class=\"fa fa-user\"></i>Edit Profile</a></li>
                                    <li><a href=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("inbox.html"), "html", null, true);
        echo "\"><i class=\"fa fa-envelope\"></i>Inbox</a></li>
                                    <li><a href=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("calendar.html"), "html", null, true);
        echo "\"><i class=\"fa fa-calendar\"></i>Calender</a></li>
                                    <li><a href=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("inbox.html"), "html", null, true);
        echo "\"><i class=\"fa fa-clipboard\"></i>Tasks</a></li>
                                </ul>
                            </li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                    <div class=\"clearfix\">

                    </div>

                    <div class=\"navbar-default sidebar\" role=\"navigation\">
                        <div class=\"sidebar-nav navbar-collapse\">
                            <ul class=\"nav\" id=\"side-menu\">

                                <li>
                                    <a href=\"";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("index.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-dashboard nav_icon \"></i><span class=\"nav-label\">Dashboards</span> </a>
                                </li>

                                <li>
                                    <a href=\"#\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-indent nav_icon\"></i> <span class=\"nav-label\">Menu Levels</span><span class=\"fa arrow\"></span></a>
                                    <ul class=\"nav nav-second-level\">
                                        <li><a href=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("graphs.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"> <i class=\"fa fa-area-chart nav_icon\"></i>Graphs</a></li>

                                        <li><a href=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("maps.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-map-marker nav_icon\"></i>Maps</a></li>

                                        <li><a href=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("typography.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-file-text-o nav_icon\"></i>Typography</a></li>

                                    </ul>
                                </li>
                                <li>
                                    <a href=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("inbox.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-inbox nav_icon\"></i> <span class=\"nav-label\">Inbox</span> </a>
                                </li>

                                <li>
                                    <a href=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("gallery.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-picture-o nav_icon\"></i> <span class=\"nav-label\">Gallery</span> </a>
                                </li>
                                <li>
                                    <a href=\"#\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-desktop nav_icon\"></i> <span class=\"nav-label\">Pages</span><span class=\"fa arrow\"></span></a>
                                    <ul class=\"nav nav-second-level\">
                                        <li><a href=\"";
        // line 227
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("404.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"> <i class=\"fa fa-info-circle nav_icon\"></i>Error 404</a></li>
                                        <li><a href=\"";
        // line 228
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("faq.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-question-circle nav_icon\"></i>FAQ</a></li>
                                        <li><a href=\"";
        // line 229
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("blank.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-file-o nav_icon\"></i>Blank</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href=\"";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("layout.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-th nav_icon\"></i> <span class=\"nav-label\">Grid Layouts</span> </a>
                                </li>

                                <li>
                                    <a href=\"#\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-list nav_icon\"></i> <span class=\"nav-label\">Forms</span><span class=\"fa arrow\"></span></a>
                                    <ul class=\"nav nav-second-level\">
                                        <li><a href=\"";
        // line 239
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("forms.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-align-left nav_icon\"></i>Basic forms</a></li>
                                        <li><a href=\"";
        // line 240
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("validation.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-check-square-o nav_icon\"></i>Validation</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href=\"#\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-cog nav_icon\"></i> <span class=\"nav-label\">Settings</span><span class=\"fa arrow\"></span></a>
                                    <ul class=\"nav nav-second-level\">
                                        <li><a href=\"";
        // line 247
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("signin.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-sign-in nav_icon\"></i>Signin</a></li>
                                        <li><a href=\"";
        // line 248
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("signup.html"), "html", null, true);
        echo "\" class=\" hvr-bounce-to-right\"><i class=\"fa fa-sign-in nav_icon\"></i>Singup</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>

            </nav>
            <div id=\"page-wrapper\" class=\"gray-bg dashbard-1\">
                <div class=\"content-main\">

                    <!--banner-->\t
                    <div class=\"banner\">

                        <h2>
                            <a href=\"#\">Home</a>
                            <i class=\"fa fa-angle-right\"></i>
                            <span>Dashboard</span>
                        </h2>
                    </div>
                    <!--//banner-->
                    ";
        // line 269
        $this->displayBlock('container', $context, $blocks);
        // line 273
        echo "                    <!----Calender -------->
                    <link rel=\"stylesheet\" href=\"";
        // line 274
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/clndr.css"), "html", null, true);
        echo "\" type=\"text/css\" />
                    <script src=\"";
        // line 275
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/underscore-min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
                    <script src= \"";
        // line 276
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/moment-2.2.1.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
                    <script src=\"";
        // line 277
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/clndr.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
                    <script src=\"";
        // line 278
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/site.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
                    <!----End Calender -------->
                </div>

                <div class=\"clearfix\"> </div>
            </div>


        </div>
        <div class=\"clearfix\"> </div>
    </div>
</div>
<!---->
<!--scrolling js-->
<script src=\"";
        // line 292
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.nicescroll.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 293
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/scripts.js"), "html", null, true);
        echo "\"></script>
<!--//scrolling js-->
<script src=\"";
        // line 295
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

</body>
</html>

";
        
        $__internal_666c7ed93bb07a0c501cf6ab45d4f91c35edcff5eeee4940c65a2d61d9a5a251->leave($__internal_666c7ed93bb07a0c501cf6ab45d4f91c35edcff5eeee4940c65a2d61d9a5a251_prof);

    }

    // line 269
    public function block_container($context, array $blocks = array())
    {
        $__internal_c717c5d697ccb6eaf43e83a763ae0ea5382501d7ef4a0f9b877f166145c04863 = $this->env->getExtension("native_profiler");
        $__internal_c717c5d697ccb6eaf43e83a763ae0ea5382501d7ef4a0f9b877f166145c04863->enter($__internal_c717c5d697ccb6eaf43e83a763ae0ea5382501d7ef4a0f9b877f166145c04863_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        echo " 


                    ";
        
        $__internal_c717c5d697ccb6eaf43e83a763ae0ea5382501d7ef4a0f9b877f166145c04863->leave($__internal_c717c5d697ccb6eaf43e83a763ae0ea5382501d7ef4a0f9b877f166145c04863_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  440 => 269,  427 => 295,  422 => 293,  418 => 292,  401 => 278,  397 => 277,  393 => 276,  389 => 275,  385 => 274,  382 => 273,  380 => 269,  356 => 248,  352 => 247,  342 => 240,  338 => 239,  329 => 233,  322 => 229,  318 => 228,  314 => 227,  306 => 222,  299 => 218,  291 => 213,  286 => 211,  281 => 209,  272 => 203,  254 => 188,  250 => 187,  246 => 186,  242 => 185,  135 => 81,  93 => 42,  69 => 21,  65 => 20,  61 => 19,  56 => 17,  52 => 16,  47 => 14,  43 => 13,  39 => 12,  34 => 10,  23 => 1,);
    }
}
/* <!DOCTYPE HTML>*/
/* <html>*/
/*     <head>*/
/*         <title>Tunisia Mall</title>*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*         <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, */
/*               Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />*/
/*         <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>*/
/*         <link href="{{asset('bundles/myappadmin/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />*/
/*         <!-- Custom Theme files -->*/
/*         <link href="{{asset('bundles/myappadmin/css/style.css')}}" rel='stylesheet' type='text/css' />*/
/*         <link href="{{asset('bundles/myappadmin/css/font-awesome.css')}}" rel="stylesheet"> */
/*         <script src="{{asset('bundles/myappadmin/js/jquery.min.js')}}"></script>*/
/*         <!-- Mainly scripts -->*/
/*         <script src="{{asset('bundles/myappadmin/js/jquery.metisMenu.js')}}"></script>*/
/*         <script src="{{asset('bundles/myappadmin/js/jquery.slimscroll.min.js')}}"></script>*/
/*         <!-- Custom and plugin javascript -->*/
/*         <link href="{{asset('bundles/myappadmin/css/custom.css')}}" rel="stylesheet">*/
/*         <script src="{{asset('bundles/myappadmin/js/custom.js')}}"></script>*/
/*         <script src="{{asset('bundles/myappadmin/js/screenfull.js')}}"></script>*/
/*         <script>*/
/* $(function() {*/
/* $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);*/
/* */
/* if (!screenfull.enabled) {*/
/* return false;*/
/* }*/
/* */
/* */
/* */
/* $('#toggle').click(function() {*/
/* screenfull.toggle($('#container')[0]);*/
/* });*/
/* */
/* });*/
/*         </script>*/
/* */
/*         <!----->*/
/* */
/*         <!--pie-chart--->*/
/*         <script src="{{asset('bundles/myappadmin/js/pie-chart.js')}}" type="text/javascript"></script>*/
/*         <script type="text/javascript">*/
/* */
/*             $(document).ready(function() {*/
/*                 $('#demo-pie-1').pieChart({*/
/*                     barColor: '#3bb2d0',*/
/*                     trackColor: '#eee',*/
/*                     lineCap: 'round',*/
/*                     lineWidth: 8,*/
/*                     onStep: function(from, to, percent) {*/
/*                         $(this.element).find('.pie-value').text(Math.round(percent) + '%');*/
/*                     }*/
/*                 });*/
/* */
/*                 $('#demo-pie-2').pieChart({*/
/*                     barColor: '#fbb03b',*/
/*                     trackColor: '#eee',*/
/*                     lineCap: 'butt',*/
/*                     lineWidth: 8,*/
/*                     onStep: function(from, to, percent) {*/
/*                         $(this.element).find('.pie-value').text(Math.round(percent) + '%');*/
/*                     }*/
/*                 });*/
/* */
/*                 $('#demo-pie-3').pieChart({*/
/*                     barColor: '#ed6498',*/
/*                     trackColor: '#eee',*/
/*                     lineCap: 'square',*/
/*                     lineWidth: 8,*/
/*                     onStep: function(from, to, percent) {*/
/*                         $(this.element).find('.pie-value').text(Math.round(percent) + '%');*/
/*                     }*/
/*                 });*/
/* */
/* */
/*             });*/
/* */
/*         </script>*/
/*         <!--skycons-icons-->*/
/*         <script src="{{asset('bundles/myappadmin/js/skycons.js')}}"></script>*/
/*         <!--//skycons-icons-->*/
/*     </head>*/
/*     <body>*/
/* */
/*         <div id="wrapper">*/
/* */
/*             <!----->*/
/*             <nav class="navbar-default navbar-static-top" role="navigation">*/
/*                 <div class="navbar-header">*/
/*                     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                         <span class="sr-only">Toggle navigation</span>*/
/*                         <span class="icon-bar"></span>*/
/*                         <span class="icon-bar"></span>*/
/*                         <span class="icon-bar"></span>*/
/*                     </button>*/
/*                     <h1> <a class="navbar-brand" href="#">Minimal</a></h1>         */
/*                 </div>*/
/*                 <div class=" border-bottom">*/
/*                     <div class="full-left">*/
/*                         <section class="full-top">*/
/*                             <button id="toggle"><i class="fa fa-arrows-alt"></i></button>	*/
/*                         </section>*/
/*                         <form class=" navbar-left-right">*/
/*                             <input type="text"  value="Search..." onfocus="this.value = '';" onblur="if (this.value == '') {*/
/*                           this.value = 'Search...';*/
/*                       }">*/
/*                             <input type="submit" value="" class="fa fa-search">*/
/*                         </form>*/
/*                         <div class="clearfix"> </div>*/
/*                     </div>*/
/* */
/* */
/*                     <!-- Brand and toggle get grouped for better mobile display -->*/
/* */
/*                     <!-- Collect the nav links, forms, and other content for toggling -->*/
/*                     <div class="drop-men" >*/
/*                         <ul class=" nav_1">*/
/* */
/*                             <li class="dropdown at-drop">*/
/*                                 <a href="#" class="dropdown-toggle dropdown-at " data-toggle="dropdown"><i class="fa fa-globe"></i> <span class="number">5</span></a>*/
/*                                 <ul class="dropdown-menu menu1 " role="menu">*/
/*                                     <li><a href="#">*/
/* */
/*                                             <div class="user-new">*/
/*                                                 <p>New user registered</p>*/
/*                                                 <span>40 seconds ago</span>*/
/*                                             </div>*/
/*                                             <div class="user-new-left">*/
/* */
/*                                                 <i class="fa fa-user-plus"></i>*/
/*                                             </div>*/
/*                                             <div class="clearfix"> </div>*/
/*                                         </a></li>*/
/*                                     <li><a href="#">*/
/*                                             <div class="user-new">*/
/*                                                 <p>Someone special liked this</p>*/
/*                                                 <span>3 minutes ago</span>*/
/*                                             </div>*/
/*                                             <div class="user-new-left">*/
/* */
/*                                                 <i class="fa fa-heart"></i>*/
/*                                             </div>*/
/*                                             <div class="clearfix"> </div>*/
/*                                         </a></li>*/
/*                                     <li><a href="#">*/
/*                                             <div class="user-new">*/
/*                                                 <p>John cancelled the event</p>*/
/*                                                 <span>4 hours ago</span>*/
/*                                             </div>*/
/*                                             <div class="user-new-left">*/
/* */
/*                                                 <i class="fa fa-times"></i>*/
/*                                             </div>*/
/*                                             <div class="clearfix"> </div>*/
/*                                         </a></li>*/
/*                                     <li><a href="#">*/
/*                                             <div class="user-new">*/
/*                                                 <p>The server is status is stable</p>*/
/*                                                 <span>yesterday at 08:30am</span>*/
/*                                             </div>*/
/*                                             <div class="user-new-left">*/
/* */
/*                                                 <i class="fa fa-info"></i>*/
/*                                             </div>*/
/*                                             <div class="clearfix"> </div>*/
/*                                         </a></li>*/
/*                                     <li><a href="#">*/
/*                                             <div class="user-new">*/
/*                                                 <p>New comments waiting approval</p>*/
/*                                                 <span>Last Week</span>*/
/*                                             </div>*/
/*                                             <div class="user-new-left">*/
/* */
/*                                                 <i class="fa fa-rss"></i>*/
/*                                             </div>*/
/*                                             <div class="clearfix"> </div>*/
/*                                         </a></li>*/
/*                                     <li><a href="#" class="view">View all messages</a></li>*/
/*                                 </ul>*/
/*                             </li>*/
/*                             <li class="dropdown">*/
/*                                 <a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown"><span class=" name-caret">Rackham<i class="caret"></i></span><img src="images/wo.jpg"></a>*/
/*                                 <ul class="dropdown-menu " role="menu">*/
/*                                     <li><a href="{{asset('profile.html')}}"><i class="fa fa-user"></i>Edit Profile</a></li>*/
/*                                     <li><a href="{{asset('inbox.html')}}"><i class="fa fa-envelope"></i>Inbox</a></li>*/
/*                                     <li><a href="{{asset('calendar.html')}}"><i class="fa fa-calendar"></i>Calender</a></li>*/
/*                                     <li><a href="{{asset('inbox.html')}}"><i class="fa fa-clipboard"></i>Tasks</a></li>*/
/*                                 </ul>*/
/*                             </li>*/
/* */
/*                         </ul>*/
/*                     </div><!-- /.navbar-collapse -->*/
/*                     <div class="clearfix">*/
/* */
/*                     </div>*/
/* */
/*                     <div class="navbar-default sidebar" role="navigation">*/
/*                         <div class="sidebar-nav navbar-collapse">*/
/*                             <ul class="nav" id="side-menu">*/
/* */
/*                                 <li>*/
/*                                     <a href="{{asset('index.html')}}" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Dashboards</span> </a>*/
/*                                 </li>*/
/* */
/*                                 <li>*/
/*                                     <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Menu Levels</span><span class="fa arrow"></span></a>*/
/*                                     <ul class="nav nav-second-level">*/
/*                                         <li><a href="{{asset('graphs.html')}}" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Graphs</a></li>*/
/* */
/*                                         <li><a href="{{asset('maps.html')}}" class=" hvr-bounce-to-right"><i class="fa fa-map-marker nav_icon"></i>Maps</a></li>*/
/* */
/*                                         <li><a href="{{asset('typography.html')}}" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>Typography</a></li>*/
/* */
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="{{asset('inbox.html')}}" class=" hvr-bounce-to-right"><i class="fa fa-inbox nav_icon"></i> <span class="nav-label">Inbox</span> </a>*/
/*                                 </li>*/
/* */
/*                                 <li>*/
/*                                     <a href="{{asset('gallery.html')}}" class=" hvr-bounce-to-right"><i class="fa fa-picture-o nav_icon"></i> <span class="nav-label">Gallery</span> </a>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-desktop nav_icon"></i> <span class="nav-label">Pages</span><span class="fa arrow"></span></a>*/
/*                                     <ul class="nav nav-second-level">*/
/*                                         <li><a href="{{asset('404.html')}}" class=" hvr-bounce-to-right"> <i class="fa fa-info-circle nav_icon"></i>Error 404</a></li>*/
/*                                         <li><a href="{{asset('faq.html')}}" class=" hvr-bounce-to-right"><i class="fa fa-question-circle nav_icon"></i>FAQ</a></li>*/
/*                                         <li><a href="{{asset('blank.html')}}" class=" hvr-bounce-to-right"><i class="fa fa-file-o nav_icon"></i>Blank</a></li>*/
/*                                     </ul>*/
/*                                 </li>*/
/*                                 <li>*/
/*                                     <a href="{{asset('layout.html')}}" class=" hvr-bounce-to-right"><i class="fa fa-th nav_icon"></i> <span class="nav-label">Grid Layouts</span> </a>*/
/*                                 </li>*/
/* */
/*                                 <li>*/
/*                                     <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-list nav_icon"></i> <span class="nav-label">Forms</span><span class="fa arrow"></span></a>*/
/*                                     <ul class="nav nav-second-level">*/
/*                                         <li><a href="{{asset('forms.html')}}" class=" hvr-bounce-to-right"><i class="fa fa-align-left nav_icon"></i>Basic forms</a></li>*/
/*                                         <li><a href="{{asset('validation.html')}}" class=" hvr-bounce-to-right"><i class="fa fa-check-square-o nav_icon"></i>Validation</a></li>*/
/*                                     </ul>*/
/*                                 </li>*/
/* */
/*                                 <li>*/
/*                                     <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-cog nav_icon"></i> <span class="nav-label">Settings</span><span class="fa arrow"></span></a>*/
/*                                     <ul class="nav nav-second-level">*/
/*                                         <li><a href="{{asset('signin.html')}}" class=" hvr-bounce-to-right"><i class="fa fa-sign-in nav_icon"></i>Signin</a></li>*/
/*                                         <li><a href="{{asset('signup.html')}}" class=" hvr-bounce-to-right"><i class="fa fa-sign-in nav_icon"></i>Singup</a></li>*/
/*                                     </ul>*/
/*                                 </li>*/
/*                             </ul>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*             </nav>*/
/*             <div id="page-wrapper" class="gray-bg dashbard-1">*/
/*                 <div class="content-main">*/
/* */
/*                     <!--banner-->	*/
/*                     <div class="banner">*/
/* */
/*                         <h2>*/
/*                             <a href="#">Home</a>*/
/*                             <i class="fa fa-angle-right"></i>*/
/*                             <span>Dashboard</span>*/
/*                         </h2>*/
/*                     </div>*/
/*                     <!--//banner-->*/
/*                     {% block  container %} */
/* */
/* */
/*                     {% endblock container %}*/
/*                     <!----Calender -------->*/
/*                     <link rel="stylesheet" href="{{asset('bundles/myappadmin/css/clndr.css')}}" type="text/css" />*/
/*                     <script src="{{asset('bundles/myappadmin/js/underscore-min.js')}}" type="text/javascript"></script>*/
/*                     <script src= "{{asset('bundles/myappadmin/js/moment-2.2.1.js')}}" type="text/javascript"></script>*/
/*                     <script src="{{asset('bundles/myappadmin/js/clndr.js')}}" type="text/javascript"></script>*/
/*                     <script src="{{asset('bundles/myappadmin/js/site.js')}}" type="text/javascript"></script>*/
/*                     <!----End Calender -------->*/
/*                 </div>*/
/* */
/*                 <div class="clearfix"> </div>*/
/*             </div>*/
/* */
/* */
/*         </div>*/
/*         <div class="clearfix"> </div>*/
/*     </div>*/
/* </div>*/
/* <!---->*/
/* <!--scrolling js-->*/
/* <script src="{{asset('bundles/myappadmin/js/jquery.nicescroll.js')}}"></script>*/
/* <script src="{{asset('bundles/myappadmin/js/scripts.js')}}"></script>*/
/* <!--//scrolling js-->*/
/* <script src="{{asset('bundles/myappadmin/js/bootstrap.min.js')}}"></script>*/
/* */
/* </body>*/
/* </html>*/
/* */
/* */
