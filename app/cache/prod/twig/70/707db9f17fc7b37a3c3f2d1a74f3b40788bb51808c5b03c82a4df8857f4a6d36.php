<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_33a01cb625c7110ea0ad26422629cf9a1e06e1692c5acbf42acd5634a9cad23d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2b95f62989a8628feb8d57a4d874ca733db52be8c264211bedafa060eda656e9 = $this->env->getExtension("native_profiler");
        $__internal_2b95f62989a8628feb8d57a4d874ca733db52be8c264211bedafa060eda656e9->enter($__internal_2b95f62989a8628feb8d57a4d874ca733db52be8c264211bedafa060eda656e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2b95f62989a8628feb8d57a4d874ca733db52be8c264211bedafa060eda656e9->leave($__internal_2b95f62989a8628feb8d57a4d874ca733db52be8c264211bedafa060eda656e9_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c4fb8870408418a26899be789ddb4195251089a42ee59470a444173d8a09ab1c = $this->env->getExtension("native_profiler");
        $__internal_c4fb8870408418a26899be789ddb4195251089a42ee59470a444173d8a09ab1c->enter($__internal_c4fb8870408418a26899be789ddb4195251089a42ee59470a444173d8a09ab1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Registration:register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_c4fb8870408418a26899be789ddb4195251089a42ee59470a444173d8a09ab1c->leave($__internal_c4fb8870408418a26899be789ddb4195251089a42ee59470a444173d8a09ab1c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Registration:register_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
