<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_e210d483af00e65c831d2190d3333dac11b89132fcbc564de959a2c60571891f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71b9dc49c21e4ba17e231726ed6c1a05da6544d238ed72709b10555cb0600973 = $this->env->getExtension("native_profiler");
        $__internal_71b9dc49c21e4ba17e231726ed6c1a05da6544d238ed72709b10555cb0600973->enter($__internal_71b9dc49c21e4ba17e231726ed6c1a05da6544d238ed72709b10555cb0600973_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        // line 1
        echo "
<!DOCTYPE HTML>
<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"fr\" xmlns:fb=\"http://www.facebook.com/2008/fbml\">
    <head>
        <title>Minimal an Admin Panel Category Flat Bootstrap Responsive Website Template | Signin :: w3layouts</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        <meta name=\"keywords\" content=\"Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\" />
        <script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/style.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
        <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 
        <script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />
        <meta charset=\"UTF-8\">

            ";
        // line 20
        $this->displayBlock('title', $context, $blocks);
        // line 21
        echo "            ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 35
        echo "            <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/js/a8de5014480b4daa57965aee6265098c_1452767442.js"), "html", null, true);
        echo "\" charset=\"utf-8\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/js/e644c8cbc8bd826f9f922db7d95f32b4_1446657238.js"), "html", null, true);
        echo "\" charset=\"utf-8\"></script>


    </head>
    <body>
        <div id=\"container\" class=\"\" >
            <header class=\"header-home\"><!--header site global-->
                <div class=\"int\">
                    <div id=\"logo\">
                        
                            <img src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/images/logo.png"), "html", null, true);
        echo "\"   style=\"\" alt=\"Créteil Soleil\" title=\"Créteil Soleil\" />
                     
                    </div>

                    <ul  class=\"rslides rslides1\"  id=\"opening\"><!--NEW BLOC horaires-->
                        <li class=\"\" style=\"display: list-item; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;\" id=\"rslides1_s0\">
                            
                        </li>
                    </ul><!--FIN NEW BLOC-->
                </div>
                <span id=\"accessMobile\">Menu</span>
                <div id=\"navigation\"><!--englobe les 2 nav pour responsive-->
                    <nav id=\"mainNav\"><!--navigation principale site-->
                        <ul class=\"int\">
                            <li>
                                <a href=\"#\">Acceuil</a>
                            </li>
                            <li>
                                <a href=\"#\">Boutiques</a>
                            </li>
                            <li>
                                <a href=\"#\">Catalogues</a>
                            </li>



                         ";
        // line 87
        echo "                        </ul>
                    </nav><!--navigation site-->
                    <div id=\"topBarSite\"><!--2eme nav et reseaux sociaux-->
                        <div class=\"int\">
                            <ul id=\"secondaryNav\"><!--New BLOC-->

                                <li>
                                    <a href=\"Creteil-Soleil/Horaires-Acces.html\" >Horaires & Accès</a>
                                </li>

                                <li>
                                    <a href=\"Creteil-Soleil/Plan-du-centre.html\" >Plan du centre</a>
                                </li>

                                <li>
                                    <a href=\"Creteil-Soleil/Services.html\" >News</a>
                                </li>


                            </ul><!--New BLOC-->
                            <ul class=\"blocSocialMedia\">
                                <li class=\"facebook\">
                                    <a href=\"#\" rel=\"nofollow\" target=\"_blank\">Facebook</a>
                                    <div class=\"contentbloc\">
                                        <a  href=\"#\">Follow @Facebook</a>
                                    </div>
                                </li>
                                <li class=\"twitter\">
                                    <a href=\"#\" rel=\"nofollow\" target=\"_blank\">Twitter</a>
                                    <div class=\"contentbloc\">
                                        <a href=\"#\">Follow @twitter</a>
                                    </div>
                                </li>
                                <li class=\"instagram\">
                                    <a href=\"#\" target=\"_blank\" rel=\"nofollow\" >Instagram</a>
                                    <div class=\"contentbloc\">
                                        <a href=\"#\">Follow @instagram</a>
                                </li>
                            </ul>        
                        </div>
                    </div>
                </div>
                    </div>
               
            </header>
        ";
        // line 133
        echo "       
            
       
            <div class=\"login-bottom\">
                <h2 class='bold\"'>Se connecter avec votre compte</h2>
                ";
        // line 138
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 139
            echo "                    <div >";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
                ";
        }
        // line 141
        echo "                <form action=\"";
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
                    <div class=\"col-md-6\">
                        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 143
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\" />
                        <div class=\"login-mail\">
                            <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 145
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" />
                            <i class=\"fa fa-envelope\"></i>
                        </div>
                        <div class=\"login-mail\">
                            <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" />
                            <i class=\"fa fa-lock\"></i>
                        </div>

                        <div class=\"news-letter\">
                            <label class=\"checkbox1\"><input id=\"remember_me\" type=\"checkbox\" name=\"_remember_me\" value=\"on\" ><i> </i>Se souvenir de moi</label>
                        </div>
                    </div>
                    <div class=\"col-md-6 login-do\">
                        <label class=\"hvr-shutter-in-horizontal login-sub\">
                            <input type=\"submit\" id=\"_submit\" class=\"btn btn-success\" name=\"_submit\" value=\"Connexion\">
                        </label>
                         <p>Vous n'avez pas de compte?</p>
                        <a href=\"";
        // line 162
        echo $this->env->getExtension('routing')->getPath("my_app_register_client");
        echo "\" class=\"hvr-shutter-in-horizontal\">Registration</a>
                    </div>
                </form>
            </div>
        

        <style>
            .login-bottom{
                height: 280px;
            }
        </style>
        <!---->
        <div class=\"copy-right\">
            <p> &copy; 2016 Tunisia Mall. All Rights Reserved | <a href=\"#\" target=\"_blank\">Tunisia Mall</a> </p>\t    </div>  
        <!--scrolling js-->
        <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.nicescroll.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/scripts.js"), "html", null, true);
        echo "\"></script>
        <!--//scrolling js-->
        <div id=\"container\" class=\"\" >
   <footer ><!--footer-->
            <div class=\"left\">
                <h2>Newsletter Tunisia Mall</h2>
                <p>Pour être informé sur:</p>
                <ul>
                    <li>Les sorties de nouvelles collections</li>
                    <li>Vos marques préférées</li>
                </ul>


                <form action=\"#\" id=\"newsletter_form\" method=\"post\">
                    <fieldset>
                        <input type=\"text\" name=\"email\" value=\"Votre email\" data-init-value=\"Votre email\" />
                        <input type=\"hidden\" name=\"centreId\" value=\"399\"/>
                        <input type=\"hidden\" name=\"centreName\" value=\"Créteil Soleil\"/>
                        <input type=\"submit\" value=\"ok\" />
                    </fieldset>
                </form>
            </div>

            <div class=\"right\">
                <h2>Application Mobile Tunisia Mall</h2>
                <p>Retrouvez votre centre Tunisia Mall sur votre smartphone et vos tablettes !</p>
                <a class=\"apple\" target=\"_blank\" href=\"#\"onClick=\"return xt_click(this, 'C', '75', 'Depuis_lien_footer:App_store', 'N');
                            return false;\">Appstore</a>
                <a class=\"google\" target=\"_blank\" href=\"#\" onClick=\"return xt_click(this, 'C', '75', 'Depuis_lien_footer:Google_Play', 'N');
                                    return false;\"> Google play</a>
            </div>


            <nav>
                <ul>
                    <li><a href=\"Creteil-Soleil/Nous-contacter.html\" title=\"Nous contacter\">Nous contacter</a></li>
                    <li><a href=\"Creteil-Soleil/Mentions-Legales.html\" title=\"Mentions Légales\">Mentions Légales</a></li>
                    <li><a href=\"Creteil-Soleil/Louer-un-emplacement-temporaire.html\" title=\"Louer un emplacement temporaire\">Visite guidee</a></li>
                </ul>
            </nav>


            <ul class=\"blocSocialMedia\">
                <li class=\"facebook\">
                    <a href=\"#\" target=\"_blank\" rel=\"nofollow\">Facebook</a>
                </li>
                <li class=\"twitter\">
                    <a href=\"#\" target=\"_blank\" rel=\"nofollow\">Twitter</a>
                </li>
                <li class=\"instagram\">
                    <a href=\"#\" target=\"_blank\" rel=\"nofollow\" >Instagram</a>
                </li>
            </ul>

        </footer><!--Fin footer-->        
    </div> </body>
</html>



";
        
        $__internal_71b9dc49c21e4ba17e231726ed6c1a05da6544d238ed72709b10555cb0600973->leave($__internal_71b9dc49c21e4ba17e231726ed6c1a05da6544d238ed72709b10555cb0600973_prof);

    }

    // line 20
    public function block_title($context, array $blocks = array())
    {
        $__internal_bb4058fb576ea98e151e87587bde90184259087ac59597bfda354d36fb65f51e = $this->env->getExtension("native_profiler");
        $__internal_bb4058fb576ea98e151e87587bde90184259087ac59597bfda354d36fb65f51e->enter($__internal_bb4058fb576ea98e151e87587bde90184259087ac59597bfda354d36fb65f51e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "<title>Tunisia Mall</title>";
        
        $__internal_bb4058fb576ea98e151e87587bde90184259087ac59597bfda354d36fb65f51e->leave($__internal_bb4058fb576ea98e151e87587bde90184259087ac59597bfda354d36fb65f51e_prof);

    }

    // line 21
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_948de508dd3e9d4118b0ae05780652643596af19a955ff7d83e8c6a3f904a869 = $this->env->getExtension("native_profiler");
        $__internal_948de508dd3e9d4118b0ae05780652643596af19a955ff7d83e8c6a3f904a869->enter($__internal_948de508dd3e9d4118b0ae05780652643596af19a955ff7d83e8c6a3f904a869_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 22
        echo "
                <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/6c6c9e8995c7ee151b5a1c238da4a176_1453471972_all.css"), "html", null, true);
        echo "\" />
                <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/mobile.css"), "html", null, true);
        echo "\" media=\"screen and (max-width:1100px)\" />
                <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/taggage.css"), "html", null, true);
        echo "\" />
                <style type=\"text/css\">        
                    #mainNav >ul >li >a:hover,#accessCatalogue > li .active,#mainNav >ul >.active >a,.back .add-wishlist,.scroll-list
                    {background-color:#bb9854;}
                    .cat,#opening a,#sidebar-nav strong,.listingCatalogue li h3, .listingCatalogue li h2,.fidelityContent p a,#modal-inscription span,.globalInfos h3,.front             .club-offer strong,.back .club-offer h2, #modal-add-wishlist p,#modal-shopping-alert p,#slider h2 .first,.backlinks a
                    {color:#bb9854;}
                    li.soon {border-color:#bb9854 !important;}
                    li.soon .mask-bonplan, li.soon .accessOffer a.access-infos{background-color:#bb9854;}
                </style>
            ";
        
        $__internal_948de508dd3e9d4118b0ae05780652643596af19a955ff7d83e8c6a3f904a869->leave($__internal_948de508dd3e9d4118b0ae05780652643596af19a955ff7d83e8c6a3f904a869_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  324 => 25,  320 => 24,  316 => 23,  313 => 22,  307 => 21,  295 => 20,  227 => 178,  223 => 177,  205 => 162,  185 => 145,  180 => 143,  174 => 141,  168 => 139,  166 => 138,  159 => 133,  112 => 87,  83 => 46,  70 => 36,  65 => 35,  62 => 21,  60 => 20,  53 => 16,  49 => 15,  45 => 14,  41 => 13,  36 => 11,  24 => 1,);
    }
}
/* */
/* <!DOCTYPE HTML>*/
/* <html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xmlns:fb="http://www.facebook.com/2008/fbml">*/
/*     <head>*/
/*         <title>Minimal an Admin Panel Category Flat Bootstrap Responsive Website Template | Signin :: w3layouts</title>*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*         <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, */
/*               Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />*/
/*         <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>*/
/*         <link href="{{asset('bundles/myappadmin/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />*/
/*         <!-- Custom Theme files -->*/
/*         <link href="{{asset('bundles/myappadmin/css/style.css')}}" rel='stylesheet' type='text/css' />*/
/*         <link href="{{asset('bundles/myappadmin/css/font-awesome.css')}}" rel="stylesheet"> */
/*         <script src="{{asset('bundles/myappadmin/js/jquery.min.js')}}"></script>*/
/*         <script src="{{asset('bundles/myappadmin/js/bootstrap.min.js')}}"></script>*/
/*         <meta http-equiv="content-type" content="text/html;charset=utf-8" />*/
/*         <meta charset="UTF-8">*/
/* */
/*             {% block title %}<title>Tunisia Mall</title>{% endblock %}*/
/*             {% block stylesheets %}*/
/* */
/*                 <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/6c6c9e8995c7ee151b5a1c238da4a176_1453471972_all.css') }}" />*/
/*                 <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/mobile.css') }}" media="screen and (max-width:1100px)" />*/
/*                 <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/taggage.css') }}" />*/
/*                 <style type="text/css">        */
/*                     #mainNav >ul >li >a:hover,#accessCatalogue > li .active,#mainNav >ul >.active >a,.back .add-wishlist,.scroll-list*/
/*                     {background-color:#bb9854;}*/
/*                     .cat,#opening a,#sidebar-nav strong,.listingCatalogue li h3, .listingCatalogue li h2,.fidelityContent p a,#modal-inscription span,.globalInfos h3,.front             .club-offer strong,.back .club-offer h2, #modal-add-wishlist p,#modal-shopping-alert p,#slider h2 .first,.backlinks a*/
/*                     {color:#bb9854;}*/
/*                     li.soon {border-color:#bb9854 !important;}*/
/*                     li.soon .mask-bonplan, li.soon .accessOffer a.access-infos{background-color:#bb9854;}*/
/*                 </style>*/
/*             {% endblock %}*/
/*             <script type="text/javascript" src="{{ asset('bundles/myappuser/js/a8de5014480b4daa57965aee6265098c_1452767442.js') }}" charset="utf-8"></script>*/
/*             <script type="text/javascript" src="{{ asset('bundles/myappuser/js/e644c8cbc8bd826f9f922db7d95f32b4_1446657238.js') }}" charset="utf-8"></script>*/
/* */
/* */
/*     </head>*/
/*     <body>*/
/*         <div id="container" class="" >*/
/*             <header class="header-home"><!--header site global-->*/
/*                 <div class="int">*/
/*                     <div id="logo">*/
/*                         */
/*                             <img src="{{ asset('bundles/myappuser/images/logo.png') }}"   style="" alt="Créteil Soleil" title="Créteil Soleil" />*/
/*                      */
/*                     </div>*/
/* */
/*                     <ul  class="rslides rslides1"  id="opening"><!--NEW BLOC horaires-->*/
/*                         <li class="" style="display: list-item; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;" id="rslides1_s0">*/
/*                             */
/*                         </li>*/
/*                     </ul><!--FIN NEW BLOC-->*/
/*                 </div>*/
/*                 <span id="accessMobile">Menu</span>*/
/*                 <div id="navigation"><!--englobe les 2 nav pour responsive-->*/
/*                     <nav id="mainNav"><!--navigation principale site-->*/
/*                         <ul class="int">*/
/*                             <li>*/
/*                                 <a href="#">Acceuil</a>*/
/*                             </li>*/
/*                             <li>*/
/*                                 <a href="#">Boutiques</a>*/
/*                             </li>*/
/*                             <li>*/
/*                                 <a href="#">Catalogues</a>*/
/*                             </li>*/
/* */
/* */
/* */
/*                          {#   {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*                                 <li class="right wish-list">*/
/*                                     <a rel="nofollow" id="regZone_off" href="{{ path('fos_user_profile_show') }}">Profil</a>*/
/*                                 </li>*/
/*                                 <li class="right wish-list">*/
/*                                     <a rel="nofollow" id="regZone_off" href="{{ path('fos_user_security_logout') }}">deconnexion</a>*/
/*                                 </li>*/
/*                             {% else %}*/
/*                                 <li class="right wish-list">*/
/*                                     <a rel="nofollow" id="regZone_off" href="{{ path('fos_user_security_login') }}" >Connexion</a>*/
/*                                 </li>*/
/*                                 <li class="right wish-list">*/
/*                                     <a rel="nofollow" id="regZone_off" href="{{ path('fos_user_registration_register') }}">Inscription</a>*/
/*                                 </li>*/
/*                             {% endif %}#}*/
/*                         </ul>*/
/*                     </nav><!--navigation site-->*/
/*                     <div id="topBarSite"><!--2eme nav et reseaux sociaux-->*/
/*                         <div class="int">*/
/*                             <ul id="secondaryNav"><!--New BLOC-->*/
/* */
/*                                 <li>*/
/*                                     <a href="Creteil-Soleil/Horaires-Acces.html" >Horaires & Accès</a>*/
/*                                 </li>*/
/* */
/*                                 <li>*/
/*                                     <a href="Creteil-Soleil/Plan-du-centre.html" >Plan du centre</a>*/
/*                                 </li>*/
/* */
/*                                 <li>*/
/*                                     <a href="Creteil-Soleil/Services.html" >News</a>*/
/*                                 </li>*/
/* */
/* */
/*                             </ul><!--New BLOC-->*/
/*                             <ul class="blocSocialMedia">*/
/*                                 <li class="facebook">*/
/*                                     <a href="#" rel="nofollow" target="_blank">Facebook</a>*/
/*                                     <div class="contentbloc">*/
/*                                         <a  href="#">Follow @Facebook</a>*/
/*                                     </div>*/
/*                                 </li>*/
/*                                 <li class="twitter">*/
/*                                     <a href="#" rel="nofollow" target="_blank">Twitter</a>*/
/*                                     <div class="contentbloc">*/
/*                                         <a href="#">Follow @twitter</a>*/
/*                                     </div>*/
/*                                 </li>*/
/*                                 <li class="instagram">*/
/*                                     <a href="#" target="_blank" rel="nofollow" >Instagram</a>*/
/*                                     <div class="contentbloc">*/
/*                                         <a href="#">Follow @instagram</a>*/
/*                                 </li>*/
/*                             </ul>        */
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                     </div>*/
/*                */
/*             </header>*/
/*         {% trans_default_domain 'FOSUserBundle' %}*/
/*        */
/*             */
/*        */
/*             <div class="login-bottom">*/
/*                 <h2 class='bold"'>Se connecter avec votre compte</h2>*/
/*                 {% if error %}*/
/*                     <div >{{ error.messageKey|trans(error.messageData, 'security') }}</div>*/
/*                 {% endif %}*/
/*                 <form action="{{ path("fos_user_security_check") }}" method="post">*/
/*                     <div class="col-md-6">*/
/*                         <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />*/
/*                         <div class="login-mail">*/
/*                             <input type="text" id="username" name="_username" value="{{ last_username }}" required="required" />*/
/*                             <i class="fa fa-envelope"></i>*/
/*                         </div>*/
/*                         <div class="login-mail">*/
/*                             <input type="password" id="password" name="_password" required="required" />*/
/*                             <i class="fa fa-lock"></i>*/
/*                         </div>*/
/* */
/*                         <div class="news-letter">*/
/*                             <label class="checkbox1"><input id="remember_me" type="checkbox" name="_remember_me" value="on" ><i> </i>Se souvenir de moi</label>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-md-6 login-do">*/
/*                         <label class="hvr-shutter-in-horizontal login-sub">*/
/*                             <input type="submit" id="_submit" class="btn btn-success" name="_submit" value="Connexion">*/
/*                         </label>*/
/*                          <p>Vous n'avez pas de compte?</p>*/
/*                         <a href="{{path('my_app_register_client')}}" class="hvr-shutter-in-horizontal">Registration</a>*/
/*                     </div>*/
/*                 </form>*/
/*             </div>*/
/*         */
/* */
/*         <style>*/
/*             .login-bottom{*/
/*                 height: 280px;*/
/*             }*/
/*         </style>*/
/*         <!---->*/
/*         <div class="copy-right">*/
/*             <p> &copy; 2016 Tunisia Mall. All Rights Reserved | <a href="#" target="_blank">Tunisia Mall</a> </p>	    </div>  */
/*         <!--scrolling js-->*/
/*         <script src="{{asset('bundles/myappadmin/js/jquery.nicescroll.js')}}"></script>*/
/*         <script src="{{asset('bundles/myappadmin/js/scripts.js')}}"></script>*/
/*         <!--//scrolling js-->*/
/*         <div id="container" class="" >*/
/*    <footer ><!--footer-->*/
/*             <div class="left">*/
/*                 <h2>Newsletter Tunisia Mall</h2>*/
/*                 <p>Pour être informé sur:</p>*/
/*                 <ul>*/
/*                     <li>Les sorties de nouvelles collections</li>*/
/*                     <li>Vos marques préférées</li>*/
/*                 </ul>*/
/* */
/* */
/*                 <form action="#" id="newsletter_form" method="post">*/
/*                     <fieldset>*/
/*                         <input type="text" name="email" value="Votre email" data-init-value="Votre email" />*/
/*                         <input type="hidden" name="centreId" value="399"/>*/
/*                         <input type="hidden" name="centreName" value="Créteil Soleil"/>*/
/*                         <input type="submit" value="ok" />*/
/*                     </fieldset>*/
/*                 </form>*/
/*             </div>*/
/* */
/*             <div class="right">*/
/*                 <h2>Application Mobile Tunisia Mall</h2>*/
/*                 <p>Retrouvez votre centre Tunisia Mall sur votre smartphone et vos tablettes !</p>*/
/*                 <a class="apple" target="_blank" href="#"onClick="return xt_click(this, 'C', '75', 'Depuis_lien_footer:App_store', 'N');*/
/*                             return false;">Appstore</a>*/
/*                 <a class="google" target="_blank" href="#" onClick="return xt_click(this, 'C', '75', 'Depuis_lien_footer:Google_Play', 'N');*/
/*                                     return false;"> Google play</a>*/
/*             </div>*/
/* */
/* */
/*             <nav>*/
/*                 <ul>*/
/*                     <li><a href="Creteil-Soleil/Nous-contacter.html" title="Nous contacter">Nous contacter</a></li>*/
/*                     <li><a href="Creteil-Soleil/Mentions-Legales.html" title="Mentions Légales">Mentions Légales</a></li>*/
/*                     <li><a href="Creteil-Soleil/Louer-un-emplacement-temporaire.html" title="Louer un emplacement temporaire">Visite guidee</a></li>*/
/*                 </ul>*/
/*             </nav>*/
/* */
/* */
/*             <ul class="blocSocialMedia">*/
/*                 <li class="facebook">*/
/*                     <a href="#" target="_blank" rel="nofollow">Facebook</a>*/
/*                 </li>*/
/*                 <li class="twitter">*/
/*                     <a href="#" target="_blank" rel="nofollow">Twitter</a>*/
/*                 </li>*/
/*                 <li class="instagram">*/
/*                     <a href="#" target="_blank" rel="nofollow" >Instagram</a>*/
/*                 </li>*/
/*             </ul>*/
/* */
/*         </footer><!--Fin footer-->        */
/*     </div> </body>*/
/* </html>*/
/* */
/* */
/* */
/* */
