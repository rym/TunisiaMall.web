<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_c8153e08f4dea56fafb6af48bbd12e93329b21805c2904f4f8280335e1b56327 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout_front_office.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout_front_office.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_05530a5425dc0338cf8295168d8a9400b9d68f8e6dc62901bfaf6e839c4c59a1 = $this->env->getExtension("native_profiler");
        $__internal_05530a5425dc0338cf8295168d8a9400b9d68f8e6dc62901bfaf6e839c4c59a1->enter($__internal_05530a5425dc0338cf8295168d8a9400b9d68f8e6dc62901bfaf6e839c4c59a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_05530a5425dc0338cf8295168d8a9400b9d68f8e6dc62901bfaf6e839c4c59a1->leave($__internal_05530a5425dc0338cf8295168d8a9400b9d68f8e6dc62901bfaf6e839c4c59a1_prof);

    }

    // line 5
    public function block_container($context, array $blocks = array())
    {
        $__internal_a6fe7f1a59303ed21ab3407d76690b772594739129e26bc24a0294b7644fe1c1 = $this->env->getExtension("native_profiler");
        $__internal_a6fe7f1a59303ed21ab3407d76690b772594739129e26bc24a0294b7644fe1c1->enter($__internal_a6fe7f1a59303ed21ab3407d76690b772594739129e26bc24a0294b7644fe1c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_a6fe7f1a59303ed21ab3407d76690b772594739129e26bc24a0294b7644fe1c1->leave($__internal_a6fe7f1a59303ed21ab3407d76690b772594739129e26bc24a0294b7644fe1c1_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 8,  45 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout_front_office.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block container %}*/
/*     <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>*/
/*     {% if targetUrl %}*/
/*     <p><a href="{{ targetUrl }}">{{ 'registration.back'|trans }}</a></p>*/
/*     {% endif %}*/
/* {% endblock container %}*/
/* */
