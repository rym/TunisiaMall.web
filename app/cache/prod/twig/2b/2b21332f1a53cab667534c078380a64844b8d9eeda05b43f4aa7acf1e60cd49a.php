<?php

/* MyAppAdminBundle:Admin:responsables.html.twig */
class __TwigTemplate_bdc85f6844d90b3670a5ee0c7058b47c572e583d55b3ba5eb5c0789b346cbfed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MyAppAdminBundle::layout.html.twig", "MyAppAdminBundle:Admin:responsables.html.twig", 1);
        $this->blocks = array(
            'container' => array($this, 'block_container'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MyAppAdminBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f0237898b5c834e131e254387166dfb872e166f7b1ba1d8f41a26ee983a7195 = $this->env->getExtension("native_profiler");
        $__internal_7f0237898b5c834e131e254387166dfb872e166f7b1ba1d8f41a26ee983a7195->enter($__internal_7f0237898b5c834e131e254387166dfb872e166f7b1ba1d8f41a26ee983a7195_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MyAppAdminBundle:Admin:responsables.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7f0237898b5c834e131e254387166dfb872e166f7b1ba1d8f41a26ee983a7195->leave($__internal_7f0237898b5c834e131e254387166dfb872e166f7b1ba1d8f41a26ee983a7195_prof);

    }

    // line 2
    public function block_container($context, array $blocks = array())
    {
        $__internal_eed1a231817fd19bbf8ed521892c650e280aa993d2e66a3e49e4fa1b79719925 = $this->env->getExtension("native_profiler");
        $__internal_eed1a231817fd19bbf8ed521892c650e280aa993d2e66a3e49e4fa1b79719925->enter($__internal_eed1a231817fd19bbf8ed521892c650e280aa993d2e66a3e49e4fa1b79719925_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 3
        echo "    <div class=\"table-responsive\">          
        <table class=\"table\">
            <thead>
                <tr>
                    <th> nom</th>
                    <th> prenom</th>
                    <th> user name</th>
                    <th> Email</th>
                    <th colspan=\"2\">Action</th>
                </tr>
                ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["responsables"]) ? $context["responsables"] : $this->getContext($context, "responsables")));
        foreach ($context['_seq'] as $context["_key"] => $context["responsable"]) {
            // line 14
            echo "                    <tr>
                        <td> ";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["responsable"], "nom", array()), "html", null, true);
            echo "</td>
                        <td> ";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["responsable"], "prenom", array()), "html", null, true);
            echo "</td>
                        <td> ";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["responsable"], "username", array()), "html", null, true);
            echo "</td>
                        <td> ";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["responsable"], "email", array()), "html", null, true);
            echo "</td>
                        <td> <a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_app_admin_delete_responsable", array("id" => $this->getAttribute($context["responsable"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-danger\" >Supprimer</a></td>
                        ";
            // line 20
            if (($this->getAttribute($context["responsable"], "locked", array()) == true)) {
                // line 21
                echo "                            <td> <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("my_app_admin_debloquer_responsable", array("id" => $this->getAttribute($context["responsable"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-info\" >Activer</a></td>
                        
                        ";
            }
            // line 24
            echo "                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['responsable'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "
            </thead>
        </table>
    </div>

";
        
        $__internal_eed1a231817fd19bbf8ed521892c650e280aa993d2e66a3e49e4fa1b79719925->leave($__internal_eed1a231817fd19bbf8ed521892c650e280aa993d2e66a3e49e4fa1b79719925_prof);

    }

    public function getTemplateName()
    {
        return "MyAppAdminBundle:Admin:responsables.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 26,  88 => 24,  81 => 21,  79 => 20,  75 => 19,  71 => 18,  67 => 17,  63 => 16,  59 => 15,  56 => 14,  52 => 13,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "MyAppAdminBundle::layout.html.twig" %}*/
/* {% block container %}*/
/*     <div class="table-responsive">          */
/*         <table class="table">*/
/*             <thead>*/
/*                 <tr>*/
/*                     <th> nom</th>*/
/*                     <th> prenom</th>*/
/*                     <th> user name</th>*/
/*                     <th> Email</th>*/
/*                     <th colspan="2">Action</th>*/
/*                 </tr>*/
/*                 {% for responsable in responsables %}*/
/*                     <tr>*/
/*                         <td> {{responsable.nom}}</td>*/
/*                         <td> {{responsable.prenom}}</td>*/
/*                         <td> {{responsable.username}}</td>*/
/*                         <td> {{responsable.email}}</td>*/
/*                         <td> <a href="{{path('my_app_admin_delete_responsable',{'id':responsable.id})}}" class="btn btn-danger" >Supprimer</a></td>*/
/*                         {%if responsable.locked == true%}*/
/*                             <td> <a href="{{path('my_app_admin_debloquer_responsable',{'id':responsable.id})}}" class="btn btn-info" >Activer</a></td>*/
/*                         */
/*                         {% endif %}*/
/*                     </tr>*/
/*                 {% endfor %}*/
/* */
/*             </thead>*/
/*         </table>*/
/*     </div>*/
/* */
/* {% endblock container %}*/
/* */
/* */
