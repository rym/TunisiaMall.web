<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_10bcf5c46a0848268318993fe1d6dda894e8b824a57946a8102339ba406b701d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e792754c0652554692a6a17a7522a1f49d033ac718dbb031220c5c47a8e6afd5 = $this->env->getExtension("native_profiler");
        $__internal_e792754c0652554692a6a17a7522a1f49d033ac718dbb031220c5c47a8e6afd5->enter($__internal_e792754c0652554692a6a17a7522a1f49d033ac718dbb031220c5c47a8e6afd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e792754c0652554692a6a17a7522a1f49d033ac718dbb031220c5c47a8e6afd5->leave($__internal_e792754c0652554692a6a17a7522a1f49d033ac718dbb031220c5c47a8e6afd5_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_332e09ed29b35c6c4e106589aee61c56ca8656c05ccf7838016b5a0035525e87 = $this->env->getExtension("native_profiler");
        $__internal_332e09ed29b35c6c4e106589aee61c56ca8656c05ccf7838016b5a0035525e87->enter($__internal_332e09ed29b35c6c4e106589aee61c56ca8656c05ccf7838016b5a0035525e87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_332e09ed29b35c6c4e106589aee61c56ca8656c05ccf7838016b5a0035525e87->leave($__internal_332e09ed29b35c6c4e106589aee61c56ca8656c05ccf7838016b5a0035525e87_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
