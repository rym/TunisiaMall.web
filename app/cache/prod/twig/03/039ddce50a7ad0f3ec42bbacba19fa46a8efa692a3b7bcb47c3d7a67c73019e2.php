<?php

/* FOSUserBundle:Registration:register-client.html.twig */
class __TwigTemplate_d80aa1107b6d4ea6dddb054620dea260a661efa165b4be4516ae83587951acba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'container' => array($this, 'block_container'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_55d269b1a003a1002d21ef723fadf7ceb0270906ade53778ea6a0cfcd4d1a464 = $this->env->getExtension("native_profiler");
        $__internal_55d269b1a003a1002d21ef723fadf7ceb0270906ade53778ea6a0cfcd4d1a464->enter($__internal_55d269b1a003a1002d21ef723fadf7ceb0270906ade53778ea6a0cfcd4d1a464_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register-client.html.twig"));

        // line 2
        $this->displayBlock('container', $context, $blocks);
        
        $__internal_55d269b1a003a1002d21ef723fadf7ceb0270906ade53778ea6a0cfcd4d1a464->leave($__internal_55d269b1a003a1002d21ef723fadf7ceb0270906ade53778ea6a0cfcd4d1a464_prof);

    }

    public function block_container($context, array $blocks = array())
    {
        $__internal_2be3f8be5853e9e8cd46d73e778f6588100cf92f61d8cca52b49a75b9421db68 = $this->env->getExtension("native_profiler");
        $__internal_2be3f8be5853e9e8cd46d73e778f6588100cf92f61d8cca52b49a75b9421db68->enter($__internal_2be3f8be5853e9e8cd46d73e778f6588100cf92f61d8cca52b49a75b9421db68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 3
        echo "
    <html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"fr\" xmlns:fb=\"http://www.facebook.com/2008/fbml\">
        <head>
            <title>Minimal an Admin Panel Category Flat Bootstrap Responsive Website Template | Signin :: w3layouts</title>
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
                <meta name=\"keywords\" content=\"Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
                      Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\" />
                <script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
                <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
                <!-- Custom Theme files -->
                <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/style.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
                <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 
                    <script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
                    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
                    <meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />
                    <meta charset=\"UTF-8\">

                        ";
        // line 21
        $this->displayBlock('title', $context, $blocks);
        // line 22
        echo "                        ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 36
        echo "                        <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/js/a8de5014480b4daa57965aee6265098c_1452767442.js"), "html", null, true);
        echo "\" charset=\"utf-8\"></script>
                        <script type=\"text/javascript\" src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/js/e644c8cbc8bd826f9f922db7d95f32b4_1446657238.js"), "html", null, true);
        echo "\" charset=\"utf-8\"></script>
                        </head>
                        <body>
                            <div id=\"container\" class=\"\" >
                                <header class=\"header-home\"><!--header site global-->
                                    <div class=\"int\">
                                        <div id=\"logo\">
                                          
                                                <img src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/images/logo.png"), "html", null, true);
        echo "\"   style=\"\" alt=\"Créteil Soleil\" title=\"Créteil Soleil\" />
                                            
                                        </div>

                                        <ul  class=\"rslides rslides1\"  id=\"opening\"><!--NEW BLOC horaires-->
                                            <li class=\"\" style=\"display: list-item; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;\" id=\"rslides1_s0\">
                                                <a href=\"Creteil-Soleil/Horaires-Acces.html\">

                                                </a>
                                            </li>
                                        </ul><!--FIN NEW BLOC-->
                                    </div>
                                    <span id=\"accessMobile\">Menu</span>
                                    <div id=\"navigation\"><!--englobe les 2 nav pour responsive-->
                                        <nav id=\"mainNav\"><!--navigation principale site-->
                                            <ul class=\"int\">
                                                <li>
                                                    <a href=\"#\">Acceuil</a>
                                                </li>
                                                <li>
                                                    <a href=\"#\">Boutiques</a>
                                                </li>
                                                <li>
                                                    <a href=\"#\">Catalogues</a>
                                                </li>
                                            </ul>
                                        </nav>
                                        <div id=\"topBarSite\"><!--2eme nav et reseaux sociaux-->
                                            <div class=\"int\">
                                                <ul id=\"secondaryNav\"><!--New BLOC-->

                                                    <li>
                                                        <a href=\"Creteil-Soleil/Horaires-Acces.html\" >Horaires & Accès</a>
                                                    </li>

                                                    <li>
                                                        <a href=\"Creteil-Soleil/Plan-du-centre.html\" >Plan du centre</a>
                                                    </li>

                                                    <li>
                                                        <a href=\"Creteil-Soleil/Services.html\" >News</a>
                                                    </li>


                                                </ul><!--New BLOC-->
                                                <ul class=\"blocSocialMedia\">
                                                    <li class=\"facebook\">
                                                        <a href=\"#\" rel=\"nofollow\" target=\"_blank\">Facebook</a>
                                                        <div class=\"contentbloc\">
                                                            <a  href=\"#\">Follow @Facebook</a>
                                                        </div>
                                                    </li>
                                                    <li class=\"twitter\">
                                                        <a href=\"#\" rel=\"nofollow\" target=\"_blank\">Twitter</a>
                                                        <div class=\"contentbloc\">
                                                            <a href=\"#\">Follow @twitter</a>
                                                        </div>
                                                    </li>
                                                    <li class=\"instagram\">
                                                        <a href=\"#\" target=\"_blank\" rel=\"nofollow\" >Instagram</a>
                                                        <div class=\"contentbloc\">
                                                            <a href=\"#\">Follow @instagram</a>
                                                    </li>
                                                </ul>        
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            </header>

                            ";
        // line 117
        echo "
                            <div class=\"login-bottom\">
                                <center><h2>Register</h2></center>

                                <form action=\"";
        // line 121
        echo $this->env->getExtension('routing')->getPath("my_app_register_client");
        echo "\"";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'enctype');
        echo " method=\"POST\">  
                                    ";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_token", array()), 'row');
        echo "
                                    <div class=\"login-mail\">
                                        ";
        // line 124
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget', array("attr" => array("placeholder" => "Nom")));
        echo "
                                        <i class=\"fa fa-user\"></i>
                                    </div>
                                    <div class=\"login-mail\">
                                        ";
        // line 128
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget', array("attr" => array("placeholder" => "Prenom")));
        echo "
                                        <i class=\"fa fa-user\"></i>
                                    </div>
                                    <div class=\"login-mail\">
                                        ";
        // line 132
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget', array("attr" => array("placeholder" => "Nom dutilisateur")));
        echo "
                                        <i class=\"fa fa-user\"></i>
                                    </div>
                                    <div class=\"login-mail\">
                                        ";
        // line 136
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("placeholder" => "E-mail")));
        echo "
                                        <i class=\"fa fa-envelope\"></i>
                                    </div>

                                    ";
        // line 140
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["passwordField"]) {
            // line 141
            echo "                                        <div class=\"login-mail\">
                                            ";
            // line 142
            if (($this->getAttribute($context["loop"], "index", array()) == 1)) {
                // line 143
                echo "                                                ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["passwordField"], 'widget', array("attr" => array("placeholder" => "Mot de passe")));
                echo "
                                                   <i class=\"fa fa-lock\"></i>
                                            ";
            } else {
                // line 146
                echo "                                                ";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["passwordField"], 'widget', array("attr" => array("placeholder" => "confirmation")));
                echo "
                                                   <i class=\"fa fa-lock\"></i>
                                            ";
            }
            // line 149
            echo "                                        </div>
                                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['passwordField'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 151
        echo "                                   

                                    <div class=\"login-do\">
                                        <label class=\"hvr-shutter-in-horizontal login-sub\">
                                            <input type=\"submit\" id=\"_submit\" class=\"btn btn-success\" name=\"_submit\" value=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\">
                                        </label>
                                        <p>déja enregistrer</p>

                                    </div>
                                </form>
                            </div>
                            </div>

                            <style>
                                .login-bottom{
                                    height: 520px;
                                }
                            </style>
                            <!---->

                            <div class=\"copy-right\">
                                <p> &copy; 2016 Tunisia Mall. All Rights Reserved | <a href=\"#\" target=\"_blank\">Tunisia Mall</a> </p>       </div>  
                            <!--scrolling js-->
                            <script src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/jquery.nicescroll.js"), "html", null, true);
        echo "\"></script>
                            <script src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappadmin/js/scripts.js"), "html", null, true);
        echo "\"></script>
                            <!--//scrolling js-->
                            <div id=\"container\" class=\"\" >
                                <footer ><!--footer-->
                                    <div class=\"left\">
                                        <h2>Newsletter Tunisia Mall</h2>
                                        <p>Pour être informé sur:</p>
                                        <ul>
                                            <li>Les sorties de nouvelles collections</li>
                                            <li>Vos marques préférées</li>
                                        </ul>


                                        <form action=\"#\" id=\"newsletter_form\" method=\"post\">
                                            <fieldset>
                                                <input type=\"text\" name=\"email\" value=\"Votre email\" data-init-value=\"Votre email\" />
                                                <input type=\"hidden\" name=\"centreId\" value=\"399\"/>
                                                <input type=\"hidden\" name=\"centreName\" value=\"Créteil Soleil\"/>
                                                <input type=\"submit\" value=\"ok\" />
                                            </fieldset>
                                        </form>
                                    </div>

                                    <div class=\"right\">
                                        <h2>Application Mobile Tunisia Mall</h2>
                                        <p>Retrouvez votre centre Tunisia Mall sur votre smartphone et vos tablettes !</p>
                                        <a class=\"apple\" target=\"_blank\" href=\"#\"onClick=\"return xt_click(this, 'C', '75', 'Depuis_lien_footer:App_store', 'N');
                                                return false;\">Appstore</a>
                                        <a class=\"google\" target=\"_blank\" href=\"#\" onClick=\"return xt_click(this, 'C', '75', 'Depuis_lien_footer:Google_Play', 'N');
                                                return false;\"> Google play</a>
                                    </div>


                                    <nav>
                                        <ul>
                                            <li><a href=\"Creteil-Soleil/Nous-contacter.html\" title=\"Nous contacter\">Nous contacter</a></li>
                                            <li><a href=\"Creteil-Soleil/Mentions-Legales.html\" title=\"Mentions Légales\">Mentions Légales</a></li>
                                            <li><a href=\"Creteil-Soleil/Louer-un-emplacement-temporaire.html\" title=\"Louer un emplacement temporaire\">Visite guidee</a></li>
                                        </ul>
                                    </nav>


                                    <ul class=\"blocSocialMedia\">
                                        <li class=\"facebook\">
                                            <a href=\"#\" target=\"_blank\" rel=\"nofollow\">Facebook</a>
                                        </li>
                                        <li class=\"twitter\">
                                            <a href=\"#\" target=\"_blank\" rel=\"nofollow\">Twitter</a>
                                        </li>
                                        <li class=\"instagram\">
                                            <a href=\"#\" target=\"_blank\" rel=\"nofollow\" >Instagram</a>
                                        </li>
                                    </ul>

                                </footer><!--Fin footer-->        
                            </div>
                        </body>
                        </html>
";
        
        $__internal_2be3f8be5853e9e8cd46d73e778f6588100cf92f61d8cca52b49a75b9421db68->leave($__internal_2be3f8be5853e9e8cd46d73e778f6588100cf92f61d8cca52b49a75b9421db68_prof);

    }

    // line 21
    public function block_title($context, array $blocks = array())
    {
        $__internal_849f65aac80321a39afac36ad093d7d8569110ebe54984107cbb66f9c196d93d = $this->env->getExtension("native_profiler");
        $__internal_849f65aac80321a39afac36ad093d7d8569110ebe54984107cbb66f9c196d93d->enter($__internal_849f65aac80321a39afac36ad093d7d8569110ebe54984107cbb66f9c196d93d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "<title>Tunisia Mall</title>";
        
        $__internal_849f65aac80321a39afac36ad093d7d8569110ebe54984107cbb66f9c196d93d->leave($__internal_849f65aac80321a39afac36ad093d7d8569110ebe54984107cbb66f9c196d93d_prof);

    }

    // line 22
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_00f28135ca7e556230c4a58957a1cb2c05c1633b5473f9c74c0fad70af3ca7ee = $this->env->getExtension("native_profiler");
        $__internal_00f28135ca7e556230c4a58957a1cb2c05c1633b5473f9c74c0fad70af3ca7ee->enter($__internal_00f28135ca7e556230c4a58957a1cb2c05c1633b5473f9c74c0fad70af3ca7ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 23
        echo "
                            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/6c6c9e8995c7ee151b5a1c238da4a176_1453471972_all.css"), "html", null, true);
        echo "\" />
                            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/mobile.css"), "html", null, true);
        echo "\" media=\"screen and (max-width:1100px)\" />
                            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/taggage.css"), "html", null, true);
        echo "\" />
                            <style type=\"text/css\">        
                                #mainNav >ul >li >a:hover,#accessCatalogue > li .active,#mainNav >ul >.active >a,.back .add-wishlist,.scroll-list
                                {background-color:#bb9854;}
                                .cat,#opening a,#sidebar-nav strong,.listingCatalogue li h3, .listingCatalogue li h2,.fidelityContent p a,#modal-inscription span,.globalInfos h3,.front             .club-offer strong,.back .club-offer h2, #modal-add-wishlist p,#modal-shopping-alert p,#slider h2 .first,.backlinks a
                                {color:#bb9854;}
                                li.soon {border-color:#bb9854 !important;}
                                li.soon .mask-bonplan, li.soon .accessOffer a.access-infos{background-color:#bb9854;}
                            </style>
                        ";
        
        $__internal_00f28135ca7e556230c4a58957a1cb2c05c1633b5473f9c74c0fad70af3ca7ee->leave($__internal_00f28135ca7e556230c4a58957a1cb2c05c1633b5473f9c74c0fad70af3ca7ee_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register-client.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  390 => 26,  386 => 25,  382 => 24,  379 => 23,  373 => 22,  361 => 21,  295 => 175,  291 => 174,  269 => 155,  263 => 151,  248 => 149,  241 => 146,  234 => 143,  232 => 142,  229 => 141,  212 => 140,  205 => 136,  198 => 132,  191 => 128,  184 => 124,  179 => 122,  173 => 121,  167 => 117,  93 => 45,  82 => 37,  77 => 36,  74 => 22,  72 => 21,  65 => 17,  61 => 16,  57 => 15,  53 => 14,  48 => 12,  37 => 3,  25 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% block container %}*/
/* */
/*     <html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xmlns:fb="http://www.facebook.com/2008/fbml">*/
/*         <head>*/
/*             <title>Minimal an Admin Panel Category Flat Bootstrap Responsive Website Template | Signin :: w3layouts</title>*/
/*             <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*                 <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, */
/*                       Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />*/
/*                 <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>*/
/*                 <link href="{{asset('bundles/myappadmin/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css' />*/
/*                 <!-- Custom Theme files -->*/
/*                 <link href="{{asset('bundles/myappadmin/css/style.css')}}" rel='stylesheet' type='text/css' />*/
/*                 <link href="{{asset('bundles/myappadmin/css/font-awesome.css')}}" rel="stylesheet"> */
/*                     <script src="{{asset('bundles/myappadmin/js/jquery.min.js')}}"></script>*/
/*                     <script src="{{asset('bundles/myappadmin/js/bootstrap.min.js')}}"></script>*/
/*                     <meta http-equiv="content-type" content="text/html;charset=utf-8" />*/
/*                     <meta charset="UTF-8">*/
/* */
/*                         {% block title %}<title>Tunisia Mall</title>{% endblock %}*/
/*                         {% block stylesheets %}*/
/* */
/*                             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/6c6c9e8995c7ee151b5a1c238da4a176_1453471972_all.css') }}" />*/
/*                             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/mobile.css') }}" media="screen and (max-width:1100px)" />*/
/*                             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/taggage.css') }}" />*/
/*                             <style type="text/css">        */
/*                                 #mainNav >ul >li >a:hover,#accessCatalogue > li .active,#mainNav >ul >.active >a,.back .add-wishlist,.scroll-list*/
/*                                 {background-color:#bb9854;}*/
/*                                 .cat,#opening a,#sidebar-nav strong,.listingCatalogue li h3, .listingCatalogue li h2,.fidelityContent p a,#modal-inscription span,.globalInfos h3,.front             .club-offer strong,.back .club-offer h2, #modal-add-wishlist p,#modal-shopping-alert p,#slider h2 .first,.backlinks a*/
/*                                 {color:#bb9854;}*/
/*                                 li.soon {border-color:#bb9854 !important;}*/
/*                                 li.soon .mask-bonplan, li.soon .accessOffer a.access-infos{background-color:#bb9854;}*/
/*                             </style>*/
/*                         {% endblock %}*/
/*                         <script type="text/javascript" src="{{ asset('bundles/myappuser/js/a8de5014480b4daa57965aee6265098c_1452767442.js') }}" charset="utf-8"></script>*/
/*                         <script type="text/javascript" src="{{ asset('bundles/myappuser/js/e644c8cbc8bd826f9f922db7d95f32b4_1446657238.js') }}" charset="utf-8"></script>*/
/*                         </head>*/
/*                         <body>*/
/*                             <div id="container" class="" >*/
/*                                 <header class="header-home"><!--header site global-->*/
/*                                     <div class="int">*/
/*                                         <div id="logo">*/
/*                                           */
/*                                                 <img src="{{ asset('bundles/myappuser/images/logo.png') }}"   style="" alt="Créteil Soleil" title="Créteil Soleil" />*/
/*                                             */
/*                                         </div>*/
/* */
/*                                         <ul  class="rslides rslides1"  id="opening"><!--NEW BLOC horaires-->*/
/*                                             <li class="" style="display: list-item; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;" id="rslides1_s0">*/
/*                                                 <a href="Creteil-Soleil/Horaires-Acces.html">*/
/* */
/*                                                 </a>*/
/*                                             </li>*/
/*                                         </ul><!--FIN NEW BLOC-->*/
/*                                     </div>*/
/*                                     <span id="accessMobile">Menu</span>*/
/*                                     <div id="navigation"><!--englobe les 2 nav pour responsive-->*/
/*                                         <nav id="mainNav"><!--navigation principale site-->*/
/*                                             <ul class="int">*/
/*                                                 <li>*/
/*                                                     <a href="#">Acceuil</a>*/
/*                                                 </li>*/
/*                                                 <li>*/
/*                                                     <a href="#">Boutiques</a>*/
/*                                                 </li>*/
/*                                                 <li>*/
/*                                                     <a href="#">Catalogues</a>*/
/*                                                 </li>*/
/*                                             </ul>*/
/*                                         </nav>*/
/*                                         <div id="topBarSite"><!--2eme nav et reseaux sociaux-->*/
/*                                             <div class="int">*/
/*                                                 <ul id="secondaryNav"><!--New BLOC-->*/
/* */
/*                                                     <li>*/
/*                                                         <a href="Creteil-Soleil/Horaires-Acces.html" >Horaires & Accès</a>*/
/*                                                     </li>*/
/* */
/*                                                     <li>*/
/*                                                         <a href="Creteil-Soleil/Plan-du-centre.html" >Plan du centre</a>*/
/*                                                     </li>*/
/* */
/*                                                     <li>*/
/*                                                         <a href="Creteil-Soleil/Services.html" >News</a>*/
/*                                                     </li>*/
/* */
/* */
/*                                                 </ul><!--New BLOC-->*/
/*                                                 <ul class="blocSocialMedia">*/
/*                                                     <li class="facebook">*/
/*                                                         <a href="#" rel="nofollow" target="_blank">Facebook</a>*/
/*                                                         <div class="contentbloc">*/
/*                                                             <a  href="#">Follow @Facebook</a>*/
/*                                                         </div>*/
/*                                                     </li>*/
/*                                                     <li class="twitter">*/
/*                                                         <a href="#" rel="nofollow" target="_blank">Twitter</a>*/
/*                                                         <div class="contentbloc">*/
/*                                                             <a href="#">Follow @twitter</a>*/
/*                                                         </div>*/
/*                                                     </li>*/
/*                                                     <li class="instagram">*/
/*                                                         <a href="#" target="_blank" rel="nofollow" >Instagram</a>*/
/*                                                         <div class="contentbloc">*/
/*                                                             <a href="#">Follow @instagram</a>*/
/*                                                     </li>*/
/*                                                 </ul>        */
/*                                             </div>*/
/*                                         </div>*/
/*                                     </div>*/
/*                             </div>*/
/* */
/*                             </header>*/
/* */
/*                             {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/*                             <div class="login-bottom">*/
/*                                 <center><h2>Register</h2></center>*/
/* */
/*                                 <form action="{{ path('my_app_register_client') }}"{{ form_enctype(form) }} method="POST">  */
/*                                     {{ form_row(form._token) }}*/
/*                                     <div class="login-mail">*/
/*                                         {{ form_widget(form.nom,{ 'attr': {'placeholder': 'Nom'} }) }}*/
/*                                         <i class="fa fa-user"></i>*/
/*                                     </div>*/
/*                                     <div class="login-mail">*/
/*                                         {{ form_widget(form.prenom,{ 'attr': {'placeholder': 'Prenom'} }) }}*/
/*                                         <i class="fa fa-user"></i>*/
/*                                     </div>*/
/*                                     <div class="login-mail">*/
/*                                         {{ form_widget(form.username,{ 'attr': {'placeholder': 'Nom dutilisateur'} }) }}*/
/*                                         <i class="fa fa-user"></i>*/
/*                                     </div>*/
/*                                     <div class="login-mail">*/
/*                                         {{ form_widget(form.email,{ 'attr': {'placeholder': 'E-mail'} }) }}*/
/*                                         <i class="fa fa-envelope"></i>*/
/*                                     </div>*/
/* */
/*                                     {% for passwordField in form.plainPassword %}*/
/*                                         <div class="login-mail">*/
/*                                             {% if loop.index == 1 %}*/
/*                                                 {{form_widget(passwordField,{ 'attr': {'placeholder': 'Mot de passe'} })}}*/
/*                                                    <i class="fa fa-lock"></i>*/
/*                                             {%else%}*/
/*                                                 {{form_widget(passwordField,{ 'attr': {'placeholder': 'confirmation'} })}}*/
/*                                                    <i class="fa fa-lock"></i>*/
/*                                             {%endif%}*/
/*                                         </div>*/
/*                                     {% endfor %}*/
/*                                    */
/* */
/*                                     <div class="login-do">*/
/*                                         <label class="hvr-shutter-in-horizontal login-sub">*/
/*                                             <input type="submit" id="_submit" class="btn btn-success" name="_submit" value="{{ 'registration.submit'|trans({}, 'FOSUserBundle') }}">*/
/*                                         </label>*/
/*                                         <p>déja enregistrer</p>*/
/* */
/*                                     </div>*/
/*                                 </form>*/
/*                             </div>*/
/*                             </div>*/
/* */
/*                             <style>*/
/*                                 .login-bottom{*/
/*                                     height: 520px;*/
/*                                 }*/
/*                             </style>*/
/*                             <!---->*/
/* */
/*                             <div class="copy-right">*/
/*                                 <p> &copy; 2016 Tunisia Mall. All Rights Reserved | <a href="#" target="_blank">Tunisia Mall</a> </p>       </div>  */
/*                             <!--scrolling js-->*/
/*                             <script src="{{asset('bundles/myappadmin/js/jquery.nicescroll.js')}}"></script>*/
/*                             <script src="{{asset('bundles/myappadmin/js/scripts.js')}}"></script>*/
/*                             <!--//scrolling js-->*/
/*                             <div id="container" class="" >*/
/*                                 <footer ><!--footer-->*/
/*                                     <div class="left">*/
/*                                         <h2>Newsletter Tunisia Mall</h2>*/
/*                                         <p>Pour être informé sur:</p>*/
/*                                         <ul>*/
/*                                             <li>Les sorties de nouvelles collections</li>*/
/*                                             <li>Vos marques préférées</li>*/
/*                                         </ul>*/
/* */
/* */
/*                                         <form action="#" id="newsletter_form" method="post">*/
/*                                             <fieldset>*/
/*                                                 <input type="text" name="email" value="Votre email" data-init-value="Votre email" />*/
/*                                                 <input type="hidden" name="centreId" value="399"/>*/
/*                                                 <input type="hidden" name="centreName" value="Créteil Soleil"/>*/
/*                                                 <input type="submit" value="ok" />*/
/*                                             </fieldset>*/
/*                                         </form>*/
/*                                     </div>*/
/* */
/*                                     <div class="right">*/
/*                                         <h2>Application Mobile Tunisia Mall</h2>*/
/*                                         <p>Retrouvez votre centre Tunisia Mall sur votre smartphone et vos tablettes !</p>*/
/*                                         <a class="apple" target="_blank" href="#"onClick="return xt_click(this, 'C', '75', 'Depuis_lien_footer:App_store', 'N');*/
/*                                                 return false;">Appstore</a>*/
/*                                         <a class="google" target="_blank" href="#" onClick="return xt_click(this, 'C', '75', 'Depuis_lien_footer:Google_Play', 'N');*/
/*                                                 return false;"> Google play</a>*/
/*                                     </div>*/
/* */
/* */
/*                                     <nav>*/
/*                                         <ul>*/
/*                                             <li><a href="Creteil-Soleil/Nous-contacter.html" title="Nous contacter">Nous contacter</a></li>*/
/*                                             <li><a href="Creteil-Soleil/Mentions-Legales.html" title="Mentions Légales">Mentions Légales</a></li>*/
/*                                             <li><a href="Creteil-Soleil/Louer-un-emplacement-temporaire.html" title="Louer un emplacement temporaire">Visite guidee</a></li>*/
/*                                         </ul>*/
/*                                     </nav>*/
/* */
/* */
/*                                     <ul class="blocSocialMedia">*/
/*                                         <li class="facebook">*/
/*                                             <a href="#" target="_blank" rel="nofollow">Facebook</a>*/
/*                                         </li>*/
/*                                         <li class="twitter">*/
/*                                             <a href="#" target="_blank" rel="nofollow">Twitter</a>*/
/*                                         </li>*/
/*                                         <li class="instagram">*/
/*                                             <a href="#" target="_blank" rel="nofollow" >Instagram</a>*/
/*                                         </li>*/
/*                                     </ul>*/
/* */
/*                                 </footer><!--Fin footer-->        */
/*                             </div>*/
/*                         </body>*/
/*                         </html>*/
/* {% endblock container %}*/
