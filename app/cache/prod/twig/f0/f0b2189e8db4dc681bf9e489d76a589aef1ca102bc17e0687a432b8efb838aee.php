<?php

/* FOSUserBundle::layout_front_office.html.twig */
class __TwigTemplate_84df994e57039c8aa4b5979da2795f34d780a2873fb883ce3c2f8c5a1ef2076b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'container' => array($this, 'block_container'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_17e7dd99689cce78336b305ab35ae59ff1c22f148c26d89eae186564ab51d68e = $this->env->getExtension("native_profiler");
        $__internal_17e7dd99689cce78336b305ab35ae59ff1c22f148c26d89eae186564ab51d68e->enter($__internal_17e7dd99689cce78336b305ab35ae59ff1c22f148c26d89eae186564ab51d68e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle::layout_front_office.html.twig"));

        // line 1
        echo "<!doctype html>
<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"fr\" xmlns:fb=\"http://www.facebook.com/2008/fbml\">

    <head>
        <meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />
        <meta charset=\"UTF-8\">

            ";
        // line 8
        $this->displayBlock('title', $context, $blocks);
        // line 9
        echo "            ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 23
        echo "            <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/js/a8de5014480b4daa57965aee6265098c_1452767442.js"), "html", null, true);
        echo "\" charset=\"utf-8\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/js/e644c8cbc8bd826f9f922db7d95f32b4_1446657238.js"), "html", null, true);
        echo "\" charset=\"utf-8\"></script>


    </head>
    <body>    
        <div id=\"container\" class=\"\" >
            <header class=\"header-home\"><!--header site global-->
                <div class=\"int\">
                    <div id=\"logo\">
                        
                            <img src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/images/logo.png"), "html", null, true);
        echo " />
                        </a>
                    </div>

                    <ul  class=\"rslides rslides1\"  id=\"opening\"><!--NEW BLOC horaires-->
                        <li class=\"\" style=\"display: list-item; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;\" id=\"rslides1_s0\">
                           
                                Ouvert le lundi et samedi de 10h à 20h<br />
                                Du mardi au vendredi de 10h à 21h</a>
                        </li>
                    </ul><!--FIN NEW BLOC-->
                </div>
                <span id=\"accessMobile\">Menu</span>
                <div id=\"navigation\"><!--englobe les 2 nav pour responsive-->
                    <nav id=\"mainNav\"><!--navigation principale site-->
                        <ul class=\"int\">
                            <li>
                                <a href=\"#\">Acceuil</a>
                            </li>
                            <li>
                                <a href=\"#\">Boutiques</a>
                            </li>
                            <li>
                                <a href=\"#\">Catalogues</a>
                            </li>



                         ";
        // line 77
        echo "                        </ul>
                    </nav><!--navigation site-->
                    <div id=\"topBarSite\"><!--2eme nav et reseaux sociaux-->
                        <div class=\"int\">
                            <ul id=\"secondaryNav\"><!--New BLOC-->

                                <li>
                                    <a  >Horaires & Accès</a>
                                </li>

                                <li>
                                    <a  >Plan du centre</a>
                                </li>

                                <li>
                                    <a >News</a>
                                </li>


                            </ul><!--New BLOC-->
                            <ul class=\"blocSocialMedia\">
                                <li class=\"facebook\">
                                    <a href=\"#\" rel=\"nofollow\" target=\"_blank\">Facebook</a>
                                    <div class=\"contentbloc\">
                                        <a  href=\"#\">Follow @Facebook</a>
                                    </div>
                                </li>
                                <li class=\"twitter\">
                                    <a href=\"#\" rel=\"nofollow\" target=\"_blank\">Twitter</a>
                                    <div class=\"contentbloc\">
                                        <a href=\"#\">Follow @twitter</a>
                                    </div>
                                </li>
                                <li class=\"instagram\">
                                    <a href=\"#\" target=\"_blank\" rel=\"nofollow\" >Instagram</a>
                                    <div class=\"contentbloc\">
                                        <a href=\"#\">Follow @instagram</a>
                                </li>
                            </ul>        
                        </div>
                    </div>
                </div>
                <form id=\"globalSearch\" method=\"post\"><!--recherche site-->
                    <fieldset class=\"int\">
                        <input id=\"searchTextTags\" type=\"text\" value=\"Recherche\" onFocus=\"\"/>
                        <input type=\"submit\" value=\"OK\"  id=\"autoCompletionSubmit\" />
                    </fieldset>
                </form><!--Fin recherche site-->
            </header>


            <main id=\"wrapper\" class=\"home\">    


            ";
        // line 131
        $this->displayBlock('container', $context, $blocks);
        // line 134
        echo "  
        </main>
        <footer ><!--footer-->
            <div class=\"left\">
                <h2>Newsletter Tunisia Mall</h2>
                <p>Pour être informé sur:</p>
                <ul>
                    <li>Les sorties de nouvelles collections</li>
                    <li>Vos marques préférées</li>
                </ul>


                <form action=\"#\" id=\"newsletter_form\" method=\"post\">
                    <fieldset>
                        <input type=\"text\" name=\"email\" value=\"Votre email\" data-init-value=\"Votre email\" />
                        <input type=\"hidden\" name=\"centreId\" value=\"399\"/>
                        <input type=\"hidden\" name=\"centreName\" value=\"Créteil Soleil\"/>
                        <input type=\"submit\" value=\"ok\" />
                    </fieldset>
                </form>
            </div>

            <div class=\"right\">
                <h2>Application Mobile Tunisia Mall</h2>
                <p>Retrouvez votre centre Tunisia Mall sur votre smartphone et vos tablettes !</p>
                <a class=\"apple\" target=\"_blank\" href=\"#\"onClick=\"return xt_click(this, 'C', '75', 'Depuis_lien_footer:App_store', 'N');
                            return false;\">Appstore</a>
                <a class=\"google\" target=\"_blank\" href=\"#\" onClick=\"return xt_click(this, 'C', '75', 'Depuis_lien_footer:Google_Play', 'N');
                                    return false;\"> Google play</a>
            </div>


            <nav>
                <ul>
                    <li><a title=\"Nous contacter\">Nous contacter</a></li>
                    <li><a title=\"Mentions Légales\">Mentions Légales</a></li>
                    <li><a title=\"Louer un emplacement temporaire\">Visite guidee</a></li>
                </ul>
            </nav>


            <ul class=\"blocSocialMedia\">
                <li class=\"facebook\">
                    <a href=\"#\" target=\"_blank\" rel=\"nofollow\">Facebook</a>
                </li>
                <li class=\"twitter\">
                    <a href=\"#\" target=\"_blank\" rel=\"nofollow\">Twitter</a>
                </li>
                <li class=\"instagram\">
                    <a href=\"#\" target=\"_blank\" rel=\"nofollow\" >Instagram</a>
                </li>
            </ul>

        </footer><!--Fin footer-->        
    </div> </body>
</html>";
        
        $__internal_17e7dd99689cce78336b305ab35ae59ff1c22f148c26d89eae186564ab51d68e->leave($__internal_17e7dd99689cce78336b305ab35ae59ff1c22f148c26d89eae186564ab51d68e_prof);

    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        $__internal_5a5b0a6b5bef39581cc0334aa1e9d85f811d7da669fe722d818ecff25f2c65d5 = $this->env->getExtension("native_profiler");
        $__internal_5a5b0a6b5bef39581cc0334aa1e9d85f811d7da669fe722d818ecff25f2c65d5->enter($__internal_5a5b0a6b5bef39581cc0334aa1e9d85f811d7da669fe722d818ecff25f2c65d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "<title>Tunisia Mall</title>";
        
        $__internal_5a5b0a6b5bef39581cc0334aa1e9d85f811d7da669fe722d818ecff25f2c65d5->leave($__internal_5a5b0a6b5bef39581cc0334aa1e9d85f811d7da669fe722d818ecff25f2c65d5_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_eb47bb4fe8f1b1db906ce96ad3621bfa7723f688923f7103f6b8b26179d84e90 = $this->env->getExtension("native_profiler");
        $__internal_eb47bb4fe8f1b1db906ce96ad3621bfa7723f688923f7103f6b8b26179d84e90->enter($__internal_eb47bb4fe8f1b1db906ce96ad3621bfa7723f688923f7103f6b8b26179d84e90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "
                <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/6c6c9e8995c7ee151b5a1c238da4a176_1453471972_all.css"), "html", null, true);
        echo "\" />
                <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/mobile.css"), "html", null, true);
        echo "\" media=\"screen and (max-width:1100px)\" />
                <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/myappuser/css/taggage.css"), "html", null, true);
        echo "\" />
                <style type=\"text/css\">        
                    #mainNav >ul >li >a:hover,#accessCatalogue > li .active,#mainNav >ul >.active >a,.back .add-wishlist,.scroll-list
                    {background-color:#bb9854;}
                    .cat,#opening a,#sidebar-nav strong,.listingCatalogue li h3, .listingCatalogue li h2,.fidelityContent p a,#modal-inscription span,.globalInfos h3,.front             .club-offer strong,.back .club-offer h2, #modal-add-wishlist p,#modal-shopping-alert p,#slider h2 .first,.backlinks a
                    {color:#bb9854;}
                    li.soon {border-color:#bb9854 !important;}
                    li.soon .mask-bonplan, li.soon .accessOffer a.access-infos{background-color:#bb9854;}
                </style>
            ";
        
        $__internal_eb47bb4fe8f1b1db906ce96ad3621bfa7723f688923f7103f6b8b26179d84e90->leave($__internal_eb47bb4fe8f1b1db906ce96ad3621bfa7723f688923f7103f6b8b26179d84e90_prof);

    }

    // line 131
    public function block_container($context, array $blocks = array())
    {
        $__internal_961350b4e84222a5217964457adb5fe8c48df22578c8641e3781edf8e613b495 = $this->env->getExtension("native_profiler");
        $__internal_961350b4e84222a5217964457adb5fe8c48df22578c8641e3781edf8e613b495->enter($__internal_961350b4e84222a5217964457adb5fe8c48df22578c8641e3781edf8e613b495_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "container"));

        // line 132
        echo "            
            
            ";
        
        $__internal_961350b4e84222a5217964457adb5fe8c48df22578c8641e3781edf8e613b495->leave($__internal_961350b4e84222a5217964457adb5fe8c48df22578c8641e3781edf8e613b495_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle::layout_front_office.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  260 => 132,  254 => 131,  237 => 13,  233 => 12,  229 => 11,  226 => 10,  220 => 9,  208 => 8,  146 => 134,  144 => 131,  88 => 77,  57 => 34,  44 => 24,  39 => 23,  36 => 9,  34 => 8,  25 => 1,);
    }
}
/* <!doctype html>*/
/* <html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xmlns:fb="http://www.facebook.com/2008/fbml">*/
/* */
/*     <head>*/
/*         <meta http-equiv="content-type" content="text/html;charset=utf-8" />*/
/*         <meta charset="UTF-8">*/
/* */
/*             {% block title %}<title>Tunisia Mall</title>{% endblock %}*/
/*             {% block stylesheets %}*/
/* */
/*                 <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/6c6c9e8995c7ee151b5a1c238da4a176_1453471972_all.css') }}" />*/
/*                 <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/mobile.css') }}" media="screen and (max-width:1100px)" />*/
/*                 <link rel="stylesheet" type="text/css" href="{{ asset('bundles/myappuser/css/taggage.css') }}" />*/
/*                 <style type="text/css">        */
/*                     #mainNav >ul >li >a:hover,#accessCatalogue > li .active,#mainNav >ul >.active >a,.back .add-wishlist,.scroll-list*/
/*                     {background-color:#bb9854;}*/
/*                     .cat,#opening a,#sidebar-nav strong,.listingCatalogue li h3, .listingCatalogue li h2,.fidelityContent p a,#modal-inscription span,.globalInfos h3,.front             .club-offer strong,.back .club-offer h2, #modal-add-wishlist p,#modal-shopping-alert p,#slider h2 .first,.backlinks a*/
/*                     {color:#bb9854;}*/
/*                     li.soon {border-color:#bb9854 !important;}*/
/*                     li.soon .mask-bonplan, li.soon .accessOffer a.access-infos{background-color:#bb9854;}*/
/*                 </style>*/
/*             {% endblock %}*/
/*             <script type="text/javascript" src="{{ asset('bundles/myappuser/js/a8de5014480b4daa57965aee6265098c_1452767442.js') }}" charset="utf-8"></script>*/
/*             <script type="text/javascript" src="{{ asset('bundles/myappuser/js/e644c8cbc8bd826f9f922db7d95f32b4_1446657238.js') }}" charset="utf-8"></script>*/
/* */
/* */
/*     </head>*/
/*     <body>    */
/*         <div id="container" class="" >*/
/*             <header class="header-home"><!--header site global-->*/
/*                 <div class="int">*/
/*                     <div id="logo">*/
/*                         */
/*                             <img src="{{ asset('bundles/myappuser/images/logo.png') }} />*/
/*                         </a>*/
/*                     </div>*/
/* */
/*                     <ul  class="rslides rslides1"  id="opening"><!--NEW BLOC horaires-->*/
/*                         <li class="" style="display: list-item; float: none; position: absolute; opacity: 0; z-index: 1; transition: opacity 500ms ease-in-out 0s;" id="rslides1_s0">*/
/*                            */
/*                                 Ouvert le lundi et samedi de 10h à 20h<br />*/
/*                                 Du mardi au vendredi de 10h à 21h</a>*/
/*                         </li>*/
/*                     </ul><!--FIN NEW BLOC-->*/
/*                 </div>*/
/*                 <span id="accessMobile">Menu</span>*/
/*                 <div id="navigation"><!--englobe les 2 nav pour responsive-->*/
/*                     <nav id="mainNav"><!--navigation principale site-->*/
/*                         <ul class="int">*/
/*                             <li>*/
/*                                 <a href="#">Acceuil</a>*/
/*                             </li>*/
/*                             <li>*/
/*                                 <a href="#">Boutiques</a>*/
/*                             </li>*/
/*                             <li>*/
/*                                 <a href="#">Catalogues</a>*/
/*                             </li>*/
/* */
/* */
/* */
/*                          {#   {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*                                 <li class="right wish-list">*/
/*                                     <a rel="nofollow" id="regZone_off" href="{{ path('fos_user_profile_show') }}">Profil</a>*/
/*                                 </li>*/
/*                                 <li class="right wish-list">*/
/*                                     <a rel="nofollow" id="regZone_off" href="{{ path('fos_user_security_logout') }}">deconnexion</a>*/
/*                                 </li>*/
/*                             {% else %}*/
/*                                 <li class="right wish-list">*/
/*                                     <a rel="nofollow" id="regZone_off" href="{{ path('fos_user_security_login') }}" >Connexion</a>*/
/*                                 </li>*/
/*                                 <li class="right wish-list">*/
/*                                     <a rel="nofollow" id="regZone_off" href="{{ path('fos_user_registration_register') }}">Inscription</a>*/
/*                                 </li>*/
/*                             {% endif %}#}*/
/*                         </ul>*/
/*                     </nav><!--navigation site-->*/
/*                     <div id="topBarSite"><!--2eme nav et reseaux sociaux-->*/
/*                         <div class="int">*/
/*                             <ul id="secondaryNav"><!--New BLOC-->*/
/* */
/*                                 <li>*/
/*                                     <a  >Horaires & Accès</a>*/
/*                                 </li>*/
/* */
/*                                 <li>*/
/*                                     <a  >Plan du centre</a>*/
/*                                 </li>*/
/* */
/*                                 <li>*/
/*                                     <a >News</a>*/
/*                                 </li>*/
/* */
/* */
/*                             </ul><!--New BLOC-->*/
/*                             <ul class="blocSocialMedia">*/
/*                                 <li class="facebook">*/
/*                                     <a href="#" rel="nofollow" target="_blank">Facebook</a>*/
/*                                     <div class="contentbloc">*/
/*                                         <a  href="#">Follow @Facebook</a>*/
/*                                     </div>*/
/*                                 </li>*/
/*                                 <li class="twitter">*/
/*                                     <a href="#" rel="nofollow" target="_blank">Twitter</a>*/
/*                                     <div class="contentbloc">*/
/*                                         <a href="#">Follow @twitter</a>*/
/*                                     </div>*/
/*                                 </li>*/
/*                                 <li class="instagram">*/
/*                                     <a href="#" target="_blank" rel="nofollow" >Instagram</a>*/
/*                                     <div class="contentbloc">*/
/*                                         <a href="#">Follow @instagram</a>*/
/*                                 </li>*/
/*                             </ul>        */
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <form id="globalSearch" method="post"><!--recherche site-->*/
/*                     <fieldset class="int">*/
/*                         <input id="searchTextTags" type="text" value="Recherche" onFocus=""/>*/
/*                         <input type="submit" value="OK"  id="autoCompletionSubmit" />*/
/*                     </fieldset>*/
/*                 </form><!--Fin recherche site-->*/
/*             </header>*/
/* */
/* */
/*             <main id="wrapper" class="home">    */
/* */
/* */
/*             {% block container %}*/
/*             */
/*             */
/*             {% endblock container%}  */
/*         </main>*/
/*         <footer ><!--footer-->*/
/*             <div class="left">*/
/*                 <h2>Newsletter Tunisia Mall</h2>*/
/*                 <p>Pour être informé sur:</p>*/
/*                 <ul>*/
/*                     <li>Les sorties de nouvelles collections</li>*/
/*                     <li>Vos marques préférées</li>*/
/*                 </ul>*/
/* */
/* */
/*                 <form action="#" id="newsletter_form" method="post">*/
/*                     <fieldset>*/
/*                         <input type="text" name="email" value="Votre email" data-init-value="Votre email" />*/
/*                         <input type="hidden" name="centreId" value="399"/>*/
/*                         <input type="hidden" name="centreName" value="Créteil Soleil"/>*/
/*                         <input type="submit" value="ok" />*/
/*                     </fieldset>*/
/*                 </form>*/
/*             </div>*/
/* */
/*             <div class="right">*/
/*                 <h2>Application Mobile Tunisia Mall</h2>*/
/*                 <p>Retrouvez votre centre Tunisia Mall sur votre smartphone et vos tablettes !</p>*/
/*                 <a class="apple" target="_blank" href="#"onClick="return xt_click(this, 'C', '75', 'Depuis_lien_footer:App_store', 'N');*/
/*                             return false;">Appstore</a>*/
/*                 <a class="google" target="_blank" href="#" onClick="return xt_click(this, 'C', '75', 'Depuis_lien_footer:Google_Play', 'N');*/
/*                                     return false;"> Google play</a>*/
/*             </div>*/
/* */
/* */
/*             <nav>*/
/*                 <ul>*/
/*                     <li><a title="Nous contacter">Nous contacter</a></li>*/
/*                     <li><a title="Mentions Légales">Mentions Légales</a></li>*/
/*                     <li><a title="Louer un emplacement temporaire">Visite guidee</a></li>*/
/*                 </ul>*/
/*             </nav>*/
/* */
/* */
/*             <ul class="blocSocialMedia">*/
/*                 <li class="facebook">*/
/*                     <a href="#" target="_blank" rel="nofollow">Facebook</a>*/
/*                 </li>*/
/*                 <li class="twitter">*/
/*                     <a href="#" target="_blank" rel="nofollow">Twitter</a>*/
/*                 </li>*/
/*                 <li class="instagram">*/
/*                     <a href="#" target="_blank" rel="nofollow" >Instagram</a>*/
/*                 </li>*/
/*             </ul>*/
/* */
/*         </footer><!--Fin footer-->        */
/*     </div> </body>*/
/* </html>*/
