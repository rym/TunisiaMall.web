<?php

/**
 * Description of AdminController
 *
 * @author asus pc
 */

namespace MyApp\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller {

    public function indexAction() {
        return $this->render('MyAppAdminBundle:Admin:statistique.html.twig');
    }

    public function clientsAction() {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("MyAppUserBundle:Client")->findAll();
        return $this->render("MyAppAdminBundle:Admin:clients.html.twig", array("clients" => $user));
    }

    public function deleteClientAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("MyAppUserBundle:Client")->find($id);
        $em->merge($user);
        $em->remove($user);
        $em->flush();
        return $this->redirectToRoute('my_app_admin_clients');
    }

    public function debloquerClientAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("MyAppUserBundle:Client")->find($id);
        $user->setlocked(false);
        $em->merge($user);
        $em->flush();
        return $this->redirectToRoute('my_app_admin_clients');
    }

    public function bloquerClientAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("MyAppUserBundle:Client")->find($id);
        $user->setlocked(true);
        $em->merge($user);
        $em->flush();
        return $this->redirectToRoute('my_app_admin_clients');
    }

    
    
    
    public function ResponsablesAction() {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("MyAppUserBundle:Responsable")->findAll();
        return $this->render("MyAppAdminBundle:Admin:responsables.html.twig", array("responsables" => $user));
    }

    public function deleteResponsableAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("MyAppUserBundle:Responsable")->find($id);
        $em->merge($user);
        $em->remove($user);
        $em->flush();
        return $this->redirectToRoute('my_app_admin_responsables');
    }

    public function debloquerResponsableAction($id) {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("MyAppUserBundle:Responsable")->find($id);
        $user->setlocked(false);
        $em->merge($user);
        $em->flush();
        return $this->redirectToRoute('my_app_admin_responsables');
    }

   
}
