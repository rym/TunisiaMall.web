<?php

namespace MyApp\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TunisiaMallController extends Controller {

    public function indexAction() {
        return $this->render('MyAppUserBundle:public:public.html.twig');
    }

    public function profileAction(Request $request) {
        $user = $this->get('security.context')->getToken()->getUser();
        $form = $this->createForm(new \MyApp\UserBundle\Form\Type\ProfileResponsableType(), $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->merge($user);
            $em->flush();
            return $this->redirectToRoute('my_app_home');
        }
        return $this->render('MyAppUserBundle:Profile:profileResponsable.html.twig', array('form' => $form->createView()));
    }

}
