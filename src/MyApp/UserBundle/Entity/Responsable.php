<?php

namespace MyApp\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;
use MyApp\UserBundle\Entity\User;
/**
 * Description of Responsable
 *
 * @author asus pc
 */

/**
 * @ORM\Entity
 * @ORM\Table(name="responsable")
 * @UniqueEntity(fields = "username", targetClass = "MyApp\UserBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "MyApp\UserBundle\Entity\User", message="fos_user.email.already_used")
 */
class Responsable extends User {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    function __construct() {
        parent::__construct();
        $this->roles = array('ROLE_RESPONSABLE_ENSEIGNE');
    }

}
